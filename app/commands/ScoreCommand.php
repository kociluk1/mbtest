<?php

namespace App\Console;

use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ScoreCommand
 * Command for calculating additional points to score.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ScoreCommand extends Command\Command {

    /** @var \App\Helpers\RankHelper */
    private $rankHelper;

    /**
     * Configurates command.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function configure() {
        $this->setName('app:score')
                ->setDescription('Calculates additional points to score.');
    }

    /**
     * Initializes required services.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        $this->rankHelper = $this->getHelper('container')->getByType('App\Helpers\RankHelper');
    }

    /**
     * Executes command to clean cache.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->rankHelper->calculateScore();
            $output->writeLn('<info>app:score - Score bonus successfully calculated</info>');
            return 0; // zero return code means everything is ok
        } catch (\Exception $ex) {
            $output->writeLn('<error>app:score - ' . $ex->getMessage() . '</error>');
            return 1; // non-zero return code means error
        }
    }

}
