<?php

namespace App\Console;

use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * FindRelationsTestsCommand
 * Command for finding and storing relations tests.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class FindRelationsTestsCommand extends Command\Command {

    /** @var \App\Helpers\OSMRelationsTestsHelper */
    private $osmRelationsTestsHelper;

    /**
     * Configurates command.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function configure() {
        $this->setName('app:find-relations-tests')
                ->setDescription('Finds relations tests and stores them into the database.');
    }

    /**
     * Initializes required services.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        $this->osmRelationsTestsHelper = $this->getHelper('container')->getByType('App\Helpers\OSMRelationsTestsHelper');
    }

    /**
     * Executes command to clean cache.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->osmRelationsTestsHelper->storeTestsByAllRelations();
            $output->writeLn('<info>app:find-relations-tests - Relations tests successfully stored</info>');
            return 0; // zero return code means everything is ok
        } catch (\Exception $ex) {
            $output->writeLn('<error>app:find-relations-tests - ' . $ex->getMessage() . '</error>');
            return 1; // non-zero return code means error
        }
    }

}
