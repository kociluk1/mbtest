<?php

namespace App\Console;

use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * RevalidateCacheCommand
 * Command for revalidating cache.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RevalidateCacheCommand extends Command\Command {

    /** @var \App\Helpers\CacheHelper */
    private $cacheHelper;

    /** @var \App\Helpers\RevalidationHelper */
    private $revalidationHelper;

    /**
     * Configurates command.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function configure() {
        $this->setName('cache:revalidate')
                ->setDescription('Revalidates cache file storage.');
    }

    /**
     * Initializes required services.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        $this->cacheHelper = $this->getHelper('container')->getByType('App\Helpers\CacheHelper');
        $this->revalidationHelper = $this->getHelper('container')->getByType('App\Helpers\RevalidationHelper');
    }

    /**
     * Executes command to clean cache.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->cacheHelper->clean();
            $this->revalidationHelper->revalidate();
            $output->writeLn('<info>cache:revalidate - Cache successfully revalidated</info>');
            return 0; // zero return code means everything is ok
        } catch (\Exception $ex) {
            $output->writeLn('<error>cache:revalidate - ' . $ex->getMessage() . '</error>');
            return 1; // non-zero return code means error
        }
    }

}
