<?php

namespace App\Console;

use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ImportCommand
 * Command for importing CSV file containing open data from netmetr.cz.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ImportCommand extends Command\Command {

    /** @var \App\Helpers\ImportHelper */
    private $importHelper;
    
    /**
     * Configurates command.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function configure() {
        $this->setName('app:import')
                ->setDescription('Imports CSV file containing open data from netmetr.cz.');
    }

    /**
     * Initializes required services.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        $this->importHelper = $this->getHelper('container')->getByType('App\Helpers\ImportHelper');
    }

    /**
     * Executes command to import CSV file containing open data from netmetr.cz.
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $this->importHelper->importCSV();
            $output->writeLn('<info>app:import - Data successfully imported</info>');
            return 0; // zero return code means everything is ok
        } catch (\Exception $ex) {
            $output->writeLn('<error>app:import - ' . $ex->getMessage() . '</error>');
            return 1; // non-zero return code means error
        }
    }

}
