<?php

namespace App\Forms;

use App\Helpers;
use App\Model;
use App\Presenters\BasePresenter as BP;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils;
use Tracy\Debugger;

/**
 * OSMImportForm
 * Load OSM data form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OSMImportForm extends Nette\Object {

    /** @var string CSV file. */
    private $csvFile;
    
    /** @var array List with features. */
    private $featureList;

    /** @var \App\Model\FeatureRepository */
    private $featureService;

    /** @var \App\Helpers\OSMImportHelper */
    private $osmImportHelper;

    /** @var \App\Model\RelationRepository */
    private $relationRepository;

    /** @var string Data directory. */
    private $dataDir;

    /**
     * Constructor.
     * @param string $dataDir
     * @param \App\Helpers\OSMImportHelper $osmImportHelper
     * @param \App\Model\FeatureRepository $featureRepository
     * @param \App\Model\RelationRepository $relationRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct($dataDir, Helpers\OSMImportHelper $osmImportHelper, Model\FeatureRepository $featureRepository, Model\RelationRepository $relationRepository) {
        $this->dataDir = $dataDir;
        $this->osmImportHelper = $osmImportHelper;
        $this->featureService = $featureRepository;
        $this->relationRepository = $relationRepository;

        $this->featureList = $this->featureService->findAll()->fetchPairs('id', 'value_cs');
    }

    /**
     * Creates new load OSM data form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addText('id', 'ID relace')
                ->addRule(Form::INTEGER, 'Zadejte celé číslo.')
                ->setRequired('Zadejte ID relace. OSM tag id=*.');

        $form->addText('name', 'Jméno relace')
                ->setRequired('Zadejte jméno relace. OSM tag name=*.');

        $form->addSelect('feature', 'OSM vlastnost', $this->featureList)
                ->setPrompt('Vyberte vlastnost. OSM feature.')
                ->setRequired('Vyberte vlastnost. OSM feature.');

        $form->addUpload('json_upload', 'geoJSON data')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 5 MB.', 5 * 1024 * 1024 /* v bytech */)
                //->addRule(Form::MIME_TYPE, 'Lze nahrát pouze soubor ve formát JSON.', 'application/json')
                ->setRequired('Vložte JSON s daty.');

        $form->addUpload('csv_upload', 'CSV data oddělená čárkou ","')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 5 MB.', 5 * 1024 * 1024 /* v bytech */)
                //->addRule(Form::MIME_TYPE, 'Lze nahrát pouze soubor ve formát CSV.', 'text/csv')
                ->setRequired('Vložte CSV s daty.');

        $form->addProtection('Vypršel ochranný časový limit, prosím odešlete formulář ještě jednou.');

        $form->addSubmit('load', 'Nahrát data');

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    /**
     * Processes valid form.
     * @param Nette\Application\UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function formSucceeded(Form $form, Utils\ArrayHash $values) {
        try {
            $this->relationRepository->insertOrUpdate(array('id' => $values->id, 'name' => $values->name, 'last_update' => '00-00-00 00:00:00', 'Feature_id' => $values->feature));
            if (!$this->storeFile($values->json_upload, $values->id, 'geojson')) {
                $form['json_upload']->addError('Soubor JSON se nepodařilo nahrát. Opakujte akci.');
            }

            if (!$this->storeFile($values->csv_upload, $values->id, 'csv')) {
                $form['csv_upload']->addError('Soubor CSV se nepodařilo nahrát. Opakujte akci.');
            }

            $this->osmImportHelper->importCSV($this->csvFile, $values->id);
            $form->presenter->flashMessage('Vložení OSM dat proběhlo úspěšně.', BP::FMT_SUCCESS);
        } catch (\Exception $ex) {
            $form->presenter->flashMessage('Neznámá chyba. Kontaktujte správce aplikace.', BP::FMT_ERROR);
            Debugger::log($ex, Debugger::ERROR);
        }
        
        $form->presenter->redirect('this');
    }

    /**
     * Stores file and checks whether is there any error.
     * @param Nette\Http\FileUpload $file
     * @param string $name
     * @param string $type
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeFile($file, $name, $type) {
        if ($file->isOK()) {
            $this->csvFile = $name . '.' . $type;
            $file->move($this->dataDir . '/' . $this->csvFile);
            return TRUE;
        }

        return FALSE;
    }

}
