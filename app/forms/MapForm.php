<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

/**
 * MapForm
 * Map form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class MapForm extends Nette\Object {

    /** @const ALL Select all values. */
    const ALL = 0;

    /** @var array List of measurements. */
    private $measurements = array();

    /** @var array List of models. */
    private $models = array();

    /** @var \App\Model\ModelRepository */
    private $modelService;

    /** @var array List of operators. */
    private $operators = array();

    /** @var \App\Model\OperatorRepository */
    private $operatorService;

    /** @var array List of percentiles. */
    private $percentiles = array();

    /** @var array List of periods. */
    private $periods = array();

    /** @var array List of platforms. */
    private $platforms = array();

    /** @var \App\Model\PlatformRepository */
    private $platformService;

    /** @var array List of technologies. */
    private $technologies = array();

    /** @var \App\Model\TechnologyRepository */
    private $technologyService;

    /**
     * Constructor.
     * @param \App\Model\ModelRepository $modelRepository
     * @param \App\Model\OperatorRepository $operatorRepository
     * @param \App\Model\PlatformRepository $platformRepository
     * @param \App\Model\TechnologyRepository $technologyRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Model\ModelRepository $modelRepository, Model\OperatorRepository $operatorRepository, Model\PlatformRepository $platformRepository, Model\TechnologyRepository $technologyRepository) {
        $this->modelService = $modelRepository;
        $this->operatorService = $operatorRepository;
        $this->platformService = $platformRepository;
        $this->technologyService = $technologyRepository;

        $this->models[0] = 'Všechny modely';
        $this->models += $this->modelService->findAll()->fetchPairs('id', 'model');
        $this->operators[0] = 'Všichni operátoři';
        $this->operators += $this->operatorService->findAll()->order('id')->fetchPairs('id', 'operator');
        $this->platforms[0] = 'Všechny OS';
        $this->platforms += $this->platformService->findAll()->order('id')->fetchPairs('id', 'platform');
        $this->technologies = $this->technologyService->findAll()->order('id')->fetchPairs('id', 'technology');

        $this->measurements = array(
            'Mobilní sítě' => array(
                'download_mobile' => 'Mobilní download',
                'upload_mobile' => 'Mobilní upload',
                'ping_mobile' => 'Mobilní ping',
                'signal_mobile' => 'Mobilní signál',
            ),
            'WLAN' => array(
                'download_wlan' => 'WLAN download',
                'upload_wlan' => 'WLAN upload',
                'ping_wlan' => 'WLAN ping',
                'signal_wlan' => 'WLAN signál',
            ),
            'Vše' => array(
                'download' => 'Download',
                'upload' => 'Upload',
                'ping' => 'Ping'
            )
        );

        $this->percentiles = array(
            10 => '10% percentil',
            30 => '30% percentil',
            50 => '50% medián',
            70 => '70% percentil',
            90 => '90% percentil',
            100 => 'Všechna měření'
        );

        $this->periods = array(
            DateTime::from('-1week')->format('Y-m-d') => '1 týden',
            DateTime::from('-1month')->format('Y-m-d') => '1 měsíc',
            DateTime::from('-3months')->format('Y-m-d') => '3 měsíce',
            DateTime::from('-6months')->format('Y-m-d') => '6 měsíců',
            DateTime::from('-1year')->format('Y-m-d') => '1 rok',
        );
    }

    /**
     * Creates new map form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addSelect('measurement', 'Měření', $this->measurements)
                ->setDefaultValue('download_mobile');

        $form->addSelect('model', 'Model', $this->models)
                ->setDefaultValue(static::ALL);

        $form->addSelect('operator', 'Operátor', $this->operators)
                ->setDefaultValue(static::ALL);

        $form->addSelect('percentile', 'Percentil', $this->percentiles)
                ->setDefaultValue(50);

        $form->addSelect('period', 'Období', $this->periods)
                ->setDefaultValue(DateTime::from('-1year')->format('Y-m-d'));

        $form->addSelect('platform', 'Operační systém', $this->platforms)
                ->setDefaultValue(static::ALL);

        $form->addSelect('technology', 'Technologie', $this->technologies)
                ->setDefaultValue($this->technologyService->findOneBy(array('technology' => Model\TechnologyRepository::TECHNOLOGY_2G_3G_4G))->id);

        return $form;
    }

}
