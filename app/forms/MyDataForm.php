<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;

/**
 * MyDataForm
 * My data form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class MyDataForm extends Nette\Object {

    /** @var array List of measurements. */
    private $measurements = array();

    /** @var array List of limits. */
    private $limits = array();

    /**
     * Constructor.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct() {
        $this->measurements = array(
            'download' => 'Download',
            'upload' => 'Upload',
            'ping' => 'Ping'
        );

        $this->limits = array(
            0 => 'Všechna měření',
            100 => 'Posledních 100 měření',
            500 => 'Posledních 500 měření',
            1000 => 'Posledních 1000 měření',
            2000 => 'Posledních 2000 měření'
        );
    }

    /**
     * Creates new my data form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addSelect('measurement', 'Měření', $this->measurements)
                ->setDefaultValue('download');

        $form->addSelect('limit', 'Počet měření', $this->limits)
                ->setDefaultValue(100);

        return $form;
    }

}
