<?php

namespace App\Forms;

use App\Model;
use App\Presenters\BasePresenter as BP;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security;
use Nette\Utils;

/**
 * SignInForm
 * Sign in form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class SignInForm extends Nette\Object {

    /** @var Nette\Security\User */
    private $user;

    /** @var \App\Model\UserRepository */
    private $userService;
    
    /** @var \App\Model\UsersRepository */
    private $usersService;

    /**
     * Constructor.
     * @param Nette\Security\User $user
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Security\User $user, Model\UserRepository $userRepository, Model\UsersRepository $usersRepository) {
        $this->user = $user;
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;
    }

    /**
     * Creates new sign-in form.
     * @param string $backlink
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create($backlink = NULL) {
        $form = new Form();

        $form->addText('username', 'Uživatelské jméno')
                ->setRequired('Zadejte své uživatelské jméno.')
                ->setAttribute('placeholder', 'Uživatelské jméno');

        $form->addPassword('password', 'Heslo')
                ->setRequired('Zadejte své heslo.')
                ->setAttribute('placeholder', 'Heslo');

        $form->addCheckbox('remember', 'Přihlásit se trvale');

        $form->addHidden('backlink', $backlink);

        $form->addProtection('Vypršel ochranný časový limit, prosím odešlete formulář ještě jednou.');

        $form->addSubmit('signin', 'Přihlásit se');

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    /**
     * Processes valid form.
     * @param Nette\Application\UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     * @return void
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function formSucceeded(Form $form, Utils\ArrayHash $values) {
        if ($values->remember) {
            $this->user->setExpiration('14 days', FALSE);
        } else {
            $this->user->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->user->login($values->username, $values->password);
            $this->userService->invalidateResetToken($this->user->getId());
            $this->userService->updateLastIp($this->user->getId());
            $this->userService->updateLastLogin($this->user->getId());
            $form->presenter->flashMessage('Přihlášení proběhlo úspěšně.', BP::FMT_SUCCESS);
        } catch (Nette\Security\AuthenticationException $ex) {
            if ($ex->getCode() == 3) {
                $form->presenter->flashMessage($ex->getMessage(), BP::FMT_WARNING);
                $user = $this->usersService->findByUsername($values->username);
                $form->presenter->getTemplate()->email = $user->email;
            } elseif ($ex->getCode()) {
                $form->addError($ex->getMessage());
            } else {
                $form->presenter->flashMessage($ex->getMessage(), BP::FMT_WARNING);
            }
            return;
        }

        if (Model\Authorizator::isAdmin($this->user)) {
            $form->presenter->redirect(':Admin:Default:');
        } else {
            $form->presenter->restoreRequest($values->backlink);
            $form->presenter->redirect(':Front:Dashboard:');
        }
        
    }

}
