<?php

namespace App\Forms;

use App\Model;
use App\Presenters\BasePresenter as BP;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security;
use Nette\Utils;

/**
 * ResetPasswordForm
 * Reset password form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ResetPasswordForm extends Nette\Object {

    /** @var \App\Model\Authenticator */
    private $authenticator;

    /** @var Nette\Security\User */
    private $user;

    /** @var \App\Model\UserRepository */
    private $userService;

    /** @var \App\Model\UsersRepository */
    private $usersService;

    /**
     * Constructor.
     * @param Nette\Security\User $user
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @param \App\Model\Authenticator $authenticator
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Security\User $user, Model\UserRepository $userRepository, Model\UsersRepository $usersRepository, Model\Authenticator $authenticator) {
        $this->authenticator = $authenticator;
        $this->user = $user;
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;
    }

    /**
     * Creates new reset password form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addPassword('newPassword', 'Nové heslo')
                ->setRequired('Je nutné zadat nové heslo.')
                ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.', 6);

        $form->addPassword('passwordConfirmation', 'Potvrzení hesla')
                ->setRequired('Nové heslo je nutné zadat ještě jednou pro potvrzení.')
                ->addRule(Form::EQUAL, 'Zadaná hesla se musejí shodovat.', $form['newPassword']);

        $form->addProtection('Vypršel ochranný časový limit, prosím odešlete formulář ještě jednou.');

        return $form;
    }

    /**
     * Processes valid form.
     * @param Nette\Application\UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     * @return void
     */
    public function setPasswordFormSucceeded(Form $form, Utils\ArrayHash $values) {
        // condition for valid reset token
        if ($this->userService->checkResetToken($values->userId, $values->resetToken)) {
            try {
                $hashedPassword = $this->authenticator->calculateHash($values->newPassword);
                $this->userService->invalidateResetToken($values->userId);
                $this->usersService->update($values->userId, array('password' => $hashedPassword, 'passsalt' => NULL));
                $form->presenter->flashMessage('Úspěšně jste nastavili nové heslo, nyní se můžete přihlásit do aplikace.', BP::FMT_SUCCESS);
            } catch (\Exception $ex) {
                \Tracy\Debugger::log($ex, \Tracy\Debugger::ERROR);
                $form->presenter->flashMessage('Došlo k nečekané chybě.', BP::FMT_ERROR);
                return;
            }
        } else {
            $form->presenter->flashMessage('Neplatné uživatelské jméno nebo řetězec. Vygenerujte si prosím nový odkaz pro obnovení hesla.', BP::FMT_ERROR);
        }

        $form->presenter->redirect(':Front:Sign:in');
    }

}
