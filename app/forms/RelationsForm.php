<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;

/**
 * RelationsForm
 * Relations form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RelationsForm extends Nette\Object {

    /** @var array List of measurements. */
    private $measurements = array();

    /** @var array List of relations. */
    private $relations = array();

    /** @var \App\Model\RelationRepository */
    private $relationService;

    /**
     * Constructor.
     * @param \App\Model\RelationRepository $relationRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Model\RelationRepository $relationRepository) {
        $this->relationService = $relationRepository;

        $rels = $this->relationService->findAll();
        foreach ($rels as $key => $rel) {
            $this->relations[$key] = Strings::firstUpper($rel->Feature->value_cs) . ' ' . $rel->name;
        }

        $this->measurements = array(
            'download_mobile' => 'Mobilní download',
            'upload_mobile' => 'Mobilní upload',
            'ping_mobile' => 'Mobilní ping',
        );
    }

    /**
     * Creates new relations form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addSelect('measurement', 'Měření', $this->measurements)
                ->setDefaultValue('download_mobile');

        $form->addSelect('relation', 'Trasa', $this->relations)
                ->setPrompt('Zvol cestu s měřeními');

        return $form;
    }

}
