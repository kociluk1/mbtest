<?php

namespace App\Forms;

use App\Model;
use App\Presenters\BasePresenter as BP;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security;
use Nette\Utils;

/**
 * OTPSignInForm
 * One-time password sign in form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OTPSignInForm extends Nette\Object {

    /** @var Nette\Security\User */
    private $user;

    /** @var \App\Model\UserRepository */
    private $userService;

    /**
     * Constructor.
     * @param Nette\Security\User $user
     * @param \App\Model\UserRepository $userRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Security\User $user, Model\UserRepository $userRepository) {
        $this->user = $user;
        $this->userService = $userRepository;
    }

    /**
     * Creates new OTP sign-in form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addText('otp', 'Jednorázový přístupový kód')
                ->setRequired('Zadejte jednorázový přístupový kód.')
                ->setAttribute('placeholder', 'Přístupový kód');

        $form->addProtection('Vypršel ochranný časový limit, prosím odešlete formulář ještě jednou.');

        $form->addSubmit('signin', 'Přihlásit se');

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    /**
     * Processes valid form.
     * @param Nette\Application\UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     * @return void
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function formSucceeded(Form $form, Utils\ArrayHash $values) {
        $this->user->setExpiration('20 minutes', TRUE);

        try {
            $this->user->login($values->otp);
            $this->userService->invalidateResetToken($this->user->getId());
            $this->userService->updateLastIp($this->user->getId());
            $this->userService->updateLastLogin($this->user->getId());
            $form->presenter->flashMessage('Přihlášení proběhlo úspěšně.', BP::FMT_SUCCESS);
        } catch (Nette\Security\AuthenticationException $ex) {
            if ($ex->getCode()) {
                $form->addError($ex->getMessage());
            } else {
                $form->presenter->flashMessage($ex->getMessage(), BP::FMT_WARNING);
            }
            return;
        }

        $form->presenter->redirect(':Front:Dashboard:');
    }

}
