<?php

namespace App\Forms;

use App\Helpers;
use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * ContactForm
 * Contact form factory.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ContactForm extends Nette\Object {

    /** @var App\Helpers\Mailer */
    private $mailer;

    /** @var string Webmaster email. */
    private $webmasterEmail;

    /**
     * Constructor.
     * @param type $webmasterEmail
     * @param \App\Helpers\Mailer $mailer
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct($webmasterEmail, Helpers\Mailer $mailer) {
        $this->webmasterEmail = $webmasterEmail;
        $this->mailer = $mailer;
    }

    /**
     * Creates new contact form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function create() {
        $form = new Form();

        $form->addText('name', 'Jméno')
                ->setAttribute('placeholder', 'Vaše jméno')
                ->setRequired('Zadejte prosím Vaše jméno.');

        $form->addText('email', 'Email')
                ->addRule(Form::EMAIL, 'Zadejte platnou emailovou adresu.')
                ->setAttribute('placeholder', 'Váš email')
                ->setRequired('Zadejte prosím Váš email, abychom Vám mohli odpovědět.');

        $form->addText('subject', 'Předmět')
                ->setAttribute('placeholder', 'Předmět zprávy')
                ->setRequired('Zadejte prosím předmět Vašeho dotazu.');

        $form->addTextArea('text', 'Zpráva')
                ->addRule(Form::MAX_LENGTH, 'Text zprávy může mít maximálně %d znaků.' , 500)
                ->setAttribute('placeholder', 'Text zprávy')
                ->setAttribute('rows', '6')
                ->setRequired('Zadejte prosím obsah Vaší zprávy nebo dotazu.');

        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = $this->formSucceeded;

        $form->addProtection('Vypršel ochranný časový limit, prosím odešlete formulář ještě jednou.');

        return $form;
    }

    /**
     * Processes form.
     * @param Nette\Application\UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values) {
        $message = $this->mailer->createMessage($values->email, $values->name, $this->webmasterEmail, $values->subject, $values->text);
        $this->mailer->sendMail($message, 'Zpráva byla úspěšně odeslána.');
        $form->presenter->redirect('this');
    }

}
