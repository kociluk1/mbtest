<?php

namespace App\Helpers;

use Nette;
use Nette\Caching\Cache;
use Nette\Utils\DateTime;

/**
 * CacheHelper
 * Performs caching of data.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class CacheHelper extends Nette\Object {

    /** @var Nette\Caching\Cache Cache. */
    private $cache;

    /** @var array Dependencies for cache. */
    private $dependencies;

    /**
     * Constructor.
     * @param Nette\Caching\Cache $cache
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Cache $cache) {
        $this->cache = $cache;
        $this->dependencies = array(
            Cache::EXPIRE => DateTime::from('+1day 04:00:00')->getTimestamp(),
            Cache::TAGS => 'api'
        );
    }

    /**
     * Cleans whole API cache.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function clean() {
        $this->cache->clean(array(Cache::TAGS => 'api'));
    }

    /**
     * Returns dependencies for the cache.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDependencies() {
        return $this->dependencies;
    }

    /**
     * Loads data from the cache.
     * @param string $key
     * @param callable|NULL $fallback
     * @return mixed
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function load($key, $fallback = NULL) {
        return $this->cache->load($key, $fallback);
    }

    /**
     * Reloads data into the cache.
     * @param callable|NULL $fallback
     * @return mixed
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function reload($key, $fallback = NULL) {
        $urlKey = str_replace(',', '.', $key);

        return $this->cache->load($urlKey, $fallback);
    }

    /**
     * Removes resource from the cache.
     * @param string $key
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function remove($key) {
        $this->cache->remove($key);
    }

    /**
     * Saves new resource to the cache.
     * @param string $key
     * @param mixed $data
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function save($key, $data) {
        $this->cache->save($key, $data);
    }

}
