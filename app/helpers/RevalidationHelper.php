<?php

namespace App\Helpers;

use App\ApiModule\Presenters\BaseApiPresenter as BAP;
use App\Helpers;
use App\Model;
use App\Model\TechnologyRepository as TR;
use Nette;
use Nette\Utils\Strings;
use Tracy\Debugger;

/**
 * RevalidationHelper
 * Revalidates API cache.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RevalidationHelper extends Nette\Object {

    /** @var \App\Helpers\DataHelper */
    private $dataHelper;

    /** @var \App\Helpers\OSMDataHelper */
    private $osmDataHelper;

    /** @var \App\Model\RelationRepository */
    private $relationService;

    /**
     * Constructor.
     * @param \App\Helpers\DataHelper $dataHelper
     * @param \App\Helpers\OSMDataHelper $osmDataHelper
     * @param \App\Model\RelationRepository $relationRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Helpers\DataHelper $dataHelper, Helpers\OSMDataHelper $osmDataHelper, Model\RelationRepository $relationRepository) {
        $this->dataHelper = $dataHelper;
        $this->osmDataHelper = $osmDataHelper;
        $this->relationService = $relationRepository;
    }

    /**
     * Revalidates all data in the cache.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function revalidate() {
        Debugger::timer();
        echo "[1] relations revalidation - started\n";
        $this->revalidateRelations();
        echo "[2] resources revalidation - started\n";
        $this->revalidateResources();

        $totalTimeSec = (int) Debugger::timer();
        $totalTimeMin = (int) ($totalTimeSec / 60);

        echo "\nTotal time: \t" . ($totalTimeMin < 1 ? $totalTimeSec . " sec. " : $totalTimeMin . " min. ") . "\n";
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns resource name.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getResource($resource) {
        return substr($resource, 0, -1);
    }

    /**
     * Revalidates all relations and cached results.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function revalidateRelations() {
        $relations = $this->relationService->findAll();
        $counter = 0;
        foreach ($relations as $key => $row) {
            $counter++;
            $this->osmDataHelper->getRelation($key, $this->getResource(BAP::API_DOWNLOADS));
            $this->osmDataHelper->getRelation($key, $this->getResource(BAP::API_UPLOADS));
            $this->osmDataHelper->getRelation($key, $this->getResource(BAP::API_PINGS));
            echo "[1." . $counter . "] relations - " . $row->Feature->value_en . " - " . Strings::webalize($row->name) . " - ok\n";
        }
    }

    /**
     * Revalidates all resources and cached results.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function revalidateResources() {
        ini_set('memory_limit', '512M'); 
        $this->dataHelper->getData(array('api' => BAP::API_DOWNLOADS, 'technology' => TR::TECHNOLOGY_2G_3G_4G, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.1] resources - download - mobile - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_DOWNLOADS, 'technology' => TR::TECHNOLOGY_WLAN, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.2] resources - download - wlan - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_PINGS, 'technology' => TR::TECHNOLOGY_2G_3G_4G, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.3] resources - ping - mobile - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_PINGS, 'technology' => TR::TECHNOLOGY_WLAN, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.4] resources - ping - wlan - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_UPLOADS, 'technology' => TR::TECHNOLOGY_2G_3G_4G, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.5] resources - uplod - mobile - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_UPLOADS, 'technology' => TR::TECHNOLOGY_WLAN, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.6] resources - uplod - wlan - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_SIGNALS, 'technology' => TR::TECHNOLOGY_2G_3G_4G, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.7] resources - signal - mobile - ok\n";
        $this->dataHelper->getData(array('api' => BAP::API_SIGNALS, 'technology' => TR::TECHNOLOGY_WLAN, 'operator' => NULL, 'model' => NULL, 'sw_lat' => NULL, 'sw_lng' => NULL, 'ne_lat' => NULL, 'ne_lng' => NULL, 'period' => '1year', 'percentile' => NULL));
        echo "[2.8] resources - signal - wlan - ok\n";
        $this->dataHelper->getModels();
        echo "[2.9] resources - model - ok\n";
        $this->dataHelper->getOperators();
        echo "[2.10] resources - operator - ok\n";
        $this->dataHelper->getPlatforms();
        echo "[2.11] resources - platforms - ok\n";
        $this->dataHelper->getTechnologies();
        echo "[2.12] resources - technologies - ok\n";
    }

}
