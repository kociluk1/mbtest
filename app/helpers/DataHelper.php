<?php

namespace App\Helpers;

use App\Helpers;
use App\Model;
use Nette;
use Nette\Utils\DateTime;

/**
 * DataHelper
 * Processes API calls and returns required data.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class DataHelper extends Nette\Object {

    /** @const DEFAULT_PERIOD Default period for SQL query. */
    const DEFAULT_PERIOD = '2010-01-01 00:00:00';

    /** @var string Api call. */
    private $api;

    /** @var \App\Helpers\CacheHelper */
    private $cacheHelper;

    /** @var string Phone model. */
    private $model;

    /** @var \App\Helpers\MeasurementHelper */
    private $measurementHelper;

    /** @var \App\Model\ModelRepository */
    private $modelService;

    /** @var array North east bound coordinates of map. */
    private $neCoords = array();

    /** @var string Operator MCC-MNC. */
    private $operator;

    /** @var \App\Model\OperatorRepository */
    private $operatorService;

    /** @var int Percentile of results. */
    private $percentile;

    /** @var string Period of resutls. */
    private $period;

    /** @var \App\Model\PlatformRepository */
    private $platformService;

    /** @var array South west bound coordinates of map. */
    private $swCoords = array();

    /** @var string Input technology. */
    private $technology;

    /** @var array List of technologies of mobile networks. */
    private $technologies;

    /** @var \App\Model\TechnologyRepository */
    private $technologyService;

    /**
     * Constructor.
     * @param \App\Helpers\CacheHelper $cacheHelper
     * @param \App\Helpers\MeasurementHelper $measurementHelper
     * @param \App\Model\ModelRepository $modelRepository
     * @param \App\Model\OperatorRepository $operatorRepository
     * @param \App\Model\PlatformRepository $platformRepository
     * @param \App\Model\TechnologyRepository $technologyRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Helpers\CacheHelper $cacheHelper, Helpers\MeasurementHelper $measurementHelper, Model\ModelRepository $modelRepository, Model\OperatorRepository $operatorRepository, Model\PlatformRepository $platformRepository, Model\TechnologyRepository $technologyRepository) {
        $this->cacheHelper = $cacheHelper;
        $this->measurementHelper = $measurementHelper;
        $this->modelService = $modelRepository;
        $this->operatorService = $operatorRepository;
        $this->platformService = $platformRepository;
        $this->technologyService = $technologyRepository;
    }

    /**
     * Returns array with data related to measurements with the given params.
     * - Cached
     * @param array $params
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getData(array $params) {
        $this->setParams($params);

        // Cached
        return $this->getMeasurements();
    }

    /**
     * Returns array with data related to measurement with the given id.
     * - Not cached
     * @param string $dataset
     * @param int $id
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataById($dataset, $id) {
        // Not cached
        return $this->measurementHelper->getDataById($dataset, $id);
    }

    /**
     * Returns array with mobile models.
     * - Cached
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getModels() {
        // Cached
        $key = 'models';
        return $this->cacheHelper->reload($key, function(&$dependencies) {
                    $dependencies = $this->cacheHelper->getDependencies();
                    return $this->modelService->findAll()->order('id')->fetchPairs('id', 'model');
                });
    }

    /**
     * Returns array with mobile models.
     * - Cached
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getPlatforms() {
        // Cached
        $key = 'platforms';
        return $this->cacheHelper->reload($key, function(&$dependencies) {
                    $dependencies = $this->cacheHelper->getDependencies();
                    return $this->platformService->findAll()->order('id')->fetchPairs('id', 'platform');
                });
    }

    /**
     * Returns array with mobile operators.
     * - Cached
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getOperators() {
        // Cached
        $key = 'operators';
        return $this->cacheHelper->reload($key, function(&$dependencies) {
                    $dependencies = $this->cacheHelper->getDependencies();
                    $operators = $this->operatorService->findAll()->order('id');
                    $data = array();

                    foreach ($operators as $id => $operator) {
                        $data[$id] = array(
                            'mcc' => $operator->mcc,
                            'mnc' => $operator->mnc,
                            'brand' => $operator->brand,
                            'operator' => $operator->operator
                        );
                    }

                    return $data;
                });
    }

    /**
     * Returns array with mobile network technologies.
     * - Cached
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getTechnologies() {
        // Cached
        $key = 'technologies';
        return $this->cacheHelper->reload($key, function(&$dependencies) {
                    $dependencies = $this->cacheHelper->getDependencies();
                    return $this->technologyService->findAll()->order('id')->fetchPairs('id', 'technology');
                });
    }

    /**
     * Returns array with user data.
     * - Not cached
     * @param int $userId
     * @param int $limit
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getUserData($userId, $resource, $limit) {
        // Not cached
        return $this->measurementHelper->getDataByUserId($userId, $resource, $limit);
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns map bounds.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getBounds() {
        return array(
            $this->swCoords,
            $this->neCoords
        );
    }

    /**
     * Returns API key.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getApiKey() {
        return $this->api . '/' .
                $this->technology . '/' .
                $this->operator . '/' .
                $this->model . '/' .
                $this->period . '/' .
                $this->percentile . '/' .
                $this->swCoords[0] . '/' . $this->swCoords[1] . '/' .
                $this->neCoords[0] . '/' . $this->swCoords[1];
    }

    /**
     * Returns options.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getOptions() {
        return array(
            'resource' => $this->getResource(),
            'period' => $this->getPeriod(),
            'percentile' => $this->percentile
        );
    }

    /**
     * Returns period.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getPeriod() {
        if ($this->period) {
            return DateTime::from('-' . $this->period)->format('Y-m-d 00:00:00');
        }

        return DateTime::from(static::DEFAULT_PERIOD)->format('Y-m-d 00:00:00');
    }

    /**
     * Returns resource name.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getResource() {
        return substr($this->api, 0, -1);
    }

    /**
     * Sets parameters.
     * @param array $params
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setParams($params) {
        $this->api = $params['api'];
        $this->model = $params['model'];
        $this->neCoords = array($params['ne_lat'], $params['ne_lng']);
        $this->operator = $this->operatorService->getOperatorId($params['operator']);
        $this->percentile = $params['percentile'];
        $this->period = $params['period'];
        $this->swCoords = array($params['sw_lat'], $params['sw_lng']);
        $this->technologies = $this->technologyService->getTechnologies($params['technology']);
        $this->technology = $params['technology'];
    }

    /**
     * Select proper method and returns measurements.
     * - Cached
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getMeasurements() {
        // Technology is always specified, if input technology in API call is NULL
        // then technology is set to all mobile technologies.
        // 1) Operator && Model 
        if ($this->operator && $this->model) {
            // Cached
            return $this->cacheHelper->reload($this->getApiKey(), function(&$dependencies) {
                        $dependencies = $this->cacheHelper->getDependencies();
                        return $this->measurementHelper->getDataByTechnologyOperatorModel($this->technologies, $this->operator, $this->model, $this->getOptions(), $this->getBounds());
                    });
        }
        // 2) Operator 
        elseif ($this->operator) {
            // Cached
            return $this->cacheHelper->reload($this->getApiKey(), function(&$dependencies) {
                        $dependencies = $this->cacheHelper->getDependencies();
                        return $this->measurementHelper->getDataByTechnologyOperator($this->technologies, $this->operator, $this->getOptions(), $this->getBounds());
                    });
        }
        // 3) Model 
        elseif ($this->model) {
            // Cached
            return $this->cacheHelper->reload($this->getApiKey(), function(&$dependencies) {
                        $dependencies = $this->cacheHelper->getDependencies();
                        return $this->measurementHelper->getDataByTechnologyModel($this->technologies, $this->model, $this->getOptions(), $this->getBounds());
                    });
        }
        // 4) Nothing
        else {
            // Cached
            return $this->cacheHelper->reload($this->getApiKey(), function(&$dependencies) {
                        $dependencies = $this->cacheHelper->getDependencies();
                        return $this->measurementHelper->getDataByTechnology($this->technologies, $this->getOptions(), $this->getBounds());
                    });
        }
    }

}
