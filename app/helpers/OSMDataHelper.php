<?php

namespace App\Helpers;

use App\Helpers;
use App\Model;
use Nette;

/**
 * OSMDataHelper
 * Processes API calls and returns required OSM data.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OSMDataHelper extends Nette\Object {

    /** @var \App\Helpers\CacheHelper */
    private $cacheHelper;

    /** @var string Required data. */
    private $data;

    /** @var int Relation id. */
    private $relationId;

    /** @var \App\Model\Relation_has_TestRepository */
    private $relationHasTestService;

    /** @var array Array with results. */
    private $measurements = array();

    /**
     * Constructor.
     * @param \App\Helpers\CacheHelper $cacheHelper
     * @param \App\Model\Relation_has_TestRepository $relationHasTestRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Helpers\CacheHelper $cacheHelper, Model\Relation_has_TestRepository $relationHasTestRepository) {
        $this->cacheHelper = $cacheHelper;
        $this->relationHasTestService = $relationHasTestRepository;
    }

    /**
     * Returns measurements which belongs to a realation.
     * @param int $relationId
     * @param string $data
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getRelation($relationId, $data) {
        $this->relationId = $relationId;
        $this->data = $data;
        $this->measurements = array();

        $key = 'relations/' . $relationId . '/' . $data;
        return $this->cacheHelper->reload($key, function(&$dependencies) {
                    $dependencies = $this->cacheHelper->getDependencies();
                    return $this->getTests();
                });
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns measurements.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getTests() {
        $this->setOpenDataResults($this->relationHasTestService->findBy(array('Relation_id' => $this->relationId)));

        return $this->measurements;
    }

    /**
     * Sets Open data results to array.
     * @param Nette\Database\Table\Selection $selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setOpenDataResults($selection) {
        foreach ($selection as $row) {
            $this->measurements[MeasurementHelper::DATASET_OPENDATA . '-' . $row->Test->id] = array(
                $this->data => $row->Test[$this->data],
                'lat' => $row->Test->latitude,
                'lng' => $row->Test->longitude,
                'date' => $row->Test->time->format('Y-m-d'),
                'tid' => $row->Test->Technology_id,
                'oid' => $row->Test->Operator_id,
                'mid' => $row->Test->Model_id,
                'pid' => $row->Test->Platform_id
            );
        }
    }

}
