<?php

namespace App\Helpers;

use App\Model;
use Nette;

/**
 * MeasurementHelper
 * Performs merge of data (found results of quering) obtain from mysql database and pgsql database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class MeasurementHelper extends Nette\Object {

    /** @const DATASET_MBTEST */
    const DATASET_MBTEST = 'mb';

    /** @const DATASET_OPENDATA */
    const DATASET_OPENDATA = 'od';

    /** @var string Api action. */
    private $apiAction;

    /** @var \App\Model\MeasureRepository */
    private $measureService;

    /** @var array One measurement found by id. */
    private $measurement = array();

    /** @var array Array of measurements found by parameters. */
    private $measurements = array();

    /** @var \App\Model\OperatorRepository */
    private $operatorService;

    /** @var \App\Model\TechnologyRepository */
    private $technologyService;

    /** @var \App\Model\TestRepository */
    private $testService;

    /**
     * Constructor.
     * @param \App\Model\MeasureRepository $measureRepository
     * @param \App\Model\TestRepository $testRepository
     * @param \App\Model\OperatorRepository $operatorRepository
     * @param \App\Model\TechnologyRepository $technologyRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Model\MeasureRepository $measureRepository, Model\TestRepository $testRepository, Model\OperatorRepository $operatorRepository, Model\TechnologyRepository $technologyRepository) {
        $this->measureService = $measureRepository;
        $this->testService = $testRepository;
        $this->operatorService = $operatorRepository;
        $this->technologyService = $technologyRepository;
    }

    /**
     * Returns data found by measurement id.
     * @param string $dataset
     * @param int $id
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataById($dataset, $id) {
        if ($dataset == static::DATASET_OPENDATA) {
            $this->setOpenDataResult($this->testService->findOneById($id));
        } else {
            $this->setMbtestDataResult($this->measureService->findOneById($id));
        }

        return $this->measurement;
    }
    
    /**
     * Returns array with user data.
     * @param int $userId
     * @param int $limit
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataByUserId($userId, $resource, $limit) {
        $this->measurements = array();
        $this->setApiAction($resource);
        $this->setMbtestDataResults($this->measureService->findByUserId($userId, $limit));
        
        return $this->measurements;
    }

    /**
     * Returns data found by technology.
     * @param array $technologies
     * @param array $options
     * @param array $bounds
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataByTechnology(array $technologies, array $options, array $bounds) {
        if ($this->checkBounds($bounds) === FALSE) {
            return array();
        }

        $this->measurements = array();
        $this->setApiAction($options['resource']);
        $this->setMbtestDataResults($this->measureService->findByTechnology($technologies, $options, $bounds));
        $this->setOpenDataResults($this->testService->findByTechnology($technologies, $options, $bounds));

        return $this->measurements;
    }

    /**
     * Returns data found by technology and model.
     * @param array $technologies
     * @param int $idModel
     * @param array $options
     * @param array $bounds
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataByTechnologyModel(array $technologies, $idModel, array $options, array $bounds) {
        if ($this->checkBounds($bounds) === FALSE) {
            return array();
        }

        $this->measurements = array();
        $this->setApiAction($options['resource']);
        $this->setOpenDataResults($this->testService->findByTechnologyModel($technologies, $idModel, $options, $bounds));
        // MBTest database has different models ids. It cannot be queried.

        return $this->measurements;
    }

    /**
     * Returns data found by technology and operator.
     * @param array $technologies
     * @param int $idOperator
     * @param array $options
     * @param array $bounds
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataByTechnologyOperator(array $technologies, $idOperator, array $options, array $bounds) {
        if ($this->checkBounds($bounds) === FALSE) {
            return array();
        }

        $this->measurements = array();
        $this->setApiAction($options['resource']);
        $this->setMbtestDataResults($this->measureService->findByTechnologyOperator($technologies, $idOperator, $options, $bounds));
        $this->setOpenDataResults($this->testService->findByTechnologyOperator($technologies, $idOperator, $options, $bounds));

        return $this->measurements;
    }

    /**
     * Returns data found by technology, operator and model.
     * @param array $technologies
     * @param int $idOperator
     * @param int $idModel
     * @param array $options
     * @param array $bounds
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getDataByTechnologyOperatorModel(array $technologies, $idOperator, $idModel, array $options, array $bounds) {
        if ($this->checkBounds($bounds) === FALSE) {
            return array();
        }

        $this->measurements = array();
        $this->setApiAction($options['resource']);
        $this->setOpenDataResults($this->testService->findByTechnologyOperatorModel($technologies, $idOperator, $idModel, $options, $bounds));
        // MBTest database has different models ids. It cannot be queried.

        return $this->measurements;
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Checks bounds.
     * @param array $bounds
     * @return mixed
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function checkBounds(array &$bounds) {
        if (is_null($bounds[0][0]) && is_null($bounds[0][1]) && is_null($bounds[1][0]) && is_null($bounds[1][1])) {
            $bounds = NULL;
            return NULL;
        }

        if (count($bounds[0]) === 2 && count($bounds[1]) === 2) {
            for ($i = 0; $i < 2; $i++) {
                for ($j = 0; $j < 2; $j++) {
                    $bounds[$i][$j] = str_replace(',', '.', $bounds[$i][$j]);
                    if (!is_numeric($bounds[$i][$j])) {
                        return FALSE;
                    }
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Converts bps to kbps.
     * @param string $speed
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function convert($speed) {
        return (int) substr($speed, 0, -3);
    }

    /**
     * Creates container (array) for one result.
     * @param string $source
     * @param int $id
     * @param string $dateTime
     * @param double $latitude
     * @param double $longitude
     * @param int $download
     * @param int $upload
     * @param int $ping
     * @param int $signal
     * @param string $network
     * @param string $technology
     * @param string $operator
     * @param string $model
     * @param string $platform
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function createResultContainer($source, $id, $dateTime, $latitude, $longitude, $download, $upload, $ping, $signal, $network, $technology, $operator, $model, $platform) {
        return array(
            'source' => $source,
            'id' => $id,
            'date' => $dateTime,
            'lat' => $latitude,
            'lng' => $longitude,
            'download' => $download,
            'upload' => $upload,
            'ping' => $ping,
            'signal' => $signal,
            'network' => $network,
            'technology' => $technology,
            'operator' => $operator,
            'model' => $model,
            'platform' => $platform
        );
    }

    /**
     * Creates container (array) for results.
     * @param string $dataType
     * @param int $data
     * @param double $latitude
     * @param double $longitude
     * @param string $dateTime
     * @param int $tid
     * @param int $oid
     * @param int $mid
     * @param int $pid
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function createResultsContainer($dataType, $data, $latitude, $longitude, $dateTime, $tid, $oid, $mid, $pid) {
        return array(
            $dataType => $data,
            'lat' => $latitude,
            'lng' => $longitude,
            'date' => $dateTime,
            'tid' => $tid,
            'oid' => $oid,
            'mid' => $mid,
            'pid' => $pid
        );
    }

    /**
     * Returns datay by api action.
     * @param Nette\Database\Table\ActiveRow $row
     * @param string $dataset
     * @return string|int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getDataByApiAction($row, $dataset) {
        switch ($this->apiAction) {
            case 'download':
                if ($dataset == static::DATASET_OPENDATA) {
                    return $row->download;
                } else {
                    return $this->convert($row->download);
                }
            case 'upload':
                if ($dataset == static::DATASET_OPENDATA) {
                    return $row->upload;
                } else {
                    return $this->convert($row->download);
                }
            case 'ping':
                if ($dataset == static::DATASET_OPENDATA) {
                    return $row->ping;
                } else {
                    return $row->ping_avg;
                }
            case 'signal':
                return $row->signal;
        }
    }

    /**
     * Sets api action.
     * @param string $action
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setApiAction($action) {
        $this->apiAction = $action;
    }

    /**
     * Sets related measurement from MBTest database.
     * @param \Nette\Database\Table\ActiveRow $row
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setMbtestDataResult($row) {
        if ($row) {
            $source = 'MBTest';
            $id = $row->id;
            $dateTime = $row->date_time->format('d.m.Y H:i');
            $latitude = $row->latitude1;
            $longitude = $row->longitude1;
            $download = $this->convert($row->download);
            $upload = $this->convert($row->upload);
            $ping = $row->ping_avg;
            $signal = $row->signal;
            $network = $row->Conntype->name;
            $technology = $this->technologyService->getTechnology($network);
            $findOperator = $this->operatorService->findOneById($row->mnc);
            $operator = isset($findOperator) ? $findOperator->brand : NULL;
            $this->measurement = $this->createResultContainer($source, $id, $dateTime, $latitude, $longitude, $download, $upload, $ping, $signal, $network, $technology, $operator, NULL, NULL);
        }
    }

    /**
     * Sets MBTest results to array.
     * @param Nette\Database\Table\Selection $selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setMbtestDataResults($selection) {
        foreach ($selection as $id => $row) {
            $dataType = $this->apiAction;
            $data = $this->getDataByApiAction($row, static::DATASET_MBTEST);
            $latitude = $row->latitude1;
            $longitude = $row->longitude1;
            $dateTime = $row->date_time->format('Y-m-d');
            $tid = $row->conntype;
            $oid = $row->mnc;
            $this->measurements[static::DATASET_MBTEST . '-' . $id] = $this->createResultsContainer($dataType, $data, $latitude, $longitude, $dateTime, $tid, $oid, NULL, NULL);
        }
    }

    /**
     * Sets related measurement from Open data.
     * @param \Nette\Database\Table\ActiveRow $row
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setOpenDataResult($row) {
        if ($row) {
            $source = 'OpenData';
            $id = $row->id;
            $dateTime = $row->time->format('d.m.Y H:i');
            $latitude = $row->latitude;
            $longitude = $row->longitude;
            $download = $row->download;
            $upload = $row->upload;
            $ping = $row->ping;
            $signal = $row->signal;
            $network = $row->Network->network;
            $technology = $row->Technology->technology;
            $operator = $row->Operator_id ? $row->Operator->brand : NULL;
            $model = $row->Model->model;
            $platform = $row->Platform->platform;
            $this->measurement = $this->createResultContainer($source, $id, $dateTime, $latitude, $longitude, $download, $upload, $ping, $signal, $network, $technology, $operator, $model, $platform);
        }
    }

    /**
     * Sets Open data results to array.
     * @param Nette\Database\Table\Selection $selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setOpenDataResults($selection) {
        foreach ($selection as $id => $row) {
            $dataType = $this->apiAction;
            $data = $this->getDataByApiAction($row, static::DATASET_OPENDATA);
            $latitude = $row->latitude;
            $longitude = $row->longitude;
            $dateTime = $row->time->format('Y-m-d');
            $tid = $row->Technology_id;
            $oid = $row->Operator_id;
            $mid = $row->Model_id;
            $pid = $row->Platform_id;
            $this->measurements[static::DATASET_OPENDATA . '-' . $id] = $this->createResultsContainer($dataType, $data, $latitude, $longitude, $dateTime, $tid, $oid, $mid, $pid);
        }
    }

}
