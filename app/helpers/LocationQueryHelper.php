<?php

namespace App\Helpers;

class LocationQueryHelper extends \Nette\Object {

    private $connection;
    private $possible_options = array("download", "upload", "ping_avg", "jitter", "packet_loss", "rtt_avg");
    private $possible_filters = array("device", "location_src1", "conntype", "datetime", "done", "forced");
    
    
    public function __construct(\Nette\Database\Context $db) {
        $this->connection = $db;
    }

    /**
     * ziska statistické informace o sloupci z databáze
     * 
     * @param type $city - id města
     * @param type $option - název databázového sloupce
     * @param type $where - dodatečné filtry
     */
    public function get_city_statistics($city, $option, $where) {
        $ret = array();

        if (!in_array($option, $this->possible_options)) {
            return $ret;
        }
        
        $where_str = "";
        
        foreach($where as $filter_key => $filter_val){
            if (in_array($filter_key, $this->possible_filters)) {
                $where_str += " AND " + $filter_key + " = " + $filter_val;
            }
        }

        $query = $this->connection->query('SELECT COUNT(measure.' . $option . ') as count, VARIANCE(measure.' . $option . ') as variance, STDDEV(measure.' . $option . ') as stddev, AVG(measure.' . $option . ') as average FROM measure '
                        . 'LEFT JOIN measure_locations ON measure.id = measure_locations.measure '
                        . 'LEFT JOIN locations ON measure_locations.location = locations.id '
                        . 'WHERE ' . $option . ' IS NOT NULL AND done = TRUE AND locations.city = ?'
                        . $where_str, $city)->fetchAll();
        
        if ($query[0]->count > 0) {
            $ret["option"] = $option;
            $ret["avg"] = $query[0]->average;
            $ret["count"] = $query[0]->count;
            $ret["variance"] = $query[0]->variance;
            $ret["stddev"] = $query[0]->stddev;
        }
        return $ret;
    }
    
    /**
     * ziska statistiky pro celý kraj
     * 
     * @param type $county - id kraje
     * @param type $option - db sloupec, jehoz statistiky chceme ziskat
     * @param type $where - dalsi filtry
     * @return array
     */
    public function get_county_statistics($county, $option, $where) {
        $ret = array();

        if (!in_array($option, $this->possible_options)) {
            return $ret;
        }

        $where_str = "";
        
        foreach($where as $filter_key => $filter_val){
            if (in_array($filter_key, $this->possible_filters)) {
                $where_str += " AND " + $filter_key + " = " + $filter_val;
            }
        }
        
        $query = $this->connection->query('SELECT COUNT(measure.' . $option . ') as count, VARIANCE(measure.' . $option . ') as variance, STDDEV(measure.' . $option . ') as stddev, AVG(measure.' . $option . ') as average FROM measure '
                        . 'LEFT JOIN measure_locations ON measure.id = measure_locations.measure '
                        . 'LEFT JOIN locations ON measure_locations.location = locations.id '
                        . 'LEFT JOIN cities ON locations.city = cities.id '
                        . 'WHERE ' . $option . ' IS NOT NULL AND done = TRUE AND cities.county = ?'
                        . $where_str, $county)->fetchAll();
        
        if ($query[0]->count > 0) {
            $ret["option"] = $option;
            $ret["avg"] = $query[0]->average;
            $ret["count"] = $query[0]->count;
            $ret["variance"] = $query[0]->variance;
            $ret["stddev"] = $query[0]->stddev;
        }
        return $ret;
    }
    
    
    /**
     * získá měření od určité geo lokace
     * 
     * @param type $lat - latitude
     * @param type $long - longitude
     * @param type $option - název databázového sloupce
     * @param type $where - dodatečné filtry
     * @param type $range - rozsah (0.001 ~ 110m)
     * @return array
     */
    public function get_location_statistics($lat, $long, $option, $where, $range = 0.001) {
        $ret = array();

        if (!in_array($option, $this->possible_options)) {
            return $ret;
        }
        
        $where_str = "";
        
        foreach($where as $filter_key => $filter_val){
            if (in_array($filter_key, $this->possible_filters)) {
                $where_str += " AND " + $filter_key + " = " + $filter_val;
            }
        }

        $query = $this->connection->query('SELECT COUNT(measure.' . $option . ') as count, VARIANCE(measure.' . $option . ') as variance, STDDEV(measure.' . $option . ') as stddev, AVG(measure.' . $option . ') as average FROM measure '
                        . 'WHERE ' . $option . ' IS NOT NULL AND '
                . 'latitude1 > ? AND longitude1 > ? AND latitude1 < ? AND longitude1 < ?'
                . $where_str, $lat - $range, $long - $range, $lat + $range, $long + $range)->fetchAll();
        
        if ($query[0]->count > 0) {
            $ret["avg"] = $query[0]->average;
            $ret["count"] = $query[0]->count;
            $ret["variance"] = $query[0]->variance;
            $ret["stddev"] = $query[0]->stddev;
        }
        return $ret;
    }

    /**
     * ziska statistiky pro cestu
     * 
     * @param type $coordinates - pole gps souradnic
     * @param type $option - nazev databazoveho sloupce, jehož statistiku chceme
     * @param type $where - dalsi filtry
     * @param type $range - přesnost (0.001 ~ 110m)
     * @return type
     */
    public function get_route_statistics($coordinates, $option, $where, $range = 0.001) {
        $ret = array();
        foreach($coordinates as $coord) {
            $ret[] = $this->get_location_statistics($coord["lat"], $coord["long"], $option, $where, $range);
        }
        return $ret;
    }
    
}
