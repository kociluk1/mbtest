<?php

namespace App\Helpers;

use Nette;
use Nette\Application;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;
use Nette\Utils\DateTime;
use App\Model;

/**
 * Mailer - Performs mails sending.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Mailer extends Nette\Object {

    /** @const ROBOT_MAILER Robot mailer email. */
    const ROBOT_MAILER = 'no-reply@mbtest.cz';

    /** @const ROBOT_MAILER_NAME Robot mailer name. */
    const ROBOT_MAILER_NAME = 'MBTest';
    
    /** @var Nette\Application\Application */
    private $app;

    /** @var \App\Model\UserRepository */
    private $userService;
    
    /** @var \App\Model\UsersRepository */
    private $usersService;
    
    /** @var Nette\Mail\SendmailMailer|SmtpMailer */
    private $mailer;

    /**
     * Constructor.
     * @param Nette\Application\Application $app
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @param Nette\Mail\SmtpMailer $mailer
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Application\Application $app, Model\UserRepository $userRepository, Model\UsersRepository $usersRepository, SmtpMailer $mailer) {
        $this->app = $app;
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;

        /** @todo Use default mailer. */
        //$this->mailer = new SendmailMailer();
        $this->mailer = $mailer;
    }

    /**
     * Creates mail message.
     * @param string email $from
     * @param string $name
     * @param string email $to
     * @param string $subject
     * @param string $body
     * @return Nette\Mail\Message
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function createMessage($from, $name, $to, $subject, $body) {
        $message = new Message();
        $message->setFrom($from, $name)
                ->addTo($to)
                ->setSubject($subject)
                ->setBody($body);

        return $message;
    }

    /**
     * Sends mail.
     * @param Nette\Mail\Message $message
     * @param string $flashMessage
     * @return void
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function sendMail($message, $flashMessage = NULL) {
        try {
            $this->mailer->send($message);
            $this->app->getPresenter()->flashMessage($flashMessage ? $flashMessage : 'E-mail byl úspěšně odeslán.', \App\Presenters\BasePresenter::FMT_SUCCESS);
        } catch (Exception $ex) {
            \Tracy\Debugger::log($ex, \Tracy\Debugger::ERROR);
            $this->app->getPresenter()->flashMessage('Došlo k nečekané chybě. Obraťte se na správce portálu.', \App\Presenters\BasePresenter::FMT_ERROR);
            return;
        } catch (\Nette\Mail\SmtpException $ex) {
            \Tracy\Debugger::log($ex, \Tracy\Debugger::ERROR);
            $this->app->getPresenter()->flashMessage('Došlo k nečekané chybě. Obraťte se na správce portálu.', \App\Presenters\BasePresenter::FMT_ERROR);
            return;
        }
    }

    /**
     * 
     * @param type $mailTo
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     * @todo 
     */
    public function sendResetToken($mailTo) {
        
    }

    /**
     * Sends authentication mail to setting new password.
     * @param string email $mailTo
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function sendAuthenticationMail($mailTo) {
        $user = $this->usersService->findByEmail($mailTo);
        $token = $this->userService->generateResetToken($user->id);
        $dt = new DateTime('+1 day');
        $url = $this->app->getPresenter()->link('//:Front:Sign:setNewPassword', $user->username, $token);
        $subject = 'MBTest - Nastavení nového hesla';
        
        /** @todo Make body in html. */
        $body = "Vážený uživateli, na základě Vaší žádosti Vám zasíláme odkaz pro obnovení hesla. "
                . "Odkaz je platný po dobu 24 hodin (do " . $dt->format('j.n. Y, H:i') . ") nebo do prvního přihlášení.\n\n"
                . "Odkaz pro obnovení hesla: " . $url;
        
        $message = $this->createMessage(static::ROBOT_MAILER, static::ROBOT_MAILER_NAME, $mailTo, $subject, $body);
        
        $flashMessage = 'Zpráva s pokyny pro nastavení nového hesla byla úspěšně odeslána na e-mailovou adresu uvedenou při registraci.';
        $this->sendMail($message, $flashMessage);
    }

}
