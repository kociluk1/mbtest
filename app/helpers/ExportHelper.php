<?php

namespace App\Helpers;

/**
 * basic export class
 */
class ExportHelper extends \Nette\Object {

    protected $dir;

    public function __construct($dir) {
        $this->dir = $dir;
    }

    /**
     * transforms post headers into database column names
     * @param type $headers
     * @return type
     */
    public static function get_headers($headers) {
        $headers_arr = array();
        foreach ($headers as $name => $header) {
            if ($header) {
                $name_clear = substr($name, 1, -1);
                switch ($name_clear) {
                    case "username" :
                        $headers_arr[] = "user_id";
                        break;
                    case "latlong" :
                        $headers_arr[] = "latitude1";
                        $headers_arr[] = "longitude1";
                        break;
                    case "ping" :
                        $headers_arr[] = "ping_min";
                        $headers_arr[] = "ping_max";
                        $headers_arr[] = "ping_avg";
                        break;
                    case "rtt" :
                        $headers_arr[] = "rtt_min";
                        $headers_arr[] = "rtt_max";
                        $headers_arr[] = "rtt_avg";
                        break;
                    default :
                        $headers_arr[] = $name_clear;
                        break;
                }
            }
        }
        return $headers_arr;
    }

    /**
     * exports data to csv file
     * @param type $filename
     * @param type $headers
     * @param type $data
     * @return boolean
     */
    public function saveCSV($filename, $headers, $data) {
        if (empty($data)) {
            return false;
        }

        $out = fopen($this->dir . '/' . $filename, 'w');
        fputcsv($out, $headers);
        foreach ($data as $d) {
            $d_arr = array();

            foreach ($headers as $h) {
                switch ($h) {
                    case "user_id":
                        $user = $d->ref("users", "user_id");
                        if ($user) {
                            $d_arr[] = $user->username;
                        } else {
                            $d_arr[] = '';
                        }
                        break;
                    case "device":
                        $device = $d->ref("devices", "device");
                        if ($device) {
                            $d_arr[] = $device->model;
                        } else {
                            $d_arr[] = '';
                        }
                        break;
                    case "conntype":
                        $conntype = $d->ref("conntype", "conntype");
                        if ($conntype) {
                            $d_arr[] = $conntype->name;
                        } else {
                            $d_arr[] = '';
                        }
                        break;
                    default:
                        $d_arr[] = $d->$h;
                }
            }

            fputcsv($out, $d_arr);
        }
        fclose($out);
        return true;
    }

    /**
     * exports data to xml
     * @param type $filename
     * @param type $headers
     * @param type $data
     * @return boolean
     */
    public function saveXML($filename, $headers, $data, $to_file = true) {
        if (empty($data)) {
            return false;
        }
        
        $xml = new \DOMDocument();
        $xml_export = $xml->createElement("export");

        foreach ($data as $d) {
            $xml_measure = $xml->createElement("measure");
            foreach ($headers as $h) {
                switch ($h) {
                    case "user_id":
                        $xml_attr = $xml->createElement("username");
                        $user = $d->ref("users", "user_id");
                        if ($user) {
                            $xml_attr->nodeValue = $user->username;
                        } else {
                            $xml_attr->nodeValue = '';
                        }
                        $xml_measure->appendChild($xml_attr);
                        break;
                    case "device":
                        $xml_attr = $xml->createElement("device");
                        $device = $d->ref("devices", "device");
                        if ($device) {
                            $xml_attr->nodeValue = $device->model;
                        } else {
                            $xml_attr->nodeValue = '';
                        }
                        $xml_measure->appendChild($xml_attr);
                        break;
                    case "conntype":
                        $xml_attr = $xml->createElement("conntype");
                        $conntype = $d->ref("conntype", "conntype");
                        if ($conntype) {
                            $xml_attr->nodeValue = $conntype->name;
                        } else {
                            $xml_attr->nodeValue = '';
                        }
                        $xml_measure->appendChild($xml_attr);
                        break;
                    default:
                        $xml_attr = $xml->createElement($h);
                        $xml_attr->nodeValue = $d->$h;
                        $xml_measure->appendChild($xml_attr);
                }
            }
            $xml_export->appendChild($xml_measure);
        }
        $xml->appendChild($xml_export);
        
        if ($to_file) {
            $xml->save($this->dir . '/' . $filename);
        } else {
            return $xml->saveXML();
        }
    }

    /**
     * exports data into all types zips them and saves
     * 
     * @param type $filename
     * @param type $baseUrl
     * @return boolean
     */
    public function saveZIP($filename, $headers, $data) {
        
        $this->saveCSV($filename . ".csv", $headers, $data);
        $xml = $this->saveXML($filename . ".xml", $headers, $data, false);
        
        $zip = new \ZipArchive();
        if ($zip->open($this->dir . '/' . $filename, \ZipArchive::CREATE) !== TRUE) {
            return false;
        }
        $zip->addFile($this->dir . "/" . $filename . ".csv", $filename . ".csv");
        $zip->addFromString($filename . ".xml", $xml);
        $zip->close();
        return true;
    }

    /**
     * exports data to xml
     * @param type $filename
     * @param type $headers
     * @param type $data
     * @return boolean
     */
    public function saveDevice($filename, $data) {
        if (empty($data)) {
            return false;
        }
        $xml = new \DOMDocument();
        $xml_export = $xml->createElement("sce");

        $xml_name = $xml->createElement("name");
        $xml_name->nodeValue = "export dat proveden " + time();
        $xml_export->appendChild($xml_name);
        
        $xml_desc = $xml->createElement("desc");
        $xml_desc->nodeValue = "export dat pro speciální zařízení";
        $xml_export->appendChild($xml_desc);

        foreach ($data as $d) {
            $xml_rule = $xml->createElement("rule");

            $xml_dlrate = $xml->createElement("dlrate");
            $xml_dlrate->nodeValue = $d->download;
            $xml_rule->appendChild($xml_dlrate);
            
            $xml_dldelay = $xml->createElement("dldelay");
            $xml_dldelay->nodeValue = $d->ping_avg;
            $xml_rule->appendChild($xml_dldelay);
            
            $xml_dljitter = $xml->createElement("dljitter");
            $xml_dljitter->nodeValue = $d->jitter;
            $xml_rule->appendChild($xml_dljitter);
            
            $xml_dlloss = $xml->createElement("dlloss");
            $xml_dlloss->nodeValue = $d->packet_loss;
            $xml_rule->appendChild($xml_dlloss);
            
            $xml_uprate = $xml->createElement("uprate");
            $xml_uprate->nodeValue = $d->upload;
            $xml_rule->appendChild($xml_uprate);
            
            $xml_updelay = $xml->createElement("updelay");
            $xml_updelay->nodeValue = $d->ping_avg;
            $xml_rule->appendChild($xml_updelay);
            
            $xml_upjitter = $xml->createElement("upjitter");
            $xml_upjitter->nodeValue = $d->jitter;
            $xml_rule->appendChild($xml_upjitter);
            
            $xml_uploss = $xml->createElement("uploss");
            $xml_uploss->nodeValue = $d->packet_loss;
            $xml_rule->appendChild($xml_uploss);
            
            $xml_time = $xml->createElement("time");
            $xml_time->nodeValue = $d->client_time2 - $d->client_time1;
            $xml_rule->appendChild($xml_time);

            $xml_export->appendChild($xml_rule);
        }
        $xml->appendChild($xml_export);

        $xml->save($this->dir . '/' . $filename);
    }

}
