<?php

namespace App\Helpers;

use App\Exceptions;
use App\Model;
use App\Presenters\BasePresenter as BP;
use Nette;
use Nette\Application\Application;
use Tracy\Debugger;

/**
 * OSMImportHelper
 * Loads and parse CSV files and strores into the database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OSMImportHelper extends Nette\Object {

    /** @var Nette\Application\Application */
    private $app;

    /** @var array Array with CSV data. */
    private $data;

    /** @var string Directory to store CSV source file. */
    private $dir;

    /** @var int Counter. */
    private $counter;

    /** @var string File name. */
    private $file;

    /** @var \App\Model\NodeRepository */
    private $nodeService;

    /** @var int ID of relation. */
    private $relationId;

    /** @var \App\Model\Relation_has_NodeRepository */
    private $relationHasNodeService;

    /** @var \App\Model\Relation_has_TestRepository */
    private $relationHasTestService;

    /**
     * Constructor.
     * @param string $dataDir
     * @param Nette\Application\Application
     * @param \App\Model\NodeRepository $nodeRepository
     * @param \App\Model\Relation_has_NodeRepository $relationHasNodeRepository
     * @param \App\Model\Relation_has_TestRepository $relationHasTestRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct($dataDir, Application $app, Model\NodeRepository $nodeRepository, Model\Relation_has_NodeRepository $relationHasNodeRepository, Model\Relation_has_TestRepository $relationHasTestRepository) {
        $this->app = $app;
        $this->counter = 0;
        $this->dir = $dataDir;
        $this->nodeService = $nodeRepository;
        $this->relationHasNodeService = $relationHasNodeRepository;
        $this->relationHasTestService = $relationHasTestRepository;
    }

    /**
     * Imports data from the CSV file to the database.
     * @param string $file
     * @param int $relationId
     * @throws \App\Exceptions\FileNotFoundException
     * @throws \App\Exceptions\ParseException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function importCSV($file, $relationId) {
        $this->file = $file;
        $this->relationId = $relationId;

        $this->load();
        $this->parse();
        $this->store();
    }

    /**
     * Loads the CSV file from data folder.
     * @throws Exceptions\FileNotFoundException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function load() {
//        echo "[1] loading - started\n";
        if (file_exists($this->getFilePath())) {
//            echo "    loading - ok\n";
        } else {
//            echo "loading - failed\n";
            throw new Exceptions\FileNotFoundException('Loading file failed.');
        }
    }

    /**
     * Parses CSV data to the array.
     * @throws \App\Exceptions\ParseException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function parse() {
//        echo "[2] parsing - started\n";
        $this->data = array_map('str_getcsv', file($this->getFilePath()));
        if ($this->data) {
//            echo "    parsing - ok\n";
        } else {
//            echo "parsing - failed\n";
            throw new Exceptions\ParseException('Parsing csv failed.');
        }
    }

    /**
     * Stores data in the database.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function store() {
        $this->deleteOldRecords();
//        echo "[3] storing - started\n";
        Debugger::timer();
        $checker = 0; // check that each hundred of rows will be printed only once

        foreach ($this->data as $key => $csvRow) {
            if ($key > 0) {
                $this->storeRow($csvRow);
                if ($this->counter % 100 == 0 && $this->counter != $checker) {
                    $checker = $this->counter;
                }
            }
        }

        $totalTimeSec = (int) Debugger::timer();
        $totalTimeMin = (int) ($totalTimeSec / 60);

//        echo "    storing - ok\n";
//        echo "\nTotal time: \t" . ($totalTimeMin < 1 ? $totalTimeSec . " sec. " : $totalTimeMin . " min. ") . "\n";
        $this->printInfo();
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns file path.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getFilePath() {
        return $this->dir . '/' . $this->file;
    }

    /**
     * Deletes old table records.
     */
    private function deleteOldRecords() {
        // deletes old relationHasNode records
        $this->relationHasNodeService->delete($this->relationId);
        // deletes old relationHasNode records
        $this->relationHasTestService->delete($this->relationId);
    }

    /**
     * Prints info about number of stored rows.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function printInfo() {
        $this->app->getPresenter()->flashMessage('Uloženo ' . $this->counter . ' bodů do databáze.', BP::FMT_SUCCESS);
    }

    /**
     * Stores data (row by row) in the database.
     * @param array $csvRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeRow(array $csvRow) {
        $nodeId = $csvRow[0];

        if (!$this->checkDuplicateData($nodeId)) {
            $this->nodeService->insert(array(
                'id' => $nodeId,
                'latitude' => $csvRow[1],
                'longitude' => $csvRow[2]
            ));

            $this->counter++;
        }

        $this->relationHasNodeService->insertOrUpdate(array(
            'Relation_id' => $this->relationId,
            'Node_id' => $nodeId
        ));
    }

    /**
     * Checks whether the test exists in the database.
     * @param int $nodeId
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function checkDuplicateData($nodeId) {
        if ($this->nodeService->findOneById($nodeId)) {
            return TRUE; // test row already exists
        }

        return FALSE; // test row does not exists
    }

}
