<?php

namespace App\Helpers;

use App\Model;
use App\Exceptions;
use Nette;
use Nette\Utils\Strings;
use Tracy\Debugger;

/**
 * ImportHelper
 * Performs import of CSV data to the database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ImportHelper extends Nette\Object {

    /** @var string Directory to store CSV source file. */
    private $dir;

    /** @var string URL to CSV source file. */
    private $sourceUrl;

    /** @var string Path to CSV file. */
    private $csvName;

    /** @var string Path to ZIP archive. */
    private $zipName;

    /** @var array Array with CSV data. */
    private $data;

    /** @var int Test counter. */
    private $counter;

    /** @var \App\Model\Autonomous_systemRepository */
    private $asService;

    /** @var \App\Model\ClientRepository */
    private $clientService;

    /** @var \App\Model\IPRepository */
    private $ipService;

    /** @var \App\Model\ModelRepository */
    private $modelService;

    /** @var \App\Model\NetworkRepository */
    private $networkService;

    /** @var \App\Model\OperatorRepository */
    private $operatorService;

    /** @var \App\Model\PlatformRepository */
    private $platformService;

    /** @var \App\Model\TechnologyRepository */
    private $technologyService;

    /** @var \App\Model\TestRepository */
    private $testService;

    /** @var array Array with one row from CSV source file. */
    private $row = array();

    /** @var int Autonomous system id. */
    private $idAs;

    /** @var int Client id. */
    private $idClient;

    /** @var int IP id. */
    private $idIp;

    /** @var int Model id. */
    private $idModel;

    /** @var int Network id. */
    private $idNetwork;

    /** @var int Operator id. */
    private $idOperator;

    /** @var int Platform id. */
    private $idPlatform;

    /** @var int Technology id. */
    private $idTechnology;

    /** @var int Test id. */
    private $idTest;

    /**
     * Constructor.
     * @param string $importDir
     * @param string $importUrl
     * @param \App\Helpers\Model\Autonomous_systemRepository $asRepository
     * @param \App\Helpers\Model\ClientRepository $clientRepository
     * @param \App\Helpers\Model\IPRepository $ipRepository
     * @param \App\Helpers\Model\ModelRepository $modelRepository
     * @param \App\Helpers\Model\NetworkRepository $networkRepository
     * @param \App\Helpers\Model\OperatorRepository $operatorRepository
     * @param \App\Helpers\Model\PlatformRepository $platformRepository
     * @param \App\Helpers\Model\TechnologyRepository $technologyRepository
     * @param \App\Helpers\Model\TestRepository $testRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct($importDir, $importUrl, Model\Autonomous_systemRepository $asRepository, Model\ClientRepository $clientRepository, Model\IPRepository $ipRepository, Model\ModelRepository $modelRepository, Model\NetworkRepository $networkRepository, Model\OperatorRepository $operatorRepository, Model\PlatformRepository $platformRepository, Model\TechnologyRepository $technologyRepository, Model\TestRepository $testRepository) {
        $this->dir = $importDir;
        $this->sourceUrl = $importUrl;
        $this->csvName = $this->dir . '\opendata.csv';
        $this->zipName = $this->dir . '\NetMetr-opendata.zip';
        $this->counter = 0;
        $this->asService = $asRepository;
        $this->clientService = $clientRepository;
        $this->ipService = $ipRepository;
        $this->modelService = $modelRepository;
        $this->networkService = $networkRepository;
        $this->operatorService = $operatorRepository;
        $this->platformService = $platformRepository;
        $this->technologyService = $technologyRepository;
        $this->testService = $testRepository;
    }

    /**
     * Imports data from the CSV file to the database.
     * @throws \App\Exceptions\FileNotFoundException
     * @throws \App\Exceptions\ZipException
     * @throws \App\Exceptions\ParseException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function importCSV() {
        $this->download();
        $this->unzip();
        $this->parse();
        $this->store();
    }

    /**
     * Downloads the CSV file from netmetr.cz.
     * @throws \App\Exceptions\FileNotFoundException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function download() {
        echo "[1] download - started\n";
        $retVal = file_put_contents($this->zipName, file_get_contents($this->sourceUrl));

        if ($retVal) {
            echo "    download - ok\n";
        } else {
//            echo "download - failed\n";
            throw new Exceptions\FileNotFoundException('Download file failed.');
        }
    }

    /**
     * Unzips archive with the CSV file.
     * @throws \App\Exceptions\ZipException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function unzip() {
        echo "[2] unzip - started\n";
        $zip = new \ZipArchive();
        $retVal = $zip->open($this->zipName);
        if ($retVal === TRUE) {
            $zip->extractTo($this->dir . '/');
            $zip->close();
            echo "    unzip - ok\n";
        } else {
//            echo "unzip - failed\n";
            throw new Exceptions\ZipException('Unzip archive failed.', $retVal);
        }
    }

    /**
     * Parses CSV data to the array.
     * @throws \App\Exceptions\ParseException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function parse() {
        echo "[3] parsing - started\n";
        $this->data = array_map('str_getcsv', file($this->csvName));
        if ($this->data) {
            echo "    parsing - ok\n";
        } else {
//            echo "parsing - failed\n";
            throw new Exceptions\ParseException('Parsing csv failed.');
        }
    }

    /**
     * Stores data in the database.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function store() {
        echo "[4] storing - started\n";
        Debugger::timer();
        $checker = 0; // check that each hundred of rows will be printed only once
        
        foreach ($this->data as $key => $csvRow) {
            if ($key > 0) {
                $this->storeRow($csvRow);
                if ($this->counter % 100 == 0 && $this->counter != $checker) {
                    $this->printInfo();
                    $checker = $this->counter;
                }
            }
        }
        
        $totalTimeSec = (int) Debugger::timer();
        $totalTimeMin = (int) ($totalTimeSec / 60);
        
        echo "    storing - ok\n";
        echo "\nTotal time: \t" . ($totalTimeMin < 1 ? $totalTimeSec . " sec. " : $totalTimeMin . " min. ") . "\n";
        $this->printInfo();
        echo "\n";
    }
    
    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Stores data (row by row) in the database.
     * @param array $csvRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeRow(array $csvRow) {
        if ($this->chceckMandatoryData($csvRow) && !$this->checkDuplicateData($csvRow[1])) {
            $this->idTechnology = $this->technologyService->insertOrUpdate(array('technology' => $this->row['technology']));
            $this->idPlatform = $this->platformService->insertOrUpdate(array('platform' => $this->row['platform']));
            $this->idModel = $this->modelService->insertOrUpdate(array('model' => $this->row['model']));
            $this->idClient = $this->clientService->insertOrUpdate(array('client_version' => $this->row['client_version']));
            $this->idNetwork = $this->networkService->insertOrUpdate(array('network' => $this->row['network']));
            $this->idAs = $this->asService->insertOrUpdate(array('as_number' => $this->row['as_number']));
            $this->idIp = $this->ipService->insertOrUpdate(array('ip' => $this->row['ip'], 'is_ipv6' => strpos($this->row['ip'], ':') ? TRUE : FALSE));

            $this->storeOperatorData();
            $this->storeTestData();

            $this->counter++;
        }
    }

    /**
     * Checks mandatory data.
     * @param array $csvRow
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function chceckMandatoryData(array $csvRow) {
        //  1 - open test uuid
        //  2 - time (dd.mm.yyyy hh:mm)
        //  3 - cat technology (1G/2G/3G/4G/WLAN)
        //  4 - network type ((2G/3G)/(2G/4G)/EDGE/UTMS/HSPA+/HSDPA/LTE/WLAN)
        //  5 - latitude
        //  6 - longitude
        //  7* - location source
        //  8* - location accuracy
        // 10 - download (kbit)
        // 11 - upload (kbit)
        // 12 - ping (ms)
        // 17 - platform (Android/iOS)
        // 18 - model
        // 19 - client version
        // 20* - network - MCC & MNC
        // 24 - ASP - Autonomous system number
        // 25 - IP anonymized
        // 29* - signal strength
        // * not mandatory, but stored
        if ($csvRow[1] && $csvRow[2] && $csvRow[3] && $csvRow[4] && $csvRow[5] && $csvRow[6] && $csvRow[10] && $csvRow[11] && $csvRow[12] && $csvRow[17] && $csvRow[18] && $csvRow[19] && $csvRow[24] && $csvRow[25]) {
            $this->setDataToRow($csvRow);

            return TRUE; // all mandatory columns are filled
        }

        return FALSE; // some mandatory values are missing
    }

    /**
     * Sets data from CSV row to the array with string keys.
     * @param array $csvRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setDataToRow(array $csvRow) {
        $this->row = array(
            'open_test_uuid' => $csvRow[1],
            'time' => $csvRow[2],
            'technology' => $csvRow[3],
            'network' => $csvRow[4],
            'latitude' => $csvRow[5],
            'longitude' => $csvRow[6],
            'location_source' => $csvRow[7],
            'location_accuracy' => $csvRow[8],
            'download' => $csvRow[10],
            'upload' => $csvRow[11],
            'ping' => $csvRow[12],
            'platform' => $csvRow[17],
            'model' => $csvRow[18],
            'client_version' => $csvRow[19],
            'mcc_mnc' => is_null($csvRow[20]) ? $csvRow[22] : $csvRow[20], // use network MCC-MNC or sim MCC-MNC
            'as_number' => $csvRow[24],
            'ip' => $csvRow[25],
            'signal' => $csvRow[29]
        );
    }

    /**
     * Checks whether the test exists in the database.
     * @param int $testId
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function checkDuplicateData($testId) {
        if ($this->testService->findByOpenTestId($testId)) {
            return TRUE; // test row already exists
        }

        return FALSE; // test row does not exists
    }

    /**
     * Stores data about the operator in the database.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeOperatorData() {
        if ($this->row['mcc_mnc']) {
            $matches = Strings::matchAll($this->row['mcc_mnc'], '#\d+#');
            if (isset($matches[1][0])) {
                $match = Strings::match($matches[1][0], '#[1-9][0-9]*#');
                $operator = $this->operatorService->findBy(array('mnc' => $match[0]))->fetch();   
                $this->idOperator = $operator ? $operator->id : null;
            } else {
                $this->idOperator = NULL;
            }
        }
    }

    /**
     * Stores data to the Test table.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeTestData() {
        $this->testService->insert(array(
            'open_test_uuid' => $this->row['open_test_uuid'],
            'time' => $this->row['time'],
            'latitude' => $this->row['latitude'],
            'longitude' => $this->row['longitude'],
            'location_source' => $this->row['location_source'] ? $this->row['location_source'] : NULL,
            'location_accuracy' => $this->row['location_accuracy'] ? $this->row['location_accuracy'] : NULL,
            'download' => $this->row['download'],
            'upload' => $this->row['upload'],
            'ping' => $this->row['ping'],
            'signal' => $this->row['signal'] ? $this->row['signal'] : NULL,
            'Technology_id' => $this->idTechnology,
            'Platform_id' => $this->idPlatform,
            'Model_id' => $this->idModel,
            'IP_id' => $this->idIp,
            'Client_id' => $this->idClient,
            'Network_id' => $this->idNetwork,
            'Autonomous_system_id' => $this->idAs,
            'Operator_id' => $this->idOperator
        ));
    }

    /**
     * Prints info about number of stored rows.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function printInfo() {
        echo "Rows stored: \t" . $this->counter . "\n";
    }

}
