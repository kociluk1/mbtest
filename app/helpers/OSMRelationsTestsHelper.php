<?php

namespace App\Helpers;

use App\Model;
use Nette;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;
use Tracy\Debugger;

/**
 * OSMRelationsTestsHelper
 * Performs finding and storing of new tests which belongs to a relation.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OSMRelationsTestsHelper extends Nette\Object {

    /** @const DEFAULT_RADIUS_HIGHWAY Default radius for finding tests on motorways. */
    const DEFAULT_RADIUS_HIGHWAY = 250;

    /** @const DEFAULT_RADIUS_RAILWAY Default radius for finding tests on railways. */
    const DEFAULT_RADIUS_RAILWAY = 100;
    
    /** @var array List of nodes. */
    private $nodes = array();

    /** @var \App\Model\NodeRepository */
    private $nodeService;

    /** @var int Relation feature. */
    private $relationFeature;
    
    /** @var int Relation id. */
    private $relationId;

    /** @var string Time of last update of relation. */
    private $relationLastUpdate;

    /** @var \App\Model\RelationRepository */
    private $relationService;

    /** @var \App\Model\Relation_has_NodeRepository */
    private $relationHasNodeService;

    /** @var \App\Model\Relation_has_TestRepository */
    private $relationHasTestService;

    /** @var \App\Model\TestRepository */
    private $testService;

    /**
     * Constructor.
     * @param \App\Model\NodeRepository $nodeRepository
     * @param \App\Model\RelationRepository $relationRepository
     * @param \App\Model\Relation_has_NodeRepository $relationHasNodeService
     * @param \App\Model\Relation_has_TestRepository $relationHasTestRepository
     * @param \App\Model\TestRepository $testRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Model\NodeRepository $nodeRepository, Model\RelationRepository $relationRepository, Model\Relation_has_NodeRepository $relationHasNodeService, Model\Relation_has_TestRepository $relationHasTestRepository, Model\TestRepository $testRepository) {
        $this->nodeService = $nodeRepository;
        $this->relationService = $relationRepository;
        $this->relationHasNodeService = $relationHasNodeService;
        $this->relationHasTestService = $relationHasTestRepository;
        $this->testService = $testRepository;
    }

    /**
     * Stores tests by relation.
     * @param int $relationId
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function storeTestsByRelation($relationId) {
        $this->relationId = $relationId;
        $this->relationLastUpdate = $this->getRelationLastUpdate();
        $this->relationFeature = $this->getRelationFeature();
        $this->findTests();
    }

    /**
     * Stores tests by all relations.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function storeTestsByAllRelations() {
        echo "[1] relations tests finding - started at " . date('H:i') . "\n";
        $relations = $this->relationService->findAll();
        $counter = 0;
        Debugger::timer('total');
        foreach ($relations as $key => $row) {
            $counter++;
            echo "[1." . $counter . "] relation - " . $row->Feature->value_en . " - " . Strings::webalize($row->name) . " - started\n";
            Debugger::timer('relation');
            $this->storeTestsByRelation($key);
            $relationTimeSec = (int) Debugger::timer('relation');
            $relationTimeMin = (int) ($relationTimeSec / 60);
            echo "[1." . $counter . "] relation - " . $row->Feature->value_en . " - " . Strings::webalize($row->name) . " - completed in " . ($relationTimeMin > 0 ? $relationTimeMin . " min\n" : $relationTimeSec . " sec\n");
            $this->relationService->update($this->relationId, array('last_update' => DateTime::from('today')));
        }

        $totalTimeSec = (int) Debugger::timer('total');
        $totalTimeMin = (int) ($totalTimeSec / 60);

        echo "\nTotal time: \t" . ($totalTimeMin < 1 ? $totalTimeSec . " sec. " : $totalTimeMin . " min. ") . "\n";
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns feature of relation.
     * @return string
     */
    private function getRelationFeature() {
        return $this->relationService->findOneById($this->relationId)->Feature->key;
    }
    
    /**
     * Returns time of last update of relation.
     * @return string
     */
    private function getRelationLastUpdate() {
        $lastUpdate = $this->relationService->findOneById($this->relationId);
        return $lastUpdate ? $lastUpdate->last_update : '0000-00-00 00:00:00';
    }

    /**
     * Finds nodes which belongs to relation.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function findNodes() {
        $this->nodes = array();
        foreach ($this->relationHasNodeService->findBy(array('Relation_id' => $this->relationId)) as $relationHasNode) {
            $this->nodes[] = array(
                'latitude' => $relationHasNode->Node->latitude,
                'longitude' => $relationHasNode->Node->longitude
            );
        }
    }

    /**
     * Finds tests which belongs to relation.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function findTests() {
        $this->findNodes();
        foreach ($this->nodes as $key => $row) {
            if ($key > 0) {
                $tests = $this->testService->findByCoords($row['latitude'], $row['longitude'], $this->relationFeature === Model\FeatureRepository::HIGHWAY ? static::DEFAULT_RADIUS_HIGHWAY : static::DEFAULT_RADIUS_RAILWAY, $this->relationLastUpdate);
                $this->storeTests($tests);
            }
        }
    }

    /**
     * Stores tests which belongs to relation into the database.
     * @param array $tests
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function storeTests($tests) {
        foreach ($tests as $test) {
            $this->relationHasTestService->insertOrUpdate(array('Relation_id' => $this->relationId, 'Test_id' => $test['id']));
        }
    }

}
