<?php

namespace App\Exceptions;

/**
 * *************************************************************************
 * Runtime Exceptions ******************************************************
 * *************************************************************************
 */

/**
 * The exception that is thrown when a method call is invalid for the object's
 * current state, method has been invoked at an illegal or inappropriate time.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class InvalidStateException extends \RuntimeException {
    
}

/**
 * The exception that is thrown when parse error occurs.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ParseException extends \RuntimeException {
    
}

/**
 * The exception that is thrown when an I/O error occurs.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class IOException extends \RuntimeException {
    
}

/**
 * The exception that is thrown when accessing a file that does not exist on disk.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class FileNotFoundException extends IOException {
    
}

/**
 * The exception that is thrown when opening zip failed.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ZipException extends IOException {
    
}

/**
 * The exception that is thrown when part of a file or directory cannot be found.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class DirectoryNotFoundException extends IOException {
    
}

/**
 * The exception that is thrown when a value (typically returned by function) does not match with the expected value.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class UnexpectedValueException extends \UnexpectedValueException {
    
}

/**
 * *************************************************************************
 * Logic Exceptions ********************************************************
 * *************************************************************************
 */

/**
 * The exception that is thrown when an argument does not match with the expected value.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class InvalidArgumentException extends \InvalidArgumentException {
    
}