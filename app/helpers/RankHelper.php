<?php

namespace App\Helpers;

use App\Model;
use Nette;
use Nette\Security\User;

/**
 * RankHelper
 * Performs checking permissions and calculating score.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RankHelper extends Nette\Object {

    /** @const RANK_SCORE_0 */
    const RANK_SCORE_0 = 0;

    /** @const RANK_SCORE_1 */
    const RANK_SCORE_1 = 10;

    /** @const RANK_SCORE_2 */
    const RANK_SCORE_2 = 100;

    /** @const RANK_SCORE_3 */
    const RANK_SCORE_3 = 1000;

    /** @const RANK_SCORE_4 */
    const RANK_SCORE_4 = 10000;

    /** @const RANK_SCORE_5 */
    const RANK_SCORE_5 = 100000;

    /** @conts RANKS_PATH Path to ranks images. */
    const RANKS_PATH = '/images/ranks/';

    /** @var array Prsenters. */
    private $presenters = array();

    /** @var Nette\Security\User */
    private $user;

    /** @var \App\Model\MeasureRepository */
    private $measureService;

    /** @var \App\Model\UsersRepository */
    private $usersService;

    /**
     * Constructor.
     * @param Nette\Security\User $user
     * @param \App\Model\MeasureRepository $measureRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(User $user, Model\MeasureRepository $measureRepository, Model\UsersRepository $usersRepository) {
        $this->user = $user;
        $this->measureService = $measureRepository;
        $this->usersService = $usersRepository;
        $this->presenters = array(0 => 'Front:Map', 1 => 'Front:MyData', 2 => 'Front:Statistics');
    }

    /**
     * Calculates additional points to score.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function calculateScore() {
        $users = $this->measureService->findPeriodicallyMeasuringUsers();
        foreach ($users as $userId => $score) {
            $this->usersService->updateScore($userId, $this->calculateScoreBonus($score));
        }
    }

    /**
     * Checks whether the user has premissions.
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function checkScorePermissions($presenter) {
        if ($this->user->isInRole(Model\Authorizator::ROLE_USER)) {
            $score = $this->getScore();
            switch (TRUE) {
                case $score > static::RANK_SCORE_5:
                case $score > static::RANK_SCORE_4 && $presenter !== $this->presenters[2]:
                case $score > static::RANK_SCORE_3 && $presenter !== $this->presenters[2] && $presenter !== $this->presenters[1]:
                case $score > static::RANK_SCORE_2 && $presenter !== $this->presenters[2] && $presenter !== $this->presenters[1]:
                case $score > static::RANK_SCORE_1 && $presenter !== $this->presenters[2] && $presenter !== $this->presenters[1]:
                    return TRUE;
                default:
                    return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * Returns rank picture.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getRankPicture() {
        $score = $this->getScore();
        switch (TRUE) {
            case $score > static::RANK_SCORE_5:
                $rankImage = 'signal_5-128x128.png';
                break;
            case $score > static::RANK_SCORE_4:
                $rankImage = 'signal_4-128x128.png';
                break;
            case $score > static::RANK_SCORE_3:
                $rankImage = 'signal_3-128x128.png';
                break;
            case $score > static::RANK_SCORE_2:
                $rankImage = 'signal_2-128x128.png';
                break;
            case $score > static::RANK_SCORE_1:
                $rankImage = 'signal_1-128x128.png';
                break;
            default:
                $rankImage = 'no_signal-128x128.png';
                break;
        }

        return static::RANKS_PATH . $rankImage;
    }

    /**
     * Returns user score.
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getScore() {
        return $this->usersService->findOneById($this->user->id)->score;
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Calculates score bonus for periodically measurements.
     * @param int $score
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function calculateScoreBonus($score) {
        switch (TRUE) {
            case $score === 30:
                return 30;
            case $score > 20:
                return 15;
            case $score >= 15:
                return 10;
            default:
                return 0;
        }
    }

}
