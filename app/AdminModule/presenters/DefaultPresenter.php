<?php

/**
 * default admin presenter
 * author: Lukáš Sokol
 */

namespace App\AdminModule\Presenters;

use Nette\Application\UI,
    Nette\Application\Responses\JsonResponse;

class DefaultPresenter extends \App\Presenters\BasePresenter {

    private $settings;

    /**
     * injects database objects needed
     * 
     * @param \App\Model\General_settingsRepository $settings
     */
    public function injectUserRepository(\App\Model\General_settingsRepository $settings) {
        $this->settings = $settings;
    }

    protected function beforeRender() {
        parent::beforeRender();

        // todo check if user has appropriete rights to do this
 
        $httpRequest = $this->getHttpRequest();
        
        // handles various ajax requests
        $this->handleAjax($httpRequest);
        
        // sets basic template variables
        $this->template->settings = $this->settings->findAll()->fetchAll();
        $this->template->action = "general";
    }
    
    /**
     * ajax handling function
     * 
     * @param $httpRequest
     */
    protected function handleAjax($httpRequest) {
        
        if ($httpRequest->isAjax()) {
            $post = $httpRequest->getPost();

            // save settings request
            if (isset($post["saveSettings"])) {
                $response = array();
                $err = false;

                // update all settings
                foreach ($post["settings"] as $id => $value) {
                    if (!$this->settings->update($id, array('value' => $value))) {
                        $err = true;
                    }
                }
                
                // reponse code
                if (!$err) {
                    $response["status"] = "100";
                    $response["msg"] = "Základní nastavení úspěšně změněno";
                } else {
                    $response["status"] = "300";
                    $response["msg"] = "Chyba při ukládání základního nastavení";
                }
                
                // send response as JSON
                $this->sendResponse(new JsonResponse($response));
            } 
        }
    }
    
}