<?php

namespace App\AdminModule\Presenters;

use Nette\Application\Responses\JsonResponse,
    Libs\Polyline;

class ExportPresenter extends \App\Presenters\BasePresenter {

    private $exporthelper;
    private $measure;
    private $location;

    public function injectCSVhelper(\App\Helpers\ExportHelper $export, \App\Model\MeasureRepository $measure, \App\Helpers\LocationQueryHelper $loc) {
        $this->exporthelper = $export;
        $this->measure = $measure;
        $this->location = $loc;
    }

    protected function beforeRender() {
        parent::beforeRender();

        // todo check if user has appropriete rights

        $httpRequest = $this->getHttpRequest();

        // handles various ajax requests
        $this->handleAjax($httpRequest);

        // sets basic template variables
        $this->template->action = "export";
        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<br>";
        //\Tracy\Dumper::dump($this->location->get_location_statistics(50.0754, 14.45, "download", 0.001));
        \Tracy\Dumper::dump(Polyline::Decode("q~occB{}brX}KaHyY{QaW}OoN{IoHuEcIcFeM}HoKyGuBsAZ}Ip@aUXaZIqT_@sUi@kNgCk`@wAePsCwTwDuTsG}YwIsZgFuMmB_F_HmQ}NoVgFyGoCsDmK}KgLaJuHeEgEyCiHcFuEeDwFGmIKoQW{JwBwHeFmFmDePcNuJqIwBmByg@ec@{EiEmU}RoGkDwPoGeD|K{U`z@_Vpy@}\\pbAsJ}B}NqCyEy@yI}A_WeFeSyEqToJu^_Nmv@ge@{MyI}TcJkTeGcg@oMgCq@"));
    }

    protected function handleAjax($httpRequest) {

        if ($httpRequest->isAjax()) {
            $post = $httpRequest->getPost();

            // export requests
            if (isset($post["export"]) and isset($post["export-type"])) {

                $opendata = $post["opendata"];
                $params = $post["param"];
                $limit = 10;

                $headers = \App\Helpers\ExportHelper::get_headers($params);
                $filename = "export-" . time() . ".";
                $type = $post["export-type"];
                $response = array();

                $measures = $this->measure->findAll()->where('done = ? AND latitude1 > 0 AND latitude2 > 0', true)->limit($limit);

                if ($type == 'csv') {
                    $this->exporthelper->saveCSV($filename . $type, $headers, $measures);

                    $response["file"] = $httpRequest->url->baseUrl . "export/" . $filename . $type;
                    $response["type"] = "csv";
                    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($response));
                } else if ($type == 'xml') {
                    $this->exporthelper->saveXML($filename . $type, $headers, $measures);

                    $response["file"] = $httpRequest->url->baseUrl . "export/" . $filename . $type;
                    $response["type"] = "xml";
                    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($response));
                } else if ($type == 'zip') {
                    $this->exporthelper->saveZIP($filename . $type, $headers, $measures);

                    $response["file"] = $httpRequest->url->baseUrl . "export/" . $filename . $type;
                    $response["type"] = "zip";
                    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($response));
                } else if ($type == 'device') {
                    $this->exporthelper->saveDevice("device-" . $filename . "xml", $measures);

                    $response["file"] = $httpRequest->url->baseUrl . "export/device-" . $filename . "xml";
                    $response["type"] = "xml";
                    $this->sendResponse(new \Nette\Application\Responses\JsonResponse($response));
                }
            }
        }
    }

}
