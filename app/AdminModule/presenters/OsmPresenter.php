<?php

namespace App\AdminModule\Presenters;

use App\Forms;
use Nette;

/**
 * OsmPresenter
 * Presenter for loading OSM data to the database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OsmPresenter extends \App\Presenters\BasePresenter {
    
    /** @var \App\Forms\LoadOSMImportForm */
    private $osmImportFormFactory;
    
    /**
     * Injects required forms.
     * @param \App\Forms\OSMImportForm $osmImportForm
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectOsmForms(Forms\OSMImportForm $osmImportForm) {
        $this->osmImportFormFactory = $osmImportForm;
    }

    /**
     * *************************************************************************
     * Components **************************************************************
     * *************************************************************************
     */
    
    /**
     * Creates component OSM form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentOsmImportForm() {
        return $this->osmImportFormFactory->create();
    }
    
}
