<?php

namespace App\AdminModule\Presenters;

use Nette\Application\Responses\JsonResponse;

/**
 * presenter handling rights change
 */
class RightsPresenter extends \App\Presenters\BasePresenter {

    private $users;
    private $users_remote;
    
    /**
     * injects all database models needed
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $users_remote
     */
    public function injectUserRepository(\App\Model\UserRepository $userRepository, \App\Model\UsersRepository $users_remote) {
        $this->users = $userRepository;
        $this->users_remote = $users_remote;
    }
    
    protected function beforeRender() {
        parent::beforeRender();

        // todo check if user has appropriete rights

        $httpRequest = $this->getHttpRequest();

        // handles various ajax requests
        $this->handleAjax($httpRequest);

        // sets basic template variables
        $this->template->users = $this->users_remote->findAll()->limit(10)->fetchAll();
        $this->template->action = "rights";
    }

    /**
     * ajax calls handling function
     * @param type $httpRequest
     */
    private function handleAjax($httpRequest) {
        if ($httpRequest->isAjax()) {
            $post = $httpRequest->getPost();
            
            if (isset($post["searchUsers"])) {
                $foundusers = array();

                foreach ($this->users_remote->searchUser("LOWER(username) LIKE ? OR email LIKE ?", $post["query"])->limit(10)->fetchAll() as $user) {
                    $props = array();
                    $props["username"] = $user->username;
                    $props["id"] = $user->id;
                    $props["email"] = ($user->email == "" or $user->email == "NULL") ? "nezadán" : $user->email;
                    $foundusers[] = $props;
                }

                $this->sendResponse(new JsonResponse($foundusers));

                // change rights of a user request
            } else if (isset($post["changeRights"])) {
                $response = array();
                $err = false;
                
                $user = $this->users->findOneById($post["userid"]);
                $user_remote = $this->users_remote->findOneById($post["userid"]);
                if (!$user) {
                    $response["status"] = "300";
                    $response["msg"] = "Takový uživatel neexistuje";
                } else {
                    // @todo change right here
                    $newright = $post["right"];
                    
                    
                    $response["status"] = "100";
                    $response["msg"] = "Práva uživatele " . $user_remote->username . " byla změněna na " . $newright;
                }
                
                $this->sendResponse(new JsonResponse($response));
            }
        }
    }

}
