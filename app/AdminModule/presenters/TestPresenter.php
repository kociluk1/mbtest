<?php

namespace App\AdminModule\Presenters;

use App\Model;
use Nette;

/**
 * TestPresenter
 * Presenter for adding TEST data to the database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class TestPresenter extends \App\Presenters\BasePresenter {

    /** @var \App\Model\MeasureRepository */
    private $measureService;

     /** @var \App\Model\UsersRepository */
    private $usersService;
    
    /**
     * Injects required repositories.
     * @param \App\Model\MeasureRepository $measureRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectTestRepositories(Model\MeasureRepository $measureRepository, Model\UsersRepository $usersRepository) {
        $this->measureService = $measureRepository;
        $this->usersService = $usersRepository;
    }

    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Default action.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {
        
    }

    /**
     * *************************************************************************
     * Handlers ****************************************************************
     * *************************************************************************
     */

    /**
     * Inserts random data to database.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function handleInsertMeasurements() {
        for ($i = 0; $i < 10; $i++) {
            $data = $this->getData();
            $measure = $this->measureService->insert($data);
            dump($measure->id);
            dump($measure);
            dump($data);
        }
    }

    /**
     * Deletes all users measurements.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function handleDeleteMeasurements() {
        $this->measureService->findBy(array('user_id' => $this->user->id))->delete();
        $this->usersService->findOneById($this->user->id)->update(array('score' => 0));
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns input data for inserting to the database.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getData() {
        $user_id = $this->user->id;
        $tcp_id = 1;
        $udp_id = 1;
        $ip = '127.0.0.0';
        $mnc = 1;
        $mcc = 230;

        $latNorth = 50.108593;
        $latSouth = 50.107145;
        $lngWest = 14.391178;
        $lngEast = 14.394327;

        return array(
            'date_time' => '2015-11-05 12:20:08',
            'user_id' => $user_id,
            'tcp_id' => $tcp_id,
            'udp_id' => $udp_id,
            'ip' => $ip,
            'mnc' => $mnc,
            'mcc' => $mcc,
            'device' => $this->getRandomPhone(),
            'battery' => $this->getRandomNumber(),
            'conntype' => $this->getRandomTechnology(),
            'signal' => '-' . $this->getRandomNumber(40, 130),
            'latitude1' => $this->getRandomCoords($latSouth, $latNorth),
            'longitude1' => $this->getRandomCoords($lngWest, $lngEast),
            'ping_avg' => $this->getRandomNumber(1, 150),
            'download' => $this->getRandomNumber(400, 15000000),
            'upload' => $this->getRandomNumber(200, 10000000)
        );
    }

    /**
     * Generates random coordinate between input coordinates.
     * @param double $coord1
     * @param double $coord2
     * @return double
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getRandomCoords($coord1, $coord2) {
        $round = 1000000;

        $min = $coord1 * $round;
        $max = $coord2 * $round;
        $coord = mt_rand($min, $max);
        return $coord / $round;
    }

    /**
     * Generates random number.
     * @param int $min
     * @param int $max
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getRandomNumber($min = 1, $max = 100) {
        return mt_rand($min, $max);
    }

    /**
     * Generates random phone model.
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getRandomPhone() {
        $device_iphone = 22;
        $device_galaxy = 32;

        $rand = mt_rand(1, 2);
        if ($rand === 1) {
            return $device_iphone;
        } else {
            return $device_galaxy;
        }
    }

    /**
     * Generates random technology.
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getRandomTechnology() {
        $conntype_edge = 35;
        $conntype_3g = 38;
        $conntype_lte = 40;

        $rand = mt_rand(1, 3);
        if ($rand === 1) {
            return $conntype_edge;
        } elseif ($rand === 2) {
            return $conntype_3g;
        } else {
            return $conntype_lte;
        }
    }

}
