<?php

namespace App\FrontModule\Presenters;

use App\Helpers;
use App\Model;
use Nette;

/**
 * DashboardPresenter
 * Presenter for base info about account.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class DashboardPresenter extends \App\Presenters\BasePresenter {

    /** @var \App\Model\MeasureRepository */
    private $measureService;

    /** @var \App\Helpers\RankHelper */
    private $rankHelper;

    /** @var \App\Model\UserRepository */
    private $userService;

    /** @var \App\Model\UsersRepository */
    private $usersService;

    /**
     * Injects required repositories.
     * @param \App\Helpers\RankHelper $rankHelper
     * @param \App\Model\MeasureRepository $measureRepository
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectDashboardRepositories(Helpers\RankHelper $rankHelper, Model\MeasureRepository $measureRepository, Model\UserRepository $userRepository, Model\UsersRepository $usersRepository) {
        $this->measureService = $measureRepository;
        $this->rankHelper = $rankHelper;
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;
    }

    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Default action.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {
        $this->template->measurementsCount = $this->measureService->findBy(array('user_id' => $this->user->id))->count();
        $this->template->measurements = $this->measureService->findBy(array('user_id' => $this->user->id))->order('date_time')->limit(10);
        $this->template->lastMeasurement = $this->measureService->findOneBy(array('user_id' => $this->user->id));
        $this->template->score = $this->rankHelper->getScore();
        $this->template->userData = $this->userService->findOneById($this->user->id);
        $this->template->rankPicture = $this->rankHelper->getRankPicture();
    }

}
