<?php

namespace App\FrontModule\Presenters;

use App\Forms;
use App\Helpers;
use App\Model;
use Nette;
use Nette\Http\Session;

/**
 * SignPresenter
 * Presenter for sing-in to app and changing forgotten password.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class SignPresenter extends \App\Presenters\BasePresenter {

    /** @var string Backlink for return to previous page. @persistent */
    private $backlink = '';

    /** @var \App\Helpers\Mailer */
    private $mailer;

    /** @var \App\Forms\OTPSignInForm */
    private $otpSignInFormFactory;

    /** @var \App\Forms\ResetPasswordForm */
    private $resetPasswordFormFactory;

    /** @var string Reset token for setting new password. */
    private $resetToken;

    /** @var Nette\Http\Session */
    private $session;

    /** @var \App\Forms\SignInForm */
    private $signInFormFactory;

    /** @var int User ID for setting new password. */
    private $userId;

    /** @var \App\Model\UserRepository */
    private $userService;

    /** @var \App\Model\UsersRepository */
    private $usersService;

    /**
     * Startup.
     */
    public function startup() {
        parent::startup();
        $this->session->start();
    }

    /**
     * Injects required forms.
     * @param \App\Forms\SignInForm $signInForm
     * @param \App\Forms\OTPSignInForm $otpSignInForm
     * @param \App\Forms\ResetPasswordForm $resetPasswordForm
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectSignForms(Forms\SignInForm $signInForm, Forms\OTPSignInForm $otpSignInForm, Forms\ResetPasswordForm $resetPasswordForm) {
        $this->signInFormFactory = $signInForm;
        $this->otpSignInFormFactory = $otpSignInForm;
        $this->resetPasswordFormFactory = $resetPasswordForm;
    }

    /**
     * Injects required reposiotries.
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectSignRepositories(Model\UserRepository $userRepository, Model\UsersRepository $usersRepository) {
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;
    }

    /**
     *  Injects required services and helpers.
     * @param \App\Helpers\Mailer $mailer
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectSignHelpers(Session $session, Helpers\Mailer $mailer) {
        $this->session = $session;
        $this->mailer = $mailer;
    }

    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Action for sing-in to app.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {
        $this->redirect(':Front:Sign:in');
    }

    /**
     * Action for sing-in to app.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionIn() {
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect(':Front:Dashboard:');
        }
    }

    /**
     * Action for sign-out from app.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionOut() {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášení z aplikace proběhlo úspěšně.', static::FMT_SUCCESS);
        $this->redirect(':Front:Default:');
    }

    /**
     * Action for setting new password.
     * @param string $login
     * @param string $token
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionSetNewPassword($login, $token) {
        // condition for logged user
        if ($this->getUser()->isLoggedIn()) {
            $this->getUser()->logout();
        }

        // condition for check arguments
        if ($login && $token) {
            $user = $this->usersService->findByUsername($login);
            $this->userId = $user->id;
            $this->resetToken = $token;

            // condition for existing user and valid reset token
            if (!$user || !$this->userService->checkResetToken($user->id, $token)) {
                $this->flashMessage('Neplatné uživatelské jméno nebo řetězec. Vygenerujte si prosím nový odkaz pro obnovení hesla.', static::FMT_ERROR);
                $this->redirect('Sign:in');
            }
        } else {
            $this->flashMessage('Tuto akci nelze provést!', static::FMT_ERROR);
            $this->redirect(':Front:Homepage:');
        }
    }

    /**
     * *************************************************************************
     * Components **************************************************************
     * *************************************************************************
     */

    /**
     * Creates component sign-in form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentSignInForm() {
        return $this->signInFormFactory->create($this->backlink);
    }

    /**
     * Creates component OTP sign-in form .
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentOtpSignInForm() {
        return $this->otpSignInFormFactory->create();
    }

    /**
     * Creates component reset password form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentResetPasswordForm() {
        $form = $this->resetPasswordFormFactory->create();

        $form->addHidden('userId', $this->userId);
        $form->addHidden('resetToken', $this->resetToken);

        $form->addSubmit('save', 'Uložit nové heslo');

        $form->onSuccess[] = $this->resetPasswordFormFactory->setPasswordFormSucceeded;

        return $form;
    }

    /**
     * *************************************************************************
     * Handlers ****************************************************************
     * *************************************************************************
     */

    /**
     * Resends authentication email.
     * @param string email $mailTo
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function handleResendAuthenticationMail($mailTo) {
        $this->mailer->sendAuthenticationMail($mailTo);
        $this->redirect('this');
    }

}
