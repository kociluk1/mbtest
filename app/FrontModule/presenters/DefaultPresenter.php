<?php

namespace App\FrontModule\Presenters;

use App\Forms;
use Nette;

/**
 * DefaultPresenter
 * Homepage presenter for guests.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class DefaultPresenter extends \App\Presenters\BasePresenter {
    
    /** @var \App\Forms\ContactForm */
    private $contactFormFactory;

    /**
     * Injects required forms.
     * @param \App\Forms\ContactForm $contactForm
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectDefaultForms(Forms\ContactForm $contactForm) {
        $this->contactFormFactory = $contactForm;
    }
    
    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Action with main info about application.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect(':Front:Dashboard:');
        }
    }
    
    /**
     * *************************************************************************
     * Components **************************************************************
     * *************************************************************************
     */

    /**
     * Creates component contact form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentContactForm() {
        return $this->contactFormFactory->create();
    }
    
}
