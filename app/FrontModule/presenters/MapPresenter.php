<?php

namespace App\FrontModule\Presenters;

use App\Forms;
use Nette;

/**
 * MapPresenter
 * Map presenter for browsing maps.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class MapPresenter extends \App\Presenters\BasePresenter {

    /** @var \App\Forms\MapForm */
    private $mapFormFactory;
    
    /** @var \App\Forms\MyDataForm */
    private $myDataFormFactory;

    /** @var \App\Forms\RelationsForm */
    private $relationsFormFactory;

    /**
     * Injects required forms.
     * @param \App\Forms\MapForm $mapForm
     * @param \App\Forms\MyDataForm $myDataForm
     * @param \App\Forms\RelationsForm $relationsForm
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectMapForms(Forms\MapForm $mapForm, Forms\MyDataForm $myDataForm, Forms\RelationsForm $relationsForm) {
        $this->mapFormFactory = $mapForm;
        $this->myDataFormFactory = $myDataForm;
        $this->relationsFormFactory = $relationsForm;
    }

    /**
     * *************************************************************************
     * Components **************************************************************
     * *************************************************************************
     */

    /**
     * Creates component map form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentMapForm() {
        return $this->mapFormFactory->create();
    }
    
    /**
     * Creates component my data form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentMyDataForm() {
        return $this->myDataFormFactory->create();
    }

    /**
     * Creates component relations form.
     * @return Nette\Application\UI\Form
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function createComponentRelationsForm() {
        return $this->relationsFormFactory->create();
    }

}
