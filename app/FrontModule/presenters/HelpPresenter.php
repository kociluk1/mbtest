<?php

namespace App\FrontModule\Presenters;

use Nette;

/**
 * HelpPresenter
 * Help presenter for showing help.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class HelpPresenter extends \App\Presenters\BasePresenter {
    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Action for showing help.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {

    }
    
    /**
     * Action for showing ranks.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionRanks() {

    }

}
