<?php

namespace App\ApiModule\Presenters;

use Nette;

/**
 * MyDataPresenter
 * Presenter for processing API calls to user data.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class MyDataPresenter extends BaseApiPresenter {
    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Sends user tests data order by download.
     * @param int $limit
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDownloads($limit) {
        $this->processRequest($this->dataHelper->getUserData($this->user->id, 'download', $limit));
    }

    /**
     * Sends user tests data order by ping.
     * @param int $limit
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionPings($limit) {
        $this->processRequest($this->dataHelper->getUserData($this->user->id, 'ping', $limit));
    }

    /**
     * Sends user tests data order by upload.
     * @param int $limit
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionUploads($limit) {
        $this->processRequest($this->dataHelper->getUserData($this->user->id, 'upload', $limit));
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Processes request and sends response or error response.
     * @param array $data
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function processRequest($data) {
        $this->sendJsonResponse(array('measurements' => $data), FALSE, FALSE);
    }

}
