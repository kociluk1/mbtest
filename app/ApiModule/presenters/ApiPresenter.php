<?php

namespace App\ApiModule\Presenters;

use Nette;
use Nette\Http\IResponse;
use Nette\Utils\Strings;

/**
 * ApiPresenter
 * Presenter for processing API calls.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ApiPresenter extends BaseApiPresenter {
    /**
     * *************************************************************************
     * Actions *****************************************************************
     * *************************************************************************
     */

    /**
     * Sends info.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDefault() {
        $this->forward(':Api:Api:info');
    }

    /**
     * Sends tests data order by download.
     * @param string $technology
     * @param string $operator
     * @param int $model
     * @param double $sw_lat
     * @param double $sw_lng
     * @param double $ne_lat
     * @param double $ne_lng
     * @param string $period
     * @param int $percentile
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionDownloads($technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile) {
        $params = $this->setParameters(static::API_DOWNLOADS, $technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile);
        $this->processRequest($params);
    }

    /**
     * Sends info.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionInfo() {
        $data = array(
            'host' => $this->baseUrl,
            'current_version' => static::API_VERSION,
            'cors' => 'enabled',
            'jsonp' => 'not supported',
            'supported_mime_type' => array(
                'application/json'
            ),
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/info',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => static::API_DOWNLOADS,
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/' . static::API_DOWNLOADS,
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'measurements',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/measurements',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'models',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/models',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'operators',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/operators',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => static::API_PINGS,
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/' . static::API_PINGS,
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'platforms',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/platforms',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'relations',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/relations',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => static::API_SIGNALS,
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/' . static::API_SIGNALS,
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => 'technologies',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/technologies',
                    'method' => static::HTTP_METHOD_GET
                ),
                array(
                    'rel' => static::API_UPLOADS,
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/' . static::API_UPLOADS,
                    'method' => static::HTTP_METHOD_GET
                ),
            )
        );

        $this->sendJsonResponse($data, FALSE);
    }

    /**
     * Sends tests data with related id.
     * @param string $dataset
     * @param int $id
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionMeasurements($dataset, $id) {
        $measurement = $this->dataHelper->getDataById($dataset, $id);
        if (empty($measurement)) {
            $this->sendErrorResponse(IResponse::S404_NOT_FOUND, 404, 'error', 'Resource not found.');
        }

        $payload = array(
            'measurement' => $measurement,
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/measurements/' . $dataset . '/' . $id,
                    'method' => static::HTTP_METHOD_GET
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends list of models.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionModels() {
        $payload = array(
            'models' => $this->dataHelper->getModels(),
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/models',
                    'method' => static::HTTP_METHOD_GET,
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends list of operators.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionOperators() {
        $payload = array(
            'operators' => $this->dataHelper->getOperators(),
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/operators',
                    'method' => static::HTTP_METHOD_GET
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends tests data order by ping.
     * @param string $technology
     * @param string $operator
     * @param int $model
     * @param double $sw_lat
     * @param double $sw_lng
     * @param double $ne_lat
     * @param double $ne_lng
     * @param string $period
     * @param int $percentile
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionPings($technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile) {
        $params = $this->setParameters(static::API_PINGS, $technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile);
        $this->processRequest($params);
    }

    /**
     * Sends list of platforms.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionPlatforms() {
        $payload = array(
            'platforms' => $this->dataHelper->getPlatforms(),
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/platforms'
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends tests in OSM relation.
     * @param int $id
     * @param string $data
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionRelations($id, $data) {
        $payload = array(
            'measurements' => $this->osmDataHelper->getRelation($id, $data),
            '_links' => array(
                array(
                    'rel' => 'relation',
                    'href' => $this->baseUrl . 'data/' . $id . '.geojson'
                ),
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/relation/' . $id . '/' . $data
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends tests data order by signal.
     * @param string $technology
     * @param string $operator
     * @param int $model
     * @param double $sw_lat
     * @param double $sw_lng
     * @param double $ne_lat
     * @param double $ne_lng
     * @param string $period
     * @param int $percentile
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionSignals($technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile) {
        $params = $this->setParameters(static::API_SIGNALS, $technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile);
        $this->processRequest($params);
    }

    /**
     * Sends list of mobile network technologies.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionTechnologies() {
        $payload = array(
            'technologies' => $this->dataHelper->getTechnologies(),
            '_links' => array(
                array(
                    'rel' => 'self',
                    'href' => $this->baseUrl . static::API_PATH . static::API_VERSION . '/technologies',
                    'method' => static::HTTP_METHOD_GET
                )
        ));

        $this->sendJsonResponse($payload);
    }

    /**
     * Sends tests data order by upload.
     * @param string $technology
     * @param string $operator
     * @param int $model
     * @param double $sw_lat
     * @param double $sw_lng
     * @param double $ne_lat
     * @param double $ne_lng
     * @param string $period
     * @param int $percentile
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function actionUploads($technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile) {
        $params = $this->setParameters(static::API_UPLOADS, $technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile);
        $this->processRequest($params);
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Processes request and sends response or error response.
     * @param array $params
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function processRequest($params) {
        $payload = array('measurements' => $this->dataHelper->getData($params));
// @todo
//        if (empty($payload)) {
//            $this->sendErrorResponse(422, 422, 'error', 'Unprocessable entity. Paramters sw_lat, sw_lng, ne_lat, ne_lng are required and must be decimal numbers.');
//        }

        $this->sendJsonResponse($payload);
    }

    /**
     * Sets parameters to array.
     * @param string $api
     * @param string $technology
     * @param string $operator
     * @param int $model
     * @param double $sw_lat
     * @param double $sw_lng
     * @param double $ne_lat
     * @param double $ne_lng
     * @param string $period
     * @param int $percentile
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setParameters($api, $technology, $operator, $model, $sw_lat, $sw_lng, $ne_lat, $ne_lng, $period, $percentile) {
        return array(
            'api' => $api,
            'technology' => Strings::replace(Strings::upper(substr($technology, 11)), '#-#', '/'),
            'operator' => substr($operator, 9),
            'model' => substr($model, 6),
            'sw_lat' => $sw_lat,
            'sw_lng' => $sw_lng,
            'ne_lat' => $ne_lat,
            'ne_lng' => $ne_lng,
            'period' => $period,
            'percentile' => $percentile
        );
    }

}
