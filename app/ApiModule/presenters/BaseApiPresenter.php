<?php

namespace App\ApiModule\Presenters;

use App\Helpers;
use Nette;
use Nette\Application\Responses\JsonResponse;
use Nette\Http\Context;
use Nette\Http\IResponse;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

/**
 * BaseApiPresenter
 * Base presenter for all Api module presenters.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
abstract class BaseApiPresenter extends \App\Presenters\BasePresenter {

    /** @const API_DOWNLOADS Download resource. */
    const API_DOWNLOADS = 'downloads';

    /** @const API_PATH Api path. */
    const API_PATH = 'rest/api/';

    /** @const API_PINGS Ping resource. */
    const API_PINGS = 'pings';

    /** @const API_SIGNALS Signal resource.  */
    const API_SIGNALS = 'signals';

    /** @const API_UPLOADS Upload resource. */
    const API_UPLOADS = 'uploads';

    /** @const API_VERSION Version of API. */
    const API_VERSION = 'v1';

    /** @const CACHE_CONTROL Time for how long the response can be cached. */
    const CACHE_CONTROL = '+1day';

    /** @const HTTP_METHOD_GET */
    const HTTP_METHOD_GET = 'GET';

    /** @const HTTP_METHOD_OPTIONS */
    const HTTP_METHOD_OPTIONS = 'OPTIONS';

    /** @var string Base URL. */
    protected $baseUrl;

    /** @var \App\Helpers\DataHelper */
    protected $dataHelper;

    /** @var Nette\Http\Context HTTP Context. */
    protected $httpContext;

    /** @var \App\Helpers\OSMDataHelper */
    protected $osmDataHelper;

    /** @var Nette\Http\Request */
    protected $request;

    /** @var Nette\Http\Response */
    protected $response;

    /**
     * Startup.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function startup() {
        parent::startup();

        $method = $this->request->getMethod();

        // HTTP METHOD OPTIONS
        if ($method == static::HTTP_METHOD_OPTIONS) {
            $this->response->setCode(IResponse::S200_OK);
            $this->response->setHeader('Allow', static::HTTP_METHOD_GET . ', ' . static::HTTP_METHOD_OPTIONS);
            $this->terminate();
        }

        // NOT ALLOWED HTTP METHOD
        if ($method != static::HTTP_METHOD_GET && $method != static::HTTP_METHOD_OPTIONS) {
            $this->response->setHeader('Allow', static::HTTP_METHOD_GET . ', ' . static::HTTP_METHOD_OPTIONS);
            $this->sendErrorResponse(IResponse::S405_METHOD_NOT_ALLOWED, 405, 'error', 'Method not allowed. Allow: ' . static::HTTP_METHOD_GET . ', ' . static::HTTP_METHOD_OPTIONS);
        }

        // NOT ACCEPTABLE MEDIA TYPE
        if ($this->request->getHeader('Accept') && !Strings::match($this->request->getHeader('Accept'), '#(application/json)|(\*/\*)#')) {
            $this->sendErrorResponse(IResponse::S406_NOT_ACCEPTABLE, 415, 'error', 'Not acceptable media type. Accept: application/json.');
        }

        // OLD API VERSION
        $version = substr(Strings::match($this->request->getUrl()->path, '#/v[1-9][0-9]*#')[0], 1);
        if ($version != static::API_VERSION) {
            $this->response->setHeader('Allow', static::HTTP_METHOD_GET . ', ' . static::HTTP_METHOD_OPTIONS);
            $this->sendErrorResponse(IResponse::S410_GONE, 410, 'error', 'Not supported API version. Use ' . static::API_VERSION . '.');
        }
    }

    /**
     * Injects required services.
     * @param Context $httpContext
     * @param \App\Helpers\DataHelper $dataHelper
     * @param \App\Helpers\OSMDataHelper $osmDataHelper
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectApiServices(Context $httpContext, Helpers\DataHelper $dataHelper, Helpers\OSMDataHelper $osmDataHelper) {
        $this->httpContext = $httpContext;
        $this->dataHelper = $dataHelper;
        $this->osmDataHelper = $osmDataHelper;
        $this->request = $httpContext->getRequest();
        $this->response = $httpContext->getResponse();
        $this->baseUrl = $this->request->getUrl()->getBaseUrl();
    }
    
    /**
     * *************************************************************************
     * Protected methods *******************************************************
     * *************************************************************************
     */
    
    /**
     * Sends error response.
     * @param Nette\Http\IResponse::RESPONSE_CODE $responseCode
     * @param int $code
     * @param string $status
     * @param string $message
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function sendErrorResponse($responseCode, $code, $status, $message) {
        $this->response->setCode($responseCode);

        $this->sendResponse(new JsonResponse(array(
            'code' => $code,
            'status' => $status,
            'message' => $message
                ), 'application/json;charset=utf-8'));
    }

    /**
     * Sends JSON response.
     * @param array $payload
     * @param boolean $cacheable
     * @param boolean $cors
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function sendJsonResponse(array $payload, $cacheable = TRUE, $cors = TRUE) {
        $this->response->setHeader('Pragma', NULL);

        if ($cors) {
            // Enable CORS - Cross origin resource sharing
            $this->enableCors($cacheable);
        }

        // Checks/Sets validity headers (ETag and Last Modified) for caching.
        $this->checkValidity($payload, $cacheable);

        if ($cacheable) {
            $this->response->setExpiration(static::CACHE_CONTROL);
        } else {
            $this->response->setExpiration('0sec');
        }

        $this->sendResponse(new JsonResponse($payload, 'application/json;charset=utf-8'));
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Checks validity.
     * @param array $payload
     * @param boolean $cacheable
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function checkValidity(array $payload, $cacheable = TRUE) {
        $etag = $this->getEtag($payload);
        // Sets only ETag, Last-Modified header is not supported by API.
        if (!$this->httpContext->isModified(NULL, $etag)) {
            $this->response->setExpiration($cacheable ? static::CACHE_CONTROL : '0sec');
            $this->terminate();
        }
    }

    /**
     * Return ETag hash of payload.
     * @param array $payload
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getEtag(array $payload) {
        return md5(serialize($payload));
    }

    /**
     * Enables CORS.
     * @param boolean $cacheable
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function enableCors($cacheable = TRUE) {
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setHeader('Access-Control-Allow-Credentials', 'false');
        $this->response->setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        $this->response->setHeader('Access-Control-Allow-Headers', 'Content-Type');
        $this->response->setHeader('Access-Control-Max-Age', $cacheable ? DateTime::from(static::CACHE_CONTROL)->format('U') - time() : '0');
    }

}
