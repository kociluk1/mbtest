<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Nette\Utils\Strings;
use Nette\Utils\Arrays;
use App\Model;
use App\Helpers;

/**
 * Authenticator - Performs user authentication.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator {

    /** @var \App\Model\UserRepository */
    private $userService;

    /** @var \App\Model\UsersRepository */
    private $usersService;

    /** @var \App\Model\Login_keysRepository */
    private $loginKeysService;

    /** @var \App\Helpers\Mailer */
    private $mailer;

    /**
     * Constructor.
     * @param \App\Model\UserRepository $userRepository
     * @param \App\Model\UsersRepository $usersRepository
     * @param \App\Helpers\Mailer $mailer
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Model\UserRepository $userRepository, Model\UsersRepository $usersRepository, Model\Login_keysRepository $loginKeysRepository, Helpers\Mailer $mailer) {
        $this->userService = $userRepository;
        $this->usersService = $usersRepository;
        $this->loginKeysService = $loginKeysRepository;
        $this->mailer = $mailer;
    }

    /**
     * Performs user authentication.
     * @param array $credentials
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function authenticate(array $credentials) {
        $count = count($credentials);

        // condition for username and password
        if ($count === 2) {
            list($username, $password) = $credentials;
            return $this->authenticateCredentials($username, $password);
        }
        // condition for one-time password
        elseif ($count === 1) {
            list($otp) = $credentials;
            return $this->authenticateOtp($otp);
        } else {
            \Tracy\Debugger::log($credentials, \Tracy\Debugger::ERROR);
            throw new Nette\Security\AuthenticationException('Zadané uživatelské jméno nebo heslo je nesprávné.', self::IDENTITY_NOT_FOUND);
        }
    }

    /**
     * Performs user authentication using username and password.
     * @param string $username
     * @param string $password
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function authenticateCredentials($username, $password) {
        $usersRow = $this->usersService->findByUsername($username);
        if ($usersRow) {
            $userRow = $this->userService->findOneById($usersRow->id);
            $hashedPassword = Strings::trim($usersRow->password);
            // condition for creating new user record
            if (!$userRow) {
                $userRow = $this->userService->createUser($usersRow->id);
            }
        }

        // condition for nonexistent user
        if (!$usersRow) {
            throw new Nette\Security\AuthenticationException('Zadané uživatelské jméno nebo heslo je chybné.', self::IDENTITY_NOT_FOUND);
        }
        // condition for setting new password
        elseif ($usersRow->passsalt && $usersRow->email) {
            $this->mailer->sendAuthenticationMail($usersRow->email);
            throw new Nette\Security\AuthenticationException('Z důvodu přechodu na nový systém nejprve nastavte nové heslo.', self::FAILURE);
        }
        // condition for using OTP
        elseif ($usersRow->passsalt && !$usersRow->email) {
            throw new Nette\Security\AuthenticationException('Z důvodu přechodu na nový systém je nutné nastavit nové heslo. '
            . 'Bohužel při registraci nebyl zadán email. Možnost nastavení nového hesla pro Vás není prozatím dostupná. '
            . 'Přihlaste se pomocí jednorázového přístupového kódu.');
        }
        // condition for bad password
        elseif (!Passwords::verify($password, $hashedPassword)) {
            throw new Nette\Security\AuthenticationException('Zadané uživatelské jméno nebo heslo je chybné.', self::INVALID_CREDENTIAL);
        } elseif (Passwords::needsRehash($hashedPassword)) {
            $usersRow->update(array('password' => Passwords::hash($password)));
        }

        unset($hashedPassword);

        $array = $this->createUserArray($usersRow, $userRow);
        $userRole = $this->setRole($array);

        return new Nette\Security\Identity($usersRow->id, $userRole, $array);
    }

    /**
     * Performs user authentication using one-time password.
     * @param string $otp
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function authenticateOtp($otp) {
        $hashedOtp = md5($otp);
        $loginKey = $this->loginKeysService->findOneBy(array('token' => $hashedOtp));

        // condition for nonexistent login key
        if (!$loginKey) {
            throw new Nette\Security\AuthenticationException('Zadaný jednorázový přístupový kód je neplatný.', self::INVALID_CREDENTIAL);
        }
        // condition for used login key
        elseif ($loginKey->used) {
            throw new Nette\Security\AuthenticationException('Zadaný jednorázový přístupový kód již byl použit. Vygenerujte si prosím nový přístupový kód.');
        }

        $this->loginKeysService->update($loginKey->id, array('used' => TRUE));

        $usersRow = $this->usersService->findOneById($loginKey->user_id);
        $userRow = $this->userService->findOneById($loginKey->user_id);
        // condition for creating new user record
        if (!$userRow) {
            $userRow = $this->userService->createUser($loginKey->user_id);
        }

        $array = $this->createUserArray($usersRow, $userRow);
        $userRole = $this->setRole($array);

        return new Nette\Security\Identity($usersRow->id, $userRole, $array);
    }

    /**
     * Generates random password.
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function generatePassword() {
        return Strings::random(8, 'a-zA-Z0-9');
    }

    /**
     * Calculates hash.
     * @param string $string
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function calculateHash($string) {
        return Passwords::hash($string);
    }

    /**
     * Creates array with user data from Users table and User table.
     * @param Nette\Database\Table\ActiveRow $usersRow
     * @param Nette\Database\Table\ActiveRow $userRow
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function createUserArray($usersRow, $userRow) {
        $usersArray = $usersRow->toArray();
        unset($usersArray['password'], $usersArray['passsalt'], $usersArray['last_activity'], $usersArray['perms'], $usersArray['perms_inv']);

        $userArray = $userRow->toArray();
        unset($userArray['id'], $userArray['reset_token'], $userArray['reset_token_validity']);
        
        Arrays::insertAfter($usersArray, 'create_time', $userArray);
        
        return $usersArray;
    }
    
    /**
     * Sets role.
     * @param array $array
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setRole(array $array) {
        return $array['is_admin'] ? Authorizator::ROLE_ADMIN : Authorizator::ROLE_USER;
    }

}
