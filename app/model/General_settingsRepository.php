<?php

namespace App\Model;

use Nette;

/**
 * Description of SettingsRepository
 * Provides methods for working with General settings table.
 * @author Lukas Sokol <sokolluk@fit.cvut.cz>
 */
class General_settingsRepository extends BaseRepository {
    
}