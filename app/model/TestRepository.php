<?php

namespace App\Model;

use Nette;
use Nette\Utils\DateTime;

/**
 * TestRepository
 * Provides methods for working with Test table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class TestRepository extends BaseRepository {

    /**
     * Finds tests at the radius distance from a given point coordinates.
     * @param double $lat
     * @param double $lng
     * @param int $radius Radius in meters.
     * @param string $dateTime
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByCoords($lat, $lng, $radius, $dateTime) {
        return $this->call('getMeasurementsArround(' . $lat . ', ' . $lng . ', ' . $radius . ', "' . $dateTime . '")')->fetchAll();
    }

    /**
     * Finds test by open test id.
     * @param string $openTestId
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByOpenTestId($openTestId) {
        return $this->findOneBy(array('open_test_uuid' => $openTestId));
    }

    /**
     * Finds tests by technology.
     * @param array $technologies
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnology(array $technologies, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds);
        }
        return $this->getTableByOptions($technologies, $options);
    }

    /**
     * Finds tests by technology and model.
     * @param array $technologies
     * @param int $idModel
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnologyModel(array $technologies, $idModel, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds)
                            ->where('model.id', $idModel);
        }
        return $this->getTableByOptions($technologies, $options)
                        ->where('model.id', $idModel);
    }

    /**
     * Finds tests by technology and operator.
     * @param array $technologies
     * @param int $idOperator
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnologyOperator(array $technologies, $idOperator, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds)
                            ->where('operator.id', $idOperator);
        }
        return $this->getTableByOptions($technologies, $options)
                        ->where('operator.id', $idOperator);
    }

    /**
     * Finds tests by technology and operator and model.
     * @param array $technologies
     * @param int $idOperator
     * @param int $idModel
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnologyOperatorModel(array $technologies, $idOperator, $idModel, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds)
                            ->where('operator.id', $idOperator)
                            ->where('model.id', $idModel);
        }
        return $this->getTableByOptions($technologies, $options)
                        ->where('operator.id', $idOperator)
                        ->where('model.id', $idModel);
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns table selection selected by technology and options.
     * @param array $technologies
     * @param array $options
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getBaseTable(array $technologies, array $options) {
        return $this->getTable()
                        ->where('technology.technology', $technologies)
                        ->where($options['resource'] . ' NOT', NULL)
                        ->where('time > ?', $options['period'])
                        ->order($options['resource']);
    }

    /**
     * Returns table selection selected by the options.
     * @param array $technologies
     * @param array $options
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getTableByOptions(array $technologies, array $options) {
        $selection = $this->getBaseTable($technologies, $options);
        return $this->limitSelection($selection, $options['percentile']);
    }

    /**
     * Returns table selection selected by the options and bounds.
     * @param array $technologies
     * @param array $options
     * @param array $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getTableByOptionsAndBounds($technologies, array $options, array $bounds) {
        $selection = $this->getBaseTable($technologies, $options)
                ->where('latitude > ? AND latitude < ?', $bounds[0][0], $bounds[1][0])
                ->where('longitude > ? AND longitude < ?', $bounds[0][1], $bounds[1][1]);
        return $this->limitSelection($selection, $options['percentile']);
    }

}
