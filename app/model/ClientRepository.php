<?php

namespace App\Model;

use Nette;

/**
 * ClientRepository - Provides methods for working with Client table. 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ClientRepository extends BaseRepository {
    
}
