<?php

namespace App\Model;

use Nette;

/**
 * Relation_has_NodeRepository
 * Provides methods for working with Relation_has_Node table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Relation_has_NodeRepository extends BaseRepository {

    /**
     * Inserts new table row to database.
     * @param array $data
     * @return Nette\Database\Table\ActiveRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function insert(array $data) {
        return $this->getDatabase()->query('INSERT INTO `relation_has_node` (`Relation_id`, `Node_id`) VALUES (?, ?)', $data['Relation_id'], $data['Node_id']);
    }
    
    /**
     * Inserts new row in the database.
     * @param array $data
     * @return Nette\Database\Table\ActiveRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function insertOrUpdate(array $data) {
        $row = $this->findOneBy($data);

        if (!$row) {
            $newRow = $this->insert($data);
            return $newRow;
        }
        
        return $row;
    }
    
    /**
     * Deletes table row found by primary relation ID.
     * @param int $relationId
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function delete($relationId) {
        return $this->findBy(array('Relation_id' => $relationId))->delete();
    }

}
