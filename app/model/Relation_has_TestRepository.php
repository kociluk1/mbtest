<?php

namespace App\Model;

use Nette;

/**
 * Relation_has_TestRepository
 * Provides methods for working with Relation_has_Test table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Relation_has_TestRepository extends BaseRepository {

    /**
     * Inserts new table row to database.
     * @param array $data
     * @return Nette\Database\Table\ActiveRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function insert(array $data) {
        return $this->getDatabase()->query('INSERT INTO `relation_has_test` (`Relation_id`, `Test_id`) VALUES (?, ?)', $data['Relation_id'], $data['Test_id']);
    }

    /**
     * Inserts new row in the database.
     * @param array $data
     * @return Nette\Database\Table\ActiveRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function insertOrUpdate(array $data) {
        $row = $this->findOneBy($data);

        if (!$row) {
            $newRow = $this->insert($data);
            return $newRow;
        }

        return $row;
    }
    
    /**
     * Deletes table row found by primary relation ID.
     * @param int $relationId
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function delete($relationId) {
        return $this->findBy(array('Relation_id' => $relationId))->delete();
    }

}
