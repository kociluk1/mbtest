<?php

namespace App\Model;

use Nette;

/**
 * UsersRepository
 * Provides methods for working with Users table (pgsql).
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class UsersRepository extends BaseRepository {

    /**
     * 
     * @param type $by
     * @param type $where
     * @return type
     */
    public function searchUser($by, $where) {
        return $this->getTable()->where($by, "%" . $where . "%", "%" . $where . "%");
    }

    /**
     * Finds user by username.
     * @param string $username
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByUsername($username) {
        return $this->findOneBy(array('username' => $username));
    }

    /**
     * Finds user by email.
     * @param string $email
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByEmail($email) {
        return $this->findOneBy(array('email' => $email));
    }

    /**
     * Updates user score.
     * @param int $userId
     * @param int $score
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function updateScore($userId, $score) {
        $user = $this->findOneById($userId);
        if ($user) {
            $oldScore = $user->score;
            return $this->update($userId, array('score' => $oldScore + $score));
        } else {
            return FALSE;
        }
    }

}
