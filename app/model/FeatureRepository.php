<?php

namespace App\Model;

use Nette;

/**
 * FeatureRepository
 * Provides methods for working with Feature table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class FeatureRepository extends BaseRepository {

    /** @const HIGHWAY Highway. */
    const HIGHWAY = 'highway';

    /** @const RAILWAY Railway. */
    const RAILWAY = 'railway';

}
