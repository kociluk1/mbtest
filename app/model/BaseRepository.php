<?php

namespace App\Model;

use Nette;
use Nette\Utils\Strings;

/**
 * BaseRepository
 * Provides base methods for working with database.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class BaseRepository extends Nette\Object {

    /** @const DEFAULT_PERCENTILE Default percentile for SQL query. */
    const DEFAULT_LIMIT = 100;

    /** @var Nette\Database\Context */
    private $database;

    /**
     * Constructor.
     * @param Nette\Database\Context $database
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /**
     * Returns database.
     * @return Nette\Database\Context
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    final protected function getDatabase() {
        return $this->database;
    }

    /**
     * Returns database table.
     * @param string $tableName
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    final protected function getTable($tableName = NULL) {
        if ($tableName) {
            return $this->database->table(Strings::lower($tableName));
        } else {
            // regular expression matching table name from class name
            $match = Strings::match($this->getReflection()->getName(), '#(\w+)Repository$#');
            return $this->database->table(Strings::lower($match[1]));
        }
    }

    /**
     * *************************************************************************
     * SQL Selectors ***********************************************************
     * *************************************************************************
     */

    /**
     * Returns all table rows.
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findAll() {
        return $this->getTable();
    }

    /**
     * Returns all table rows filtered by input array.
     * @param array $by
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findBy(array $by) {
        return $this->getTable()->where($by);
    }

    /**
     * Returns one table row filtered by input array.
     * @param array $by
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function findOneBy(array $by) {
        $limit = 1;
        return $this->findBy($by)->limit($limit)->fetch();
    }

    /**
     * Returns one table row found by primary key (id). 
     * @param int $id
     * @return Nette\Database\Table\ActiveRow|FALSE 
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findOneById($id) {
        return $this->getTable()->get($id);
    }

    /**
     * Returns limited selection or full selection.
     * @param Nette\Database\Table\Selection $selection
     * @param int $percentile
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function limitSelection($selection, $percentile) {
        return is_null($percentile) ? $selection : $selection->limit($this->getLimit($percentile, $selection->count()));
    }

    /**
     * *************************************************************************
     * Data Manipulation *******************************************************
     * *************************************************************************
     */

    /**
     * Inserts new table row to database.
     * @param array $data
     * @return Nette\Database\Table\ActiveRow
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function insert(array $data) {
        return $this->getTable()->insert($data);
    }

    /**
     * Updates table row found by primary key (id).
     * @param int $id
     * @param array $data
     * @return boolean
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function update($id, array $data) {
        $found = $this->findOneById($id);
        if ($found) {
            $found->update($data);
            return true;
        }
        return false;
    }

    /**
     * Deletes table row found by primary key (id).
     * @param int $id
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz> 
     */
    public function delete($id) {
        return $this->findOneById($id)->delete();
    }

    /**
     * Inserts new row in the database or updates existing one.
     * First key in array must be primary key of database table.
     * @param array $data
     * @return int Id of new inserted row.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function insertOrUpdate(array $data) {
        reset($data);
        $firstKey = key($data);

        $row = $this->findBy(array($firstKey => $data[$firstKey]))->fetch();

        if ($row) {
            $this->update($row->id, $data);
            return $row->id;
        } else {
            $newRow = $this->insert($data);
            return $newRow->id;
        }
    }

    /**
     * *************************************************************************
     * Shortcuts *******************************************************
     * *************************************************************************
     */

    /**
     * Calls SQL stored procedure.
     * Shortcut for $this->database->query('CALL ' . $procedure);
     * @param string $procedure
     * @return type
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function call($procedure) {
        return $this->database->query('CALL ' . $procedure);
    }

    /**
     * Returns count of rows.
     * Shortcut for $this->getTable()->count()
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function count() {
        return $this->getTable()->count();
    }

    /**
     * Delimites identifier for use in a SQL statement.
     * Shortcut for $this->driver->delimite($name)
     * @param string $name
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function delimite($name) {
        return $this->database->getConnection()->getSupplementalDriver()->delimite($name);
    }

    /**
     * Generates and executes SQL query.
     * Shortcut for $this->database->query($query)
     * @param string $query
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function query($query) {
        return $this->database->query($query);
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns limit by percentile.
     * @return int
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getLimit($percentile, $count) {
        (int) $limit = $count * ((is_null($percentile) ? static::DEFAULT_LIMIT : $percentile) / 100);
        return $limit > 1 ? $limit : 1;
    }

}
