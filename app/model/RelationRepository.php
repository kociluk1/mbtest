<?php

namespace App\Model;

use Nette;

/**
 * RelationRepository
 * Provides methods for working with Relations table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RelationRepository extends BaseRepository {

    /**
     * Returns all table rows.
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findAll() {
        return $this->getTable()->order('LENGTH(name), name');
    }

    /**
     * Inserts new row in the database or updates existing one.
     * First key in array must be primary key of database table.
     * @param array $data
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function insertOrUpdate(array $data) {
        reset($data);
        $firstKey = key($data);

        $row = $this->findBy(array($firstKey => $data[$firstKey]))->fetch();

        if ($row) {
            $this->update($row->id, $data);
        } else {
            $this->insert($data);
        }
    }

}
