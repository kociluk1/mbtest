<?php

namespace App\Model;

use Nette;

/**
 * PlatformRepository
 * Provides methods for working with Platform table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class PlatformRepository extends BaseRepository {
    
}
