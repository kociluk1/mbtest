<?php

namespace App\Model;

use Nette;

/**
 * ModelRepository 
 * Provides methods for working with Model table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class ModelRepository extends BaseRepository {
    
}
