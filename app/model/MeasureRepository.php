<?php

namespace App\Model;

use Nette;
use Nette\Utils\DateTime;

/**
 * MeasureRepository
 * Provides methods for working with Measure table (pgsql).
 * @author Lukas Sokol <sokolluk@fit.cvut.cz>
 */
class MeasureRepository extends BaseRepository {

    /**
     * Finds tests by technology.
     * @param array $technologies
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnology(array $technologies, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds);
        }
        return $this->getTableByOptions($technologies, $options);
    }

    /**
     * Finds tests by technology and operator.
     * @param array $technologies
     * @param int $idOperator
     * @param array $options
     * @param array|NULL $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByTechnologyOperator(array $technologies, $idOperator, array $options, $bounds = NULL) {
        if ($bounds) {
            return $this->getTableByOptionsAndBounds($technologies, $options, $bounds)
                            ->where('mnc', $idOperator);
        }
        return $this->getTableByOptions($technologies, $options)
                        ->where('mnc', $idOperator);
    }

    /**
     * Finds tests by user ID.
     * @param int $userId
     * @param int $limit
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findByUserId($userId, $limit) {
        if ($limit) {
            return $this->findBy(array('user_id' => $userId))->order('date_time')->limit($limit);
        }
        return $this->findBy(array('user_id' => $userId))->order('date_time');
    }

    /**
     * Finds users who periodically measure.
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function findPeriodicallyMeasuringUsers() {
        $users = $this->getTable()->where('date_time >= ?', DateTime::from('-1day'))->select('DISTINCT user_id');
        $usersMeasurements = array();
        foreach ($users as $user) {
            $usersMeasurements[$user->user_id] = $this->getTable()->where('user_id', $user->user_id)->where('date_time >= ?', DateTime::from('-30days'))->count('DISTINCT date_time');
        }
        
        return $usersMeasurements;
    }
    
    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns table selection selected by technology and options.
     * @param array $technologies
     * @param array $options
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getBaseTable(array $technologies, array $options) {
        $resourceCol = $this->getResource($options['resource']);
        $badValues = array(0, 1);

        return $this->getTable()
                        ->where('NOT (latitude1 ?) AND latitude1 NOT ?', $badValues, NULL)
                        ->where('NOT (longitude1 ?) AND longitude1 NOT ?', $badValues, NULL)
                        ->where($resourceCol . ' NOT', NULL)
                        ->where('NOT ' . $resourceCol, 0)
                        ->where('conntype.name', $technologies)
                        ->where('date_time > ?', $options['period'])
                        ->order($resourceCol);
    }

    /**
     * Returns resource.
     * @param string $resource
     * @return string
     */
    private function getResource($resource) {
        return $resource === 'ping' ? 'ping_avg' : $resource;
    }

    /**
     * Returns table selection selected by the options.
     * @param array $technologies
     * @param array $options
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getTableByOptions(array $technologies, array $options) {
        $selection = $this->getBaseTable($technologies, $options);
        return $this->limitSelection($selection, $options['percentile']);
    }

    /**
     * Returns table selection selected by the options and bounds.
     * @param array $technologies
     * @param array $options
     * @param array $bounds
     * @return Nette\Database\Table\Selection
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getTableByOptionsAndBounds(array $technologies, array $options, array $bounds) {
        $selection = $this->getBaseTable($technologies, $options)
                ->where('latitude1 > ? AND latitude1 < ?', $bounds[0][0], $bounds[1][0])
                ->where('longitude1 > ? AND longitude1 < ?', $bounds[0][1], $bounds[1][1]);
        return $this->limitSelection($selection, $options['percentile']);
    }

}
