<?php

namespace App\Model;

use Nette;

/**
 * IPRepository - Provides methods for working with IP table. 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class IPRepository extends BaseRepository {

}
