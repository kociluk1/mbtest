<?php

namespace App\Model;

use Nette;
use Nette\Security\Permission;

/**
 * Authorizator
 * Performs user authorization.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Authorizator extends Permission {

    /** @const ROLE_GUEST User role guest. */
    const ROLE_GUEST = 'guest';

    /** @const ROLE_USER User role user. */
    const ROLE_USER = 'user';

    /** @const ROLE_ADMIN User role admin. */
    const ROLE_ADMIN = 'admin';

    /**
     * Constructor.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct() {
        $this->addRoles();
        $this->addResources();
        $this->setRules();
    }

    /**
     * Adds all roles used in application.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function addRoles() {
        $this->addRole(static::ROLE_GUEST);
        $this->addRole(static::ROLE_USER, static::ROLE_GUEST);
        $this->addRole(static::ROLE_ADMIN, static::ROLE_USER);
    }

    /**
     * Adds all resources (presenters) used in application.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function addResources() {
        // Front presenters
        $this->addResource('Front:Default');
        $this->addResource('Front:MyData');
        $this->addResource('Front:Dashboard');
        $this->addResource('Front:Help');
        $this->addResource('Front:Map');
        $this->addResource('Front:Sign');
        $this->addResource('Front:Statistics');
        $this->addResource('Front:User');

        // API presenters
        $this->addResource('Api:Api');
        $this->addResource('Api:MyData');

        // Admin presenters
        $this->addResource('Admin:Default');
        $this->addResource('Admin:Export');
        $this->addResource('Admin:Osm');
        $this->addResource('Admin:Rights');
        $this->addResource('Admin:Test');
    }

    /**
     * Sets authorization rules.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function setRules() {
        // Guest privileges and resources
        $this->allow(static::ROLE_GUEST, 'Api:Api');
        $this->allow(static::ROLE_GUEST, 'Front:Default');
        $this->allow(static::ROLE_GUEST, 'Front:Sign');

        // User privileges and resources
        $this->allow(static::ROLE_USER, 'Api:MyData');
        $this->allow(static::ROLE_USER, 'Front:Dashboard');
        $this->allow(static::ROLE_USER, 'Front:MyData');
        $this->allow(static::ROLE_USER, 'Front:Help');
        $this->allow(static::ROLE_USER, 'Front:Map');
        $this->allow(static::ROLE_USER, 'Front:Statistics');
        $this->allow(static::ROLE_USER, 'Front:User');

        // Admin privileges and resources
        $this->allow(static::ROLE_ADMIN);
    }

    /**
     * Decides whether the user is admin.
     * @param Nette\Security\User $user
     * @return bool
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public static function isAdmin($user) {
        return $user->isInRole(static::ROLE_ADMIN);
    }

}
