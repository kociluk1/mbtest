<?php

namespace App\Model;

use Nette;

/**
 * OperatorRepository
 * Provides methods for working with Operator table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class OperatorRepository extends BaseRepository {

    /** @const ID_OPERATOR_230_01 T-Mobile */
    const ID_OPERATOR_230_01 = '1';

    /** @const ID_OPERATOR_230_02 O2 */
    const ID_OPERATOR_230_02 = '2';

    /** @const ID_OPERATOR_230_03 Vodafone */
    const ID_OPERATOR_230_03 = '3';

    /** @const MNC_MCC_OPERATOR_230_01 T-Mobile */
    const MNC_MCC_OPERATOR_230_01 = '230-01';

    /** @const MNC_MCC_OPERATOR_230_02 O2 */
    const MNC_MCC_OPERATOR_230_02 = '230-02';

    /** @const MNC_MCC_OPERATOR_230_03 Vodafone */
    const MNC_MCC_OPERATOR_230_03 = '230-03';

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */
    
    /**
     * Returns relevant operator id.
     * @param string|NULL $mccMnc
     * @return int|NULL
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getOperatorId($mccMnc) {
        switch ($mccMnc) {
            case OperatorRepository::MNC_MCC_OPERATOR_230_01:
                return OperatorRepository::ID_OPERATOR_230_01;
            case OperatorRepository::MNC_MCC_OPERATOR_230_02:
                return OperatorRepository::ID_OPERATOR_230_02;
            case OperatorRepository::MNC_MCC_OPERATOR_230_03:
                return OperatorRepository::ID_OPERATOR_230_03;
            default:
                return NULL;
        }
    }

}
