<?php

namespace App\Model;

use Nette;

/**
 * Login_keysRepository - Provides methods for working with Login_keys table.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Login_keysRepository extends BaseRepository {
    
}
