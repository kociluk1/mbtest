<?php

namespace App\Model;

use Nette;

/**
 * Autonomous_systemRepository - Provides methods for working with Autonomous_sytem table. 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class Autonomous_systemRepository extends BaseRepository {
    
}
