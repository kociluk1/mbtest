<?php

namespace App\Model;

use Nette;

/**
 * NetworkRepository - Provides methods for working with Network table. 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class NetworkRepository extends BaseRepository {
    
}
