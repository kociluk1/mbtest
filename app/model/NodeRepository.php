<?php

namespace App\Model;

use Nette;

/**
 * NodeRepository
 * Provides methods for working with Node table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class NodeRepository extends BaseRepository {
    
}
