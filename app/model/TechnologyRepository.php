<?php

namespace App\Model;

use Nette;

/**
 * TechnologyRepository
 * Provides methods for working with Technology table (mysql). 
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class TechnologyRepository extends BaseRepository {

    /** @const TECHNOLOGY_2G */
    const TECHNOLOGY_2G = '2G';

    /** @const TECHNOLOGY_2G_3G */
    const TECHNOLOGY_2G_3G = '2G/3G';

    /** @const TECHNOLOGY_2G_4G */
    const TECHNOLOGY_2G_4G = '2G/4G';

    /** @const TECHNOLOGY_2G_3G_4G */
    const TECHNOLOGY_2G_3G_4G = '2G/3G/4G';

    /** @const TECHNOLOGY_3G */
    const TECHNOLOGY_3G = '3G';

    /** @const TECHNOLOGY_3G_4G */
    const TECHNOLOGY_3G_4G = '3G/4G';

    /** @const TECHNOLOGY_4G */
    const TECHNOLOGY_4G = '4G';

    /** @const TECHNOLOGY_WLAN */
    const TECHNOLOGY_WLAN = 'WLAN';

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Returns array of 2G mobile technologies.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function get2G() {
        return array(
            'GSM',
            'GPRS',
            'EDGE',
            static::TECHNOLOGY_2G
        );
    }

    /**
     * Returns array of 3G mobile technologies.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function get3G() {
        return array(
            'UMTS',
            'HSPA',
            'HSPA+',
            'HSDPA',
            'HSUPA',
            static::TECHNOLOGY_3G
        );
    }

    /**
     * Returns array of 4G mobile technologies.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function get4G() {
        return array(
            'LTE',
            static::TECHNOLOGY_4G
        );
    }

    /**
     * Returns array of WLANs.
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getWLAN() {
        return array(
            'wifi',
            static::TECHNOLOGY_WLAN
        );
    }

    /**
     * Returns relevant technology.
     * @param string $network
     * @return string|NULL
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getTechnology($network) {
        switch (TRUE) {
            case in_array($network, $this->get2G()):
                return static::TECHNOLOGY_2G;
            case in_array($network, $this->get3G()):
                return static::TECHNOLOGY_3G;
            case in_array($network, $this->get4G()):
                return static::TECHNOLOGY_4G;
            case in_array($network, $this->getWLAN()):
                return static::TECHNOLOGY_WLAN;
            default:
                return NULL;
        }
    }

    /**
     * Returns relevant technology list.
     * @param string|NULL $technology
     * @return array
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function getTechnologies($technology) {
        switch ($technology) {
            case static::TECHNOLOGY_2G:
                return $this->get2G();
            case static::TECHNOLOGY_3G:
                return $this->get3G();
            case static::TECHNOLOGY_4G:
                return $this->get4G();
            case static::TECHNOLOGY_2G_3G:
                return array_merge($this->get2G(), $this->get3G(), array(static::TECHNOLOGY_2G_3G));
            case static::TECHNOLOGY_2G_4G:
                return array_merge($this->get2G(), $this->get4G(), array(static::TECHNOLOGY_2G_4G));
            case static::TECHNOLOGY_3G_4G:
                return array_merge($this->get3G(), $this->get4G(), array(static::TECHNOLOGY_3G_4G));
            case static::TECHNOLOGY_WLAN:
                return $this->getWLAN();
            case static::TECHNOLOGY_2G_3G_4G:
                return array_merge($this->get2G(), $this->get3G(), $this->get4G(), array(
                    static::TECHNOLOGY_2G_3G,
                    static::TECHNOLOGY_2G_4G,
                    static::TECHNOLOGY_3G_4G,
                    static::TECHNOLOGY_2G_3G_4G
                        )
                );
            default:
                return array_merge($this->get2G(), $this->get3G(), $this->get4G(), $this->getWLAN(), array(
                    static::TECHNOLOGY_2G_3G,
                    static::TECHNOLOGY_2G_4G,
                    static::TECHNOLOGY_3G_4G,
                    static::TECHNOLOGY_2G_3G_4G
                        )
                );
        }
    }

}
