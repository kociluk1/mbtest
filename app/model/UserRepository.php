<?php

namespace App\Model;

use Nette;
use Nette\Utils\DateTime;
use Nette\Utils\Random;

/**
 * UserRepository - Provides methods for working with User table (mysql).
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class UserRepository extends BaseRepository {

    /** @var Nette\Http\Request */
    private $httpRequest;

    /**
     * Constructor.
     * @param Nette\Database\Context $database
     * @param Nette\Http\Request $httpRequest
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function __construct(Nette\Database\Context $database, Nette\Http\Request $httpRequest) {
        parent::__construct($database);
        $this->httpRequest = $httpRequest;
    }

    /**
     * Creates new user record.
     * @param int $idUser
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function createUser($idUser) {
        $this->insert(array(
            'id' => $idUser,
            'last_ip' => $this->httpRequest->getRemoteAddress(),
            'last_login' => '0000-00-00 00:00:00',
            'last_activity' => '0000-00-00 00:00:00',
            'is_admin' => FALSE
        ));

        return $this->findOneById($idUser);
    }

    /**
     * Generates reset token.
     * @param int $idUser
     * @return string
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function generateResetToken($idUser) {
        $token = Random::generate(32, '0-9a-zA-Z');
        $this->findOneById($idUser)->update(array(
            'reset_token' => md5($token),
            'reset_token_validity' => new DateTime('+1 day')
        ));

        return $token;
    }

    /**
     * Checks reset token validity.
     * @param int $idUser
     * @param string $token
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function checkResetToken($idUser, $token) {
        return $this->findBy(array(
                    'id' => $idUser,
                    'reset_token' => md5($token)
                ))->where('reset_token_validity >= ?', new DateTime())->fetch();
    }

    /**
     * Invalidates reset token.
     * @param int $idUser
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function invalidateResetToken($idUser) {
        return $this->findOneById($idUser)->update(array(
                    'reset_token' => NULL,
                    'reset_token_validity' => NULL
        ));
    }

    /**
     * Updates last ip.
     * @param int $idUser
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function updateLastIp($idUser) {
        return $this->findOneById($idUser)->update(array(
                    'last_ip' => $this->httpRequest->getRemoteAddress()
        ));
    }
    
    /**
     * Updates last user login.
     * @param int $idUser
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function updateLastLogin($idUser) {
        return $this->findOneById($idUser)->update(array(
                    'last_login' => new DateTime()
        ));
    }

    /**
     * Updates last user activity.
     * @param int $idUser
     * @return Nette\Database\Table\ActiveRow|FALSE
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function updateLastActivity($idUser) {
        return $this->findOneById($idUser)->update(array(
                    'last_activity' => new DateTime()
        ));
    }

}
