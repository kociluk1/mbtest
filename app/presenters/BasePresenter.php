<?php

namespace App\Presenters;

use App\Helpers;
use App\Model;
use Nette;
use Nette\Security;

/**
 * BasePresenter
 * Parent presenter for all application presenters.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /** @const API Api presenter name. */
    const API = 'Api:Api';

    /** @const FM_SUCCESS Flash message type for success. Green notice. */
    const FMT_SUCCESS = 'success';

    /** @const FM_ERROR Flash message type for error. Red notice. */
    const FMT_ERROR = 'error';

    /** @const FM_INFO Flash message type for info. Blue notice. */
    const FMT_INFO = 'info';

    /** @const FM_WARNING Flash message type for warning. Yellow notice. */
    const FMT_WARNING = 'warning';

    /** @const HOMEPAGE Site homepage presenter name. */
    const HOMEPAGE = 'Front:Default';

    /** @const HOMEPAGE_ADMIN_MODULE Admin module homepage presenter name. */
    const HOMEPAGE_ADMIN_MODULE = 'Admin:Default';

    /** @const HOMEPAGE_FRONT_MODULE Front module homepage presenter name. */
    const HOMEPAGE_FRONT_MODULE = 'Front:Dashboard';

    /** @const SIGN Sing presenter name. */
    const SIGN = 'Front:Sign';

    /** @const FM_INACTIVITY Flash message for inactivity logout. */
    const FM_INACTIVITY = 'Proběhlo odhlášení z aplikace z důvodu příliš dlouhé doby nečinnosti.';

    /** @const FM_MANUAL Flash message for logout by browser closing. */
    const FM_BROWSER_CLOSED = 'Proběhlo odhlášení z aplikace z důvodu uzavření prohlížeče.';

    /** @const FM_MANUAL Flash message for manual logout. */
    const FM_MANUAL = 'Byli jste odhlášeni. Prosím, přihlašte se znovu.';

    /** @const FM_PAGE_AUTHORIZATION_FAILED Flash message for page autorization failure. */
    const FM_PAGE_AUTHORIZATION_FAILED = 'Přístup byl odepřen. Nemáte oprávnění pro přístup na tuto stránku!';

    /** @const FM_SIGNAL_AUTHORIZATION_FAILED Flash message for signal autorization failure. */
    const FM_SIGNAL_AUTHORIZATION_FAILED = 'Přístup byl odepřen. Nemáte oprávnění pro provední této akce!';

    /** @var \App\Helpers\RankHelper */
    private $rankHelper;

    /** @var \App\Model\UserRepository */
    private $userService;

    /**
     * Startup.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function startup() {
        parent::startup();

        if ($this->name !== static::SIGN && $this->name !== static::HOMEPAGE) {
            $this->checkAutorization();
        }
    }

    /**
     * Injects required helpers.
     * @param \App\Helpers\RankHelper $rankHelper
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectHelpers(Helpers\RankHelper $rankHelper) {
        $this->rankHelper = $rankHelper;
    }

    /**
     * Injects required reposiotries.
     * @param \App\Model\UserRepository $userRepository
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public function injectRepositories(Model\UserRepository $userRepository) {
        $this->userService = $userRepository;
    }

    /**
     * Before render for flash messages and last user activity.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    protected function beforeRender() {
        if ($this->isAjax()) {
            $this->redrawControl('flashMessages');
        }

        if ($this->user->isLoggedIn()) {
            $this->userService->updateLastActivity($this->user->id);
        }

        // Useful variables
        $this->template->homepage = $this->getHomepage();
        $this->template->viewName = $this->view;
        $this->template->root = isset($_SERVER['SCRIPT_FILENAME']) ? realpath(dirname(dirname($_SERVER['SCRIPT_FILENAME']))) : NULL;
        $a = strrpos($this->name, ':');
        if ($a === FALSE) {
            $this->template->moduleName = '';
            $this->template->presenterName = $this->name;
        } else {
            $this->template->moduleName = substr($this->name, 0, $a + 1);
            $this->template->presenterName = substr($this->name, $a + 1);
        }
    }

    /**
     * *************************************************************************
     * Helpers *****************************************************************
     * *************************************************************************
     */

    /**
     * Checks if user has permissions to perform action or signal.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function checkAutorization() {
        // condition for logged user
        if ($this->user->isLoggedIn()) {
            // condition for presenter and action
            if (!$this->user->isAllowed($this->name, $this->action)) {
                $this->flashMessage(static::FM_PAGE_AUTHORIZATION_FAILED, static::FMT_ERROR);
                $this->redirect(':Front:Dashboard:');
            }
            // condition for signal
            elseif ($this->signal !== NULL && !$this->user->isAllowed($this->name, $this->formatSignalString())) {
                $this->flashMessage(static::FM_SIGNAL_AUTHORIZATION_FAILED, static::FMT_ERROR);
                $this->redirect('this');
            }

            if (!$this->rankHelper->checkScorePermissions($this->name) && $this->name !== static::HOMEPAGE_FRONT_MODULE) {
                $this->flashMessage('Nemáte dostatek mBodů pro zobrazení této stránky. Více informací viz Nápověda.', static::FMT_INFO);
                $this->redirect(':Front:Dashboard:');
            }
            // condition for logged user
        } else {
            if ($this->name !== static::API) {
                $this->getLogoutReason();
                $this->redirect(':Front:Sign:in', array('backlink' => $this->storeRequest()));
            }
        }
    }

    /**
     * Formats signal string.
     * @return string 
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function formatSignalString() {
        return $this->presenter->signal === NULL ? NULL : ltrim(implode('-', $this->presenter->signal), '-') . '!';
    }

    /**
     * Gets homepage link.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getHomepage() {
        if ($this->user->isLoggedIn()) {
            if ($this->user->isInRole(Model\Authorizator::ROLE_ADMIN)) {
                return ':' . static::HOMEPAGE_ADMIN_MODULE . ':';
            } else {
                return ':' . static::HOMEPAGE_FRONT_MODULE . ':';
            }
        }

        return ':' . static::HOMEPAGE . ':';
    }

    /**
     * Gets logout reason and sets flashmessage.
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    private function getLogoutReason() {
        switch ($this->user->getLogoutReason()) {
            case Security\IUserStorage::INACTIVITY:
                $this->flashMessage(static::FM_INACTIVITY, static::FMT_WARNING);
                break;
            case Security\IUserStorage::BROWSER_CLOSED:
                $this->flashMessage(static::FM_BROWSER_CLOSED, static::FMT_WARNING);
                break;
            case Security\IUserStorage::MANUAL:
                $this->flashMessage(static::FM_MANUAL, static::FMT_WARNING);
                break;
            default:
                $this->flashMessage(static::FM_PAGE_AUTHORIZATION_FAILED, static::FMT_ERROR);
                break;
        }
    }

}
