<?php

namespace App\Router;

use App\ApiModule\Presenters\ApiPresenter;
use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Router factory
 * Creates routes.
 * @author Lukas Koci <kociluk1@fit.cvut.cz>
 */
class RouterFactory {

    /** @const TECHNOLOGY Regex for technology. */
    const TECHNOLOGY = '[/<technology technology/(2G(-[3-4]G)*|3G(-4G)*|4G|WLAN)>]';
    
    /** @const OPERATOR Regex for operator. */
    const OPERATOR = '[/<operator operator/230-[0-9]{2}>]';
    
    /** @const MODEL Regex for model. */
    const MODEL = '[/<model model/[1-9][0-9]{0,5}>]';
    
    /**
     * Creates router with routes.
     * @return Nette\Application\IRouter
     * @author Lukas Koci <kociluk1@fit.cvut.cz>
     */
    public static function createRouter() {

        $router = new RouteList();

        $router[] = new Route('index.php', 'Front:Default:default', Route::ONE_WAY);

        // Admin routes
        $adminRouter = new RouteList('Admin');
        $adminRouter[] = new Route('admin/<presenter>/<action>[/<id>]', 'Default:default');
        $router[] = $adminRouter;

        // API routes
        $apiRouter = new RouteList('Api');

        // API acctepts netmetr.cz openId and mbtest.cz measureId
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/measurements/<dataset od|mb>/<id [0-9]+>', 'Api:measurements');

        // API acctepts 
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/relations/<id [0-9]+>/<data download|upload|ping>', 'Api:relations');
        
        // API accepts all combinations of resources
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::TECHNOLOGY . static::OPERATOR . static::MODEL, 'Api:default');
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::TECHNOLOGY . static::MODEL . static::OPERATOR, 'Api:default');
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::OPERATOR . static::TECHNOLOGY . static::MODEL, 'Api:default');
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::OPERATOR . static::MODEL . static::TECHNOLOGY, 'Api:default');
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::MODEL . static::TECHNOLOGY . static::OPERATOR, 'Api:default');
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action downloads|uploads|pings|signals>' . static::MODEL. static::OPERATOR . static::TECHNOLOGY, 'Api:default');

        // API accepts only action (resource) parameter
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/<action>', 'Api:default');
        
        // API accepts only action (resource) parameter
        $apiRouter[] = new Route(ApiPresenter::API_PATH . '<api_version v[1-9][0-9]*>/my-data/<action>', 'MyData:default');

        $router[] = $apiRouter;

        // Front routes
        $frontRouter = new RouteList('Front');
        $frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Default:default');
        $router[] = $frontRouter;

        return $router;
    }

}
