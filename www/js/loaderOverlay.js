/**
 * Shows loader overlay or adds one to body.
 */
function showLoaderOverlay() {
    if ($('.loader-overlay').length === 0) {
	var overlay = '<div class="loader-overlay"><div class="loader"></div></div>';
	$(overlay).appendTo('body');
    } else {
	$('.loader-overlay').removeClass('hidden');
    }
}

/**
 * Hides loader overlay.
 */
function hideLoaderOverlay() {
    $('.loader-overlay').addClass('hidden');
}

$('#frm-osmImportForm').submit(showLoaderOverlay);

$(document).ready(hideLoaderOverlay);