/* 
 Leaflet & OSM Map & Mapbox
 Author: Lukas Koci <kociluk1@fit.cvut.cz>
 */

/**
 * ******** Global variables ***************************************************
 */

// REST API url
var restApiUrl = 'http://dip_mbtest/REST/api/';

// MB Test API version
var apiVersion = 'v1';

// Mapbox API Default Public Token
var accessToken = 'pk.eyJ1Ijoic3B5bmVyIiwiYSI6ImNpZzE5ZjV0MDAwYjZ2b202djVmd3Z5MHoifQ.W4LaFDw13-Y1TkuvurOKeg';

// Mapbox URL
var mapboxUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + accessToken;

// OSM URL
var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

// Attributions
var mapDataAttr = 'Map data by &copy; <a href="http://openstreetmap.org" target="_blank" title="An openly licensed map of the world">OpenStreetMap</a> contributors';
var licenseAttr = '<a href="http://creativecommons.org/licenses/by-sa/2.0/" target="_blank" title="Attribution-ShareAlike 2.0 Generic">CC-BY-SA</a>';
var tilesAttr = 'Tiles by &copy; <a href="http://mapbox.com" target="_blank">Mapbox</a>';
var dataAttr = 'Data by &copy; <a href="https://www.netmetr.cz/" target="_blank">Netmetr</a> and MB Test';

// Attribution text for Mapbox
var mapboxAttr = mapDataAttr + ', ' + licenseAttr + ', ' + tilesAttr + ', ' + dataAttr;

// Attribution text for OSM
var osmAttr = mapDataAttr + ', ' + licenseAttr + ', ' + dataAttr;

// Marker with geographic coordinates of clicking on the map
var posMarker = null;

// Max zoom
var maxZoom = 18;

// Min zoom
var minZoom = 8;

// Map center
var mapCenter = [49.8108534, 15.6122417];

// Limits for download, upload, ping and signal.
var limits = {
    download: {green: 15000, yellow: 3000, red: 1000},
    upload: {green: 8000, yellow: 1500, red: 600},
    ping: {green: 5, yellow: 25, red: 100},
    signal_mobile: {green: -50, yellow: -90, red: -120},
    signal_wlan: {green: -45, yellow: -60, red: -80}
};

// Default select value for all selects
var defaultSelectVal = 0;

// Default ID for 2G technologies in select options
var default2GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '2G');
}).val());

// Default ID for 3G technologies in select options
var default3GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '3G');
}).val());

// Default ID for 4G technologies in select options
var default4GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '4G');
}).val());

// Default ID for 2G/3G technologies in select options
var default2G3GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '2G/3G');
}).val());

// Default ID for 2G/4G technologies in select options
var default2G4GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '2G/4G');
}).val());

// Default ID for 3G/4G technologies in select options
var default3G4GId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '3G/4G');
}).val());

// Default ID for all mobile technologies in select options
var defaultAllMobTechId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === '2G/3G/4G');
}).val());

// Default ID for WLAN in select options
var defaultWlanId = parseInt($('select#frm-mapForm-technology option').filter(function () {
    return ($(this).text() === 'WLAN');
}).val());

// Default period in select options
var defaultPeriodId = $('select#frm-mapForm-period option').filter(function () {
    return ($(this).text() === '1 rok');
}).val();

// Default percentile in select options
var defaultPercentileId = parseInt($('select#frm-mapForm-percentile option').filter(function () {
    return ($(this).text() === 'Všechna měření');
}).val());

/**
 * ******** Map layers *********************************************************
 */

// Creates new streers layer
var streets = L.tileLayer(mapboxUrl, {
    attribution: mapboxAttr,
    id: 'mapbox.streets',
    maxZoom: maxZoom,
    minZoom: minZoom
});

// Creates new satellite layer
var satellite = L.tileLayer(mapboxUrl, {
    attribution: mapboxAttr,
    id: 'mapbox.satellite',
    maxZoom: maxZoom,
    minZoom: minZoom
});

// Creates new grayscale layer
var light = L.tileLayer(mapboxUrl, {
    attribution: mapboxAttr,
    id: 'mapbox.light',
    maxZoom: maxZoom,
    minZoom: minZoom
});

// Creates new dark layer
var dark = L.tileLayer(mapboxUrl, {
    attribution: mapboxAttr,
    id: 'mapbox.dark',
    maxZoom: maxZoom,
    minZoom: minZoom
});

// Creates new OSM layer
var osm = L.tileLayer(osmUrl, {
    attribution: osmAttr,
    //id: 'mapbox.streets',
    maxZoom: maxZoom,
    minZoom: minZoom
});

/**
 * ******** Point map variables ************************************************
 */

var markers = [];

var pointRadius = 11;
var pointStrokeWeight = 3;
var pointStrokeOpacity = 0.8;
var pointFillOpacity = 0.8;

var pointsOptionsDarkGreen = {
    radius: pointRadius,
    weight: pointStrokeWeight,
    opacity: pointStrokeOpacity,
    color: "#146905",
    fillColor: "#20910C",
    fillOpacity: pointFillOpacity
};

var pointsOptionsGreen = {
    radius: pointRadius,
    weight: pointStrokeWeight,
    opacity: pointStrokeOpacity,
    color: "#30BD09",
    fillColor: "#32E600",
    fillOpacity: pointFillOpacity
};

var pointsOptionsYellow = {
    radius: pointRadius,
    weight: pointStrokeWeight,
    opacity: pointStrokeOpacity,
    color: "#FADF32",
    fillColor: "#FCF80A",
    fillOpacity: pointFillOpacity
};

var pointsOptionsRed = {
    radius: pointRadius,
    weight: pointStrokeWeight,
    opacity: pointStrokeOpacity,
    color: "#BD0202",
    fillColor: "#E30505",
    fillOpacity: pointFillOpacity
};

var points = new L.markerClusterGroup({
    chunkedLoading: true,
    chunkProgress: updateProgressBar,
    //disableClusteringAtZoom: maxZoom,
}).bringToFront();

/**
 * ******** Heat map variables *************************************************
 */

var heatmapIntensity = 0.4;

// Radius and blur list for heatmap 
// heatmapRadiusList[0] = minZoom
// heatmapRadiusList[10] = maxZoom
// min value = 1, max value = 70
// zoom level		 8  9  10 11 12 13 14  15  16  17  18
var heatmapRadiusList = [2, 4, 6, 8, 10, 12, 20, 30, 40, 50, 74];
var heatmapBlurList = [2, 4, 6, 8, 10, 12, 20, 30, 40, 50, 74];

// Radius list for heatmap 
// heatmapOpacityList[0] = minZoom
// heatmapOpacityList[10] = maxZoom
// min value = 0, max value = 1
var heatmapOpacityList = [0.6, 0.5, 0.5, 0.5, 0.5, 0.5, 0.4, 0.4, 0.4, 0.3, 0];

var dataDarkGreen = [];
var dataGreen = [];
var dataYellow = [];
var dataRed = [];

var heatmapMinOpacity = heatmapOpacityList[maxZoom - minZoom];
var heatmapRadius = heatmapRadiusList[maxZoom - minZoom];
var heatmapBlur = heatmapBlurList[maxZoom - minZoom];

var heatmapOptionsDarkGreen = {
    minOpacity: heatmapMinOpacity,
    radius: heatmapRadius,
    blur: heatmapBlur,
    gradient: {0: 'Lime', 0.2: 'Green'}
};

var heatmapOptionsGreen = {
    minOpacity: heatmapMinOpacity,
    radius: heatmapRadius,
    blur: heatmapBlur,
    gradient: {0: 'yellow', 0.2: 'LawnGreen'}
};

var heatmapOptionsYellow = {
    minOpacity: heatmapMinOpacity,
    radius: heatmapRadius,
    blur: heatmapBlur,
    gradient: {0.5: 'yellow', 1: 'Gold'}
};

var heatmapOptionsRed = {
    minOpacity: heatmapMinOpacity,
    radius: heatmapRadius,
    blur: heatmapBlur,
    gradient: {0: 'OrangeRed', 0.2: 'red'}
};

var heatmap = L.featureGroup().bringToBack();

/**
 * ******** GeoJson variables *************************************************
 */

var relation = L.geoJson();

/**
 * ******** Loading map ********************************************************
 */

/**
 * Loads default Leaflet map.
 */
function loadMap() {
    map = new L.map('map', {
	center: mapCenter, // initial geographical center of the map
	zoom: minZoom, // initial map zoom
	layers: [streets, points], // initial layers
	zoomControl: false
    });

    registerEventListeners();
    addControls();
}

/**
 * Sets pointmap leayers.
 */
function setPointLayers() {
    points.clearLayers();
    points.addLayers(markers).bringToFront();
}

/**
 * Sets heatmap leayers.
 */
function setHeatLayers() {
    heatmapDarkGreen = L.heatLayer(dataDarkGreen, heatmapOptionsDarkGreen);
    heatmapGreen = L.heatLayer(dataGreen, heatmapOptionsGreen);
    heatmapYellow = L.heatLayer(dataYellow, heatmapOptionsYellow);
    heatmapRed = L.heatLayer(dataRed, heatmapOptionsRed);

    heatmap.clearLayers();
    heatmap.addLayer(heatmapRed);
    heatmap.addLayer(heatmapYellow);
    heatmap.addLayer(heatmapGreen);
    heatmap.addLayer(heatmapDarkGreen);
    heatmap.bringToBack();
}

/**
 * Registers event listeners.
 */
function registerEventListeners() {
    map.on('click', onClick);
    map.on('overlayadd', onOverlayAdd);
    map.on('zoomend', onZoomEnd);
    points.on('click', markerOnClick);
}

/**
 * Adds controls to the map.
 */
function addControls() {
    addZoomControl();
    addSearchControl();
    addLocateControl();
    addScaleControl();
    addLayersControl();
}

/**
 * Adds layers control to the map.
 */
function addLayersControl() {
    // Creates layers list
    var baseLayers = {
	'Základí': streets,
	'Satelitní': satellite,
	'Světlá': light,
	'Tmavá': dark,
	'OpenStreetMap': osm
    };

    // Creates overlays list
    var overlays = {
	'Jednotlivá měření': points,
	'Heatmapa': heatmap
    };

    // Sets layers options
    var layersOptions = {
	//collapsed : false
    };

    // Adds layers control to map
    L.control.layers(baseLayers, overlays, layersOptions).addTo(map);
}

/**
 * Adds locate control to the map.
 */
function addLocateControl() {
    // Sets locate options
    var locateOptions = {
	//keepCurrentZoomLevel: true,
	onLocationError: function (err) {
	    //alert(err.message)
	    alert('Geolokační chyba:\nUživatel odmítl sdělit svou zeměpisnou polohu.');
	},
	strings: {
	    title: 'Zobrazit moji polohu', // title of the locate control
	    metersUnit: 'metrů', // string for metric units
	    feetUnit: 'stop', // string for imperial units
	    popup: 'Moje poloha s přesností na {distance} {unit}.', // text to appear if user clicks on circle
	    outsideMapBoundsMsg: 'Nacházíte se mimo hranice mapy.' // default message for onLocationOutsideMapBounds
	},
	locateOptions: {
	    maxZoom: maxZoom, // maximum zoom for automatic view
	    enableHighAccuracy: true
	}
    };

    // Adds locate control to map
    L.control.locate(locateOptions).addTo(map).start();
}

/**
 * Adds scale control to the map.
 */
function addScaleControl() {
    // Sets scale options
    var scaleOptions = {
	maxWidth: 200,
	imperial: false
    };

    // Adds scale control to map
    L.control.scale(scaleOptions).addTo(map);
}

/**
 * Adds search control to the map.
 */
function addSearchControl() {
    // Sets search options
    var searchOptions = {
	url: '//nominatim.openstreetmap.org/search?format=json&countrycodes=cz,sk&q={s}', // url for search
	jsonpParam: 'json_callback',
	propertyName: 'display_name',
	propertyLoc: ['lat', 'lon'],
	textErr: 'Nenalezeno', // error message
	textCancel: 'Zrušit', // title in cancel button		
	textPlaceholder: 'Vyhledat...', // placeholder value
	zoom: 12,
	autoCollapse: true, // auto collapse after showing results
	delayType: 0,
	markerLocation: true
    };

    // Adds search control to map
    L.control.search(searchOptions).addTo(map);
}

/**
 * Adds zoom control to the map.
 */
function addZoomControl() {
    // Sets zoom options
    var zoomOptions = {
	zoomInTitle: 'Přiblížit',
	zoomOutTitle: 'Oddálit'
    };

    // Adds zoom control to map
    L.control.zoom(zoomOptions).addTo(map);
}

/**
 * ******** Loading data *******************************************************
 */

/**
 * Downloads data via ajax.
 */
function loadData() {
    jsonDataMobile = null;
    jsonDataWlan = null;
    var select = $('#frm-mapForm-measurement').val();

    if (select.match(/.*_mobile$/)) { // mobile data
	jsonDataMobile = downloadData(getMobileUrl(select));
    } else if (select.match(/.*_wlan$/)) { // wlan data
	jsonDataWlan = downloadData(getWlanUrl(select));
    } else { // all data
	jsonDataMobile = downloadData(getMobileUrl(select));
	jsonDataWlan = downloadData(getWlanUrl(select));
    }
}

/**
 * Downloads relation data via ajax.
 */
function loadRelationData() {
    jsonDataMobile = downloadData(getRalationUrl());
    jsonDataWlan = null;
}

/**
 * Downloads my data via ajax.
 */
function loadMyData() {
    jsonDataMobile = downloadData(getMyDataUrl());
    jsonDataWlan = null;
}

/**
 * Load and draw geoJSON data into the map.
 */
function loadGeoJson() {
    if ($('#headingTwo').hasClass('active')) {
	jsonDataMobile.done(function (data) {
	    $.getJSON(data._links[0].href).done(function (measurements) {
		relation.clearLayers();
		relation.addData(measurements).addTo(map);
		map.fitBounds(relation.getBounds());
	    });
	});
    }
}

/**
 * Retruns mobile url for API call.
 * @param {String} select
 * @returns {String}
 */
function getMobileUrl(select) {
    var action = '';
    action = select.replace('_mobile', '');
    action += 's/technology/2G-3G-4G';
    return getUrl(action);
}

/**
 * Retruns WLAN url for API call.
 * @param {String} select
 * @returns {String}
 */
function getWlanUrl(select) {
    var action = '';
    action = select.replace('_wlan', '');
    action += 's/technology/WLAN';
    return getUrl(action);
}

/**
 * Retruns Relation url for API call.
 * @returns {String}
 */
function getRalationUrl() {
    var measurement = $('#frm-relationsForm-measurement').val();
    var relationId = $('#frm-relationsForm-relation').val();
    var action = 'relations';
    return restApiUrl + apiVersion + '/' + action + '/' + relationId + '/' + measurement.replace('_mobile', '');
}

/**
 * Retruns MyData url for API call.
 * @returns {String}
 */
function getMyDataUrl() {
    var measurement = $('#frm-myDataForm-measurement').val();
    var limit = $('#frm-myDataForm-limit').val();
    var action = 'my-data';
    return restApiUrl + apiVersion + '/' + action + '/' + measurement + 's?limit=' + limit;
}

/**
 * Returns url for API call.
 * @param {String} action
 * @returns {String}
 */
function getUrl(action) {
    var period = '1year';
    return restApiUrl + apiVersion + '/' + action + '?' + 'period=' + period;
}

/**
 * Downloads data and returns jqXHR object.
 * @param {String} url
 * @returns {Object}
 */
function downloadData(url) {
    //console.log(url);
    // shorthand for $.ajax();
    return $.getJSON(url)
	    .done(function () {
		//console.log('Data downloading successful.');
		//hideLoaderOverlay();
	    })
	    .fail(function () {
		console.log('Data downloading failed.');
		//alert('Nepodařilo se načíst data.');
		//hideLoaderOverlay();
	    });
}

/**
 * ******** Events listeners ***************************************************
 */

/**
 * Listener for clicking to marker.
 * @param {Object} e
 */
function markerOnClick(e) {
    var popup = e.target._map._popup;
    var content = popup.getContent();
    var action = '';
    if (content.match(/.*od-[1-9][0-9]*/)) {
	action = 'od/' + content.match(/[1-9][0-9]*/);
    } else if (content.match(/.*mb-[1-9][0-9]*/)) {
	action = 'mb/' + content.match(/[1-9][0-9]*/);
    } else {
	return;
    }

    var url = restApiUrl + apiVersion + '/measurements/' + action;
    $.getJSON(url).done(function (data) {
	var signal = data.measurement.signal ? data.measurement.signal + ' dBm' : '';
	var operator = data.measurement.operator ? data.measurement.operator : '';
	var model = data.measurement.model ? data.measurement.model : '';
	var platform = data.measurement.platform ? data.measurement.platform : '';

	popup.setContent('\
<section class="measurement">\n\
    <h3>Měření ' + data.measurement.id + '</h3>\n\
    <h5>' + data.measurement.date + '</h5>\n\
    <table class="table table-striped table-hover">\n\
	<tr><th>Download</th><td></td><td>' + data.measurement.download + ' kbps</td></tr>\n\
	<tr><th>Upload</th><td></td><td>' + data.measurement.upload + ' kbps</td></tr>\n\
	<tr><th>Ping</th><td></td><td>' + parseInt(data.measurement.ping) + ' ms</td></tr>\n\
	<tr><th>Signál</th><td></td><td>' + signal + '</td></tr>\n\
	<tr><th>Technologie</th><td></td><td>' + data.measurement.technology + '</td></tr>\n\
	<tr><th>Síť</th><td></td><td>' + data.measurement.network + '</td></tr>\n\
	<tr><th>Operátor</th><td></td><td>' + operator + '</td></tr>\n\
	<tr><th>Model</th><td></td><td>' + model + '</td></tr>\n\
	<tr><th>Operační systém</th><td></td><td>' + platform + '</td></tr>\n\
	<tr><th>Zdroj</th><td></td><td>' + data.measurement.source + '</td></tr>\n\
    </table>\n\
</section>');
	popup.update();
    }).fail(function () {
	popup.setContent('Nepodařilo se načíst záznam.');
	popup.update();
    });
}

/**
 * Listener for clicking to the map.
 * @param {Object} e
 */
function onClick(e) {
    if (posMarker) {
	map.removeLayer(posMarker);
	posMarker = null;
    } else {
	posMarker = L.circleMarker(e.latlng);
	//var radius = 85;
	//posMarker = L.circle(e.latlng, radius);
	map.addLayer(posMarker);
	posMarker.bindPopup('Poloha: [' + roundGeoCoord(e.latlng.lat) + ', ' + roundGeoCoord(e.latlng.lng) + ']').openPopup();
    }
}

/**
 * Listener for adding overlay.
 * @param {Object} e
 */
function onOverlayAdd(e) {
    redrawHeatmap();
    map.invalidateSize();
}

/**
 * Listener for zooming end.
 * @param {Object} e
 */
function onZoomEnd(e) {
    redrawHeatmap();
    map.invalidateSize();
}

/**
 * ******** Helpers ************************************************************
 */

/**
 * Redraws map.
 */
function redrawMap() {
    counter = 0;
    emptyArrays();

    loadDataToMap(jsonDataMobile);
    loadDataToMap(jsonDataWlan);
    // wait for done of all jqXHR
    $.when(jsonDataMobile, jsonDataWlan).then(
	    function () {
		loadGeoJson();
		showMeasurementsInfo();
		setPointLayers();
		setHeatLayers();
		redrawHeatmap();
		//console.log('Data loaded: ' + markers.length);
		//console.log('Data loaded: ' + dataGreen.length);
		if (!map.hasLayer(points)) {
		    hideLoaderOverlay();
		}
	    },
	    function () {
		setPointLayers();
		setHeatLayers();
		redrawHeatmap();
		hideLoaderOverlay();
		alert('Nepodařilo se načíst požadovaná data.');
	    });
}

/**
 * Cleans all data and points arrays.
 */
function emptyArrays() {
    markers = [];
    dataDarkGreen = [];
    dataGreen = [];
    dataYellow = [];
    dataRed = [];
}

/**
 * Loads data to map.
 * @param {Object} jqXHR
 * @return {Boolean}
 */
function loadDataToMap(jqXHR) {
    if (!jqXHR) {
	return false;
    }

    var valPercentileSelect = $('#frm-mapForm-percentile').val();
    var valPeriodSelect = $('#frm-mapForm-period').val();
    var valTechnologySelect = getTechnologySelect();
    var valOperatorSelect = $('#frm-mapForm-operator').val();
    var valModelSelect = $('#frm-mapForm-model').val();
    var valPlatformSelect = $('#frm-mapForm-platform').val();

    var localCounter = 0;
    jqXHR.done(function (data) {
	var limit = getLimit(valPercentileSelect, Object.keys(data.measurements).length);
	$.each(data.measurements, function (key, val) {
	    if (testOpePlaMod(valOperatorSelect, val.oid) &&
		    testOpePlaMod(valModelSelect, val.mid) &&
		    testOpePlaMod(valPlatformSelect, val.pid) &&
		    testTechnology(valTechnologySelect, val.tid) &&
		    testPeriod(valPeriodSelect, val.date)) {
		counter++;
		localCounter++;
		setData(key, val);
		if (limit === localCounter) { // break each with return false
		    return false;
		}
	    }
	});
    });
}

/**
 * Returns technology select value.
 * @returns {Number}
 */
function getTechnologySelect() {
    var technology = $('select#frm-mapForm-technology');
    var measurement = $('select#frm-mapForm-measurement');
    var technologyWlan = $('select#frm-mapForm-technology option[value=' + defaultWlanId + ']');
    var technologyAll = $('select#frm-mapForm-technology option[value=' + defaultSelectVal + ']');

    if (technologyAll.length === 1 || technologyWlan.length === 1) {
	technology.val(defaultAllMobTechId);
    }

    technology.removeAttr('disabled', '');
    technologyWlan.remove();
    technologyAll.remove();

    if (measurement.val().match(/.*_mobile$/)) { // mobile data
	return technology.val(); // mobile technology ID
    } else if (measurement.val().match(/.*_wlan$/)) { // WLAN data
	technology.append($('<option>', {
	    value: defaultWlanId,
	    text: 'WLAN'
	}));
	technology.val(defaultWlanId);
	technology.attr('disabled', '');
	return defaultWlanId; // WLAN ID
    } else {
	technology.append($('<option>', {
	    value: defaultSelectVal,
	    text: 'Všechny sítě'
	}));
	technology.val(defaultSelectVal);
	technology.attr('disabled', '');
	return defaultSelectVal; // all data ID
    }
}

/**
 * Checks whether measurement has selected technology.
 * @param {String} selectVal
 * @param {Number} dataVal
 * @returns {Boolean}
 */
function testTechnology(selectVal, dataVal) {
    selectVal = parseInt(selectVal);
    if (selectVal === defaultSelectVal) { // all technologies
	return true;
    } else if ((selectVal === defaultAllMobTechId && dataVal !== defaultWlanId) || selectVal === dataVal) { // (2G/3G/4G + !WLAN) || all mobile technologies
	return true;
    } else if (selectVal === default2G3GId && (dataVal === default2GId || dataVal === default3GId)) { // 2G/3G + (2G || 3G) 
	return true;
    } else if (selectVal === default2G4GId && (dataVal === default2GId || dataVal === default4GId)) { // 2G/4G + (2G || 4G) 
	return true;
    } else if (selectVal === default3G4GId && (dataVal === default3GId || dataVal === default4GId)) { // 3G/4G + (3G || 4G) 
	return true;
    }

    return false;
}

/**
 * Checks whether measurement has selected operator, platform or model.
 * @param {String} selectVal
 * @param {Number} dataVal
 * @returns {Boolean}
 */
function testOpePlaMod(selectVal, dataVal) {
    selectVal = parseInt(selectVal);
    if (selectVal === defaultSelectVal || selectVal === dataVal) {
	return true;
    }
    return false;
}

/**
 * Checks whether measurement is in selected period.
 * @param {String} selectVal
 * @param {String} dataVal
 * @returns {Boolean}
 */
function testPeriod(selectVal, dataVal) {
    if (selectVal <= dataVal) {
	return true;
    }
    return false;
}

/**
 * Returns limit calculate by percentile.
 * @param {String} selectVal
 * @param {Number} count
 * @returns {Number}
 */
function getLimit(selectVal, count) {
    return parseInt(count * (parseInt(selectVal) / 100));
}

/**
 * Sets data.
 * @param {String} key
 * @param {Object} val
 */
function setData(key, val) {
    var select = $('select#frm-mapForm-measurement').val();
    var measurement = '';
    var limitSet = '';
    if (select.match(/.*_mobile$/)) { // mobile data
	measurement = select.replace('_mobile', '');
	limitSet = measurement;
	if (measurement.match(/^signal.*/)) {
	    limitSet = select;
	}
    } else if (select.match(/.*_wlan$/)) { // wlan data
	measurement = select.replace('_wlan', '');
	limitSet = measurement;
	if (measurement.match(/^signal.*/)) {
	    limitSet = select;
	}
    } else {
	measurement = limitSet = select;
    }

    if (measurement === 'ping') {
	setDataByColours(key, val, limits[limitSet].green, limits[limitSet].yellow, limits[limitSet].red, val[measurement], val[measurement], val[measurement]);
    } else {
	setDataByColours(key, val, val[measurement], val[measurement], val[measurement], limits[limitSet].green, limits[limitSet].yellow, limits[limitSet].red);
    }
}

/**
 * Sets data to arrays by colours (limits).
 * @param {String} key
 * @param {Object} val
 * @param {String} leftGreen
 * @param {String} leftYellow
 * @param {String} leftRed
 * @param {String} rightGreen
 * @param {String} rightYellow
 * @param {String} rightRed
 */
function setDataByColours(key, val, leftGreen, leftYellow, leftRed, rightGreen, rightYellow, rightRed) {
    var latLng = [val.lat, val.lng];
    var latLngIntensity = [val.lat, val.lng, heatmapIntensity];

    if (leftGreen >= rightGreen) { // dark green
	setDataToMarkers(key, latLng, pointsOptionsDarkGreen);
	dataDarkGreen.push(latLngIntensity);
    } else if (leftYellow >= rightYellow) { // green
	setDataToMarkers(key, latLng, pointsOptionsGreen);
	dataGreen.push(latLngIntensity);
    } else if (leftRed < rightRed) { // red
	setDataToMarkers(key, latLng, pointsOptionsRed);
	dataRed.push(latLngIntensity);
    } else { // yellow
	setDataToMarkers(key, latLng, pointsOptionsYellow);
	dataYellow.push(latLngIntensity);
    }
}

/**
 * Sets data to markers array.
 * @param {String} key
 * @param {Array} latLng
 * @param {Object} options
 */
function setDataToMarkers(key, latLng, options) {
    markers.push(L.circleMarker(latLng, options).bindPopup('Načítám záznam ' + key));
}

/**
 * Redraws heatmap.
 * return void
 */
function redrawHeatmap() {
    if (!map.hasLayer(heatmap)) {
	return;
    }

    heatmapDarkGreen.setOptions({
	minOpacity: getOpacity(),
	radius: getRadius(),
	blur: getBlur()
    });
    heatmapGreen.setOptions({
	minOpacity: getOpacity(),
	radius: getRadius(),
	blur: getBlur()
    });
    heatmapYellow.setOptions({
	minOpacity: getOpacity(),
	radius: getRadius(),
	blur: getBlur()
    });
    heatmapRed.setOptions({
	minOpacity: getOpacity(),
	radius: getRadius(),
	blur: getBlur()
    });
}

/**
 * Rounds geographic coordinates to max 6 decimals
 * @param {Number} geoCoord
 * @returns {Number}
 */
function roundGeoCoord(geoCoord) {
    return Math.round(geoCoord * 1000000) / 1000000;
}

/**
 * Returns opacity
 * @returns {Number}
 */
function getBlur() {
    return heatmapBlurList[map.getZoom() - minZoom];
}

/**
 * Returns radius
 * @returns {Number}
 */
function getRadius() {
    return heatmapRadiusList[map.getZoom() - minZoom];
}

/**
 * Returns opacity
 * @returns {Number}
 */
function getOpacity() {
    return heatmapOpacityList[map.getZoom() - minZoom];
}

/**
 * Updates progress bar
 * @param {number} processed
 * @param {number} total
 * @param {number} elapsed
 * @param {array} layersArray
 */
function updateProgressBar(processed, total, elapsed, layersArray) {
    if (elapsed > 1000) {
	showLoaderOverlay();
    }
    if (processed === total) {
	hideLoaderOverlay();
    }
}

/**
 * ******** Selects ************************************************************
 */

/**
 * Sets model to default value.
 */
function setModelToDefault() {
    $('select#frm-mapForm-model').val(defaultSelectVal);
}

/**
 * Sets relevant platform for phone model.
 */
function setRelevantPlatform() {
    var platform = $('select#frm-mapForm-platform');
    var platformOption = $('select#frm-mapForm-platform option');
    var model = $('select#frm-mapForm-model');
    var modelText = model.find(':selected').text();

    if (parseInt(model.val()) === defaultSelectVal) {
	platform.val(defaultSelectVal); // sets All platforms
	platform.removeAttr('disabled', '');
    } else if (modelText.match(/^iP.*/)) { // checks if text started to iP (iPhone, iPad, iPod, ...)
	platform.val(platformOption.filter(function () {
	    return ($(this).text() === 'iOS'); //To select Blue
	}).val()); // sets iOS
	platform.attr('disabled', '');
    } else {
	platform.val(platformOption.filter(function () {
	    return ($(this).text() === 'Android'); //To select Blue
	}).val()); // sets Android
	platform.attr('disabled', '');
    }
}

/**
 * Sets default values to selects.
 */
function setDefaultValues() {
    $('select#frm-mapForm-percentile').val(defaultPercentileId);
    $('select#frm-mapForm-period').val(defaultPeriodId);
    $('select#frm-mapForm-technology').val(defaultAllMobTechId);
    $('select#frm-mapForm-operator').val(defaultSelectVal);
    $('select#frm-mapForm-model').val(defaultSelectVal);
    $('select#frm-mapForm-platform').val(defaultSelectVal);
}

/**
 * ******** Main ***************************************************************
 */

/**
 * Main
 */
$(document).ready(function () {
    showLoaderOverlay();
    loadMap();

    if ($(window).width() >= 768) {
	layersControl = $('.leaflet-control-layers.leaflet-control');
	layersControl.addClass('leaflet-control-layers-expanded');
    }

    onResize();
    showLoaderOverlay();
    loadData();
    redrawMap();
});

/**  
 * ******** HTML & CSS manipulation ********************************************
 */

$('body').css('margin-bottom', '0').css('background', 'white');

/**
 * Listener for window resize.
 */
function onResize() {
    $(window).on('resize', function () {
	var winWidth = $(window).width();
	var winHeight = $(window).height();
	var offset = 51 + (winWidth < 992 ? 50 : 0);
	$('#map').height(height = winHeight - offset);
	map.invalidateSize();
    }).trigger('resize');
}

/**
 * Shows measurements info.
 */
function showMeasurementsInfo() {
    if ($('#headingOne').hasClass('active')) {
	var info = $('#measurement-info-one');
	info.removeClass('hidden');
    } else if ($('#headingTwo').hasClass('active')) {
	var info = $('#measurement-info-two');
	info.removeClass('hidden');
    } else {
	var info = $('#measurement-info-three');
	info.removeClass('hidden');
    }

    if (counter) {
	info.removeClass('alert-warning');
	info.addClass('alert-success');
    } else {
	info.removeClass('alert-success');
	info.addClass('alert-warning');
    }

    info.children('strong').html(counter);
}

/**
 * Redraws map when the data changed.
 */
function onChangeData() {
    showLoaderOverlay();
    map.removeLayer(relation);
    loadData();
    setTimeout(function () {
	redrawMap();
    }, 0);
}

/**
 * Redraws map when the select changed.
 */
function onChangeSelect() {
    showLoaderOverlay();
    setTimeout(function () {
	redrawMap();
    }, 0);
}

/**
 * Redraws map when the relation changed.
 */
function onChangeRelations() {
    if ($('select#frm-relationsForm-relation').val()) {
	showLoaderOverlay();
	setDefaultValues();
	loadRelationData();
	setTimeout(function () {
	    redrawMap();
	}, 0);
    }
}

/**
 * Redraws map when my data changed.
 */
function onChangeMyData() {
    showLoaderOverlay();
    map.removeLayer(relation);
    setDefaultValues();
    loadMyData();
    setTimeout(function () {
	redrawMap();
    }, 0);
}

/**  
 * ******** Map Form ***********************************************************
 */

$('select#frm-mapForm-measurement').change(onChangeData);

$('select#frm-mapForm-percentile').change(onChangeSelect);

$('select#frm-mapForm-period').change(onChangeSelect);

$('select#frm-mapForm-technology').change(onChangeSelect);

$('select#frm-mapForm-operator').change(onChangeSelect);

$('select#frm-mapForm-model').change(function () {
    setRelevantPlatform();
    onChangeSelect();
});

$('select#frm-mapForm-platform').change(function () {
    setModelToDefault();
    onChangeSelect();
});

/**  
 * ******** Relations Form *****************************************************
 */

$('select#frm-relationsForm-measurement').change(onChangeRelations);

$('select#frm-relationsForm-relation').change(onChangeRelations);

/**  
 * ******** MyData Form *****************************************************
 */

$('select#frm-myDataForm-measurement').change(onChangeMyData);

$('select#frm-myDataForm-limit').change(onChangeMyData);

/**  
 * ******** Headings *****************************************************
 */

$('#headingOne').click(function () {
    if (!$('#headingOne').hasClass('active')) {
	$('#headingTwo').removeClass('active');
	$('#headingThree').removeClass('active');
	$('#headingOne').addClass('active');
	onChangeData();
    }
});

$('#headingTwo').click(function () {
    if (!$('#headingTwo').hasClass('active')) {
	$('#headingOne').removeClass('active');
	$('#headingThree').removeClass('active');
	$('#headingTwo').addClass('active');
	onChangeRelations();
    }
});

$('#headingThree').click(function () {
    if (!$('#headingThree').hasClass('active')) {
	$('#headingOne').removeClass('active');
	$('#headingTwo').removeClass('active');
	$('#headingThree').addClass('active');
	onChangeMyData();
	map.setView(mapCenter, minZoom);
    }
});