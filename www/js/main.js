/**
 *  Project:  MBTest
 */

(function (window, document, undefined) {

    var mbtest = window.mbtest = {
	utils: {},
	pages: {},
	functions: {},
	cache: {}
    };

    /**
     * inicializacni funkce pro nastaveni promennych
     * @returns {undefined}
     */
    mbtest.utils.init = function () {
	mbtest.cache.window = $(window);
	mbtest.cache.document = $(document);
	mbtest.cache.html = $('html');
	mbtest.cache.body = $('body');

	//set the base url
	if (!window.location.origin)
	    window.location.origin = window.location.protocol + "//" + window.location.host;
	if (window.location.host == "127.0.0.1")
	    mbtest.cache.baseUrl = "http://127.0.0.1/dip_mbtest";
	else if (window.location.host == "localhost")
	    mbtest.cache.baseUrl = "http://localhost/dip_mbtest";
	else
	    mbtest.cache.baseUrl = window.location.origin;
    };

    // ------- FUNKCE ---------
    // ------------------------

    /**
     * Function representing php urlencode
     * @param string str
     * @return string encoded string
     */
    mbtest.functions.urlencode = function (str) {
	str = (str + '').toString();
	return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    }

    /**
     * Function representing php urldecode
     * @param string str
     * @return string decoded string
     */
    mbtest.functions.urldecode = function (str) {
	return decodeURIComponent((str + '').replace(/\+/g, '%20'));
    }

    /**
     * Function representing php htmlspcialchars_decode
     * @param string str
     * @return string encoded string
     */
    mbtest.functions.htmlspecialchars_decode = function (str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    };

    /**
     * saves general settings in administration
     */
    mbtest.functions.sendSaveSettings = function () {
	var queryPost = "saveSettings=1";

	$("#settings-form .form-group").each(function () {
	    var input = $(this).find("input");
	    if (input.is(":text")) {
		queryPost += "&settings[" + $(this).attr("data-id") + "]=" + input.val();
	    } else {
		var check = input.is(":checked") ? 1 : 0;
		queryPost += "&settings[" + $(this).attr("data-id") + "]=" + check;
	    }
	});

	$.ajax({
	    type: "POST",
	    cache: false,
	    url: mbtest.cache.baseUrl + "/www/admin/",
	    data: queryPost,
	    success: function (data) {
		if (data["status"] == "100") {
		    mbtest.functions.showSystemMessage("success", data["msg"]);
		} else {
		    mbtest.functions.showSystemMessage("danger", data["msg"]);
		}
		console.log(data);
	    }
	});
    };

    /**
     * searches for users
     * @returns {undefined}
     */
    mbtest.functions.searchUser = function () {
	var query = $("#search-user-input").val();
	if (query != "") {
	    var queryPost = "searchUsers=1&query=" + query;
	    $.ajax({
		type: "POST",
		cache: false,
		url: mbtest.cache.baseUrl + "/www/admin/rights/",
		data: queryPost,
		success: function (data) {
		    var content = "";

		    for (var i in data) {
			content += '<div class="user row">';
			content += '<div class="name col-sm-8"><h3><i class="fa fa-user"></i>' + data[i]["username"] + ', ' + data[i]["email"] + '</h3></div>';
			content += '<div class="options col-sm-4"><div class="col-sm-7">';
			content += '<select id="user-right-' + data[i]["id"] + '" class="form-control">';
			content += '<option value="0">admin</option><option value="1">right1</option><option value="2">right2</option><option value="3">right3</option><option value="4">right4</option>';
			content += '</select></div>';
			content += '<button class="btn btn-admin col-sm-5" onclick="mbtest.functions.changeRight(' + data[i]["id"] + ');">Změnit</button></div>';
			content += "</div>";
		    }

		    if (data.length == 0) {
			content += '<h3>Pro zadaný dotaz nebyli nalezeni žádní uživatelé.</h3>';
		    }

		    $("#found-users").fadeOut(function () {
			$("#found-users").html(content);
			$("#found-users").fadeIn();
		    });
		}
	    });
	}
    }

    mbtest.functions.trySearch = function (e) {
	if (e.keyCode == 13) {
	    mbtest.functions.searchUser();
	}
    };



    /**
     * changes rights of a user
     * @returns {undefined}
     */
    mbtest.functions.changeRight = function (user_id) {
	var right = $("#user-right-" + user_id).val();
	var queryPost = "changeRights=1&userid=" + user_id + "&right=" + right;
	$.ajax({
	    type: "POST",
	    cache: false,
	    url: mbtest.cache.baseUrl + "/www/admin/rights/",
	    data: queryPost,
	    success: function (data) {
		if (data["status"] == "100") {
		    mbtest.functions.showSystemMessage("success", data["msg"]);
		} else {
		    mbtest.functions.showSystemMessage("danger", data["msg"]);
		}
		//console.log(data);
	    }
	});

    };


    mbtest.functions.export = function (type) {
	if (type != 'csv' && type != 'xml' && type != 'zip' && type != 'device') {
	    return;
	}
	var queryPost = "export=1&export-type=" + type;

	var includeopendata = $("#param-opendata").is(":checked") ? '1' : '0';

	queryPost += "&opendata=" + includeopendata;

	$(".param").each(function () {
	    var inp = $(this).find("input");
	    var inpname = inp.attr("id").slice(6);
	    queryPost += "&param['" + inpname + "']=" + (inp.is(":checked") ? '1' : '0');
	});

	$.ajax({
	    type: "POST",
	    cache: false,
	    url: mbtest.cache.baseUrl + "/www/admin/export/",
	    data: queryPost,
	    success: function (data) {

		if (data.type == "xml") {
		    window.open(data.file, data.file);
		} else {
		    $.fileDownload(data.file, {
			successCallback: function (url) {
			    console.log("success");
			},
			failCallback: function (html, url) {
			    console.log("failure");
			}
		    });
		}
	    }
	});

    };

    /**
     * display system message in administration
     * 
     * @param {type} type
     * @param {type} message
     * @returns {undefined}
     */
    mbtest.functions.showSystemMessage = function (type, message) {
	var cont = $("#sys-notif");
	cont.hide();

	cont.html('<div style="cursor: pointer;" class="alert alert-' + type + '" role="alert">' + message + '</div>');
	cont.fadeIn('slow').click(function () {
	    cont.html('');
	}).delay(10000).fadeOut('slow', function () {
	    cont.html('');
	});
    };

    // ------- JEDNOTLIVE STRANKY ---------
    // ------------------------------------

    /**
     * obecne skripty pro vsechny stranky
     * @returns {undefined}
     */
    mbtest.pages.general = function () {

    };

    /**
     * administrace
     * @returns {undefined}
     */
    mbtest.pages.admin = function () {
	$(".set-export").click(function () {
	    $("#export-settings").slideToggle("slow");
	    var arrow = $(this).find("i");
	    if (arrow.hasClass("fa-caret-down")) {
		arrow.removeClass("fa-caret-down");
		arrow.addClass("fa-caret-up");
	    } else {
		arrow.removeClass("fa-caret-up");
		arrow.addClass("fa-caret-down");
	    }
	});


    };

    /**
     * mapa
     * @returns {undefined}
     */
    mbtest.pages.map = function () {
	var mapOptions = {
	    zoom: 18,
	    center: new google.maps.LatLng(50.082409, 14.424979),
	    mapTypeId: google.maps.MapTypeId.ROAD,
	    scrollwheel: false
	}
	var map = new google.maps.Map(document.getElementById('map'),
		mapOptions);

	var myLatLng = new google.maps.LatLng(50.082409, 14.424979);
	var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map
	});

	var infowindow = new google.maps.InfoWindow({
	    content: '<b>Kamenný obchod</b>:<br>Václavské náměstí 28<br>110 00 Praha 1'
	});
	google.maps.event.addListener(marker, 'click', function () {
	    infowindow.open(map, marker);
	});
    }

    mbtest.utils.domLoad = function () {

	if ($("#content").attr("data-type") == "map")
	    mbtest.pages.map();

	if ($("#content").attr("data-type") == "admin")
	    mbtest.pages.admin();

	mbtest.pages.general();

    };

    // ------ INICIALIZACE ------
    // --------------------------

    mbtest.utils.init();

    jQuery(function ($) {
	mbtest.utils.domLoad();
    });

    $("#dropdownMenu, #dropdown, #dropdownMapMenu").click(function () {
	$.gritter.removeAll();
    });


})(window, document);

$(function () {
    // Navbar
    $(window).scroll(function () {
	if ($(this).scrollTop() > 100) {
	    $(".navbar.navbar-fixed-top").addClass("scroll");
	    $(".scroll-top-wrapper").addClass("show");
	} else {
	    $(".navbar.navbar-fixed-top").removeClass("scroll");
	    $(".scroll-top-wrapper").removeClass("show");
	}
    });

    // scroll back to top btn
    $('.scrolltop').click(function () {
	$("html, body").animate({scrollTop: 0}, 700);
	return false;
    });

    // scroll navigation functionality
    $('.scroller').click(function () {
	var section = $($(this).data("section"));
	var top = section.offset().top - 40;
	$("html, body").animate({scrollTop: top}, 700);
	return false;
    });
});