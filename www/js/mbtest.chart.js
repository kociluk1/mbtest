/* 
 Charts
 Author: Lukas Koci <kociluk1@fit.cvut.cz>
 */

/**
 * ******** Global variables ***************************************************
 */

var tableData = {
    labels: [],
    downloads: [],
    uploads: [],
    pings: [],
    signals: []
};

/**
 * ******** Push data to arrays ************************************************
 */

$($('#dashboard-table tbody tr').get().reverse()).each(function () {
    var tds = $(this).find('td');
    tableData.labels.push(tds.eq(0).text());
    tableData.downloads.push(tds.eq(1).text());
    tableData.uploads.push(tds.eq(3).text());
    tableData.pings.push(tds.eq(5).text());
    tableData.signals.push(tds.eq(7).text());
});

/**
 * ******** Global chart configuration *****************************************
 */

Chart.defaults.global.responsive = true;

/**
 * ******** Charts data ********************************************************
 */

// downloads data
var dataDownload = {
    labels: tableData.labels,
    datasets: [
	{
	    label: "Download dataset",
	    fillColor: "rgba(62, 207, 53, 0.3)",
	    strokeColor: "rgba(62, 207, 53, 0.8)",
	    pointColor: "rgba(62, 207, 53, 1)",
	    pointStrokeColor: "#fff",
	    pointHighlightFill: "#fff",
	    pointHighlightStroke: "rgba(220,220,220,1)",
	    data: tableData.downloads
	}
    ]
};

// uploads data
var dataUploads = {
    labels: tableData.labels,
    datasets: [
	{
	    label: "Download dataset",
	    fillColor: "rgba(220, 49, 53, 0.3)",
	    strokeColor: "rgba(220, 49, 53, 0.8)",
	    pointColor: "rgba(220, 49, 53, 1)",
	    pointStrokeColor: "#fff",
	    pointHighlightFill: "#fff",
	    pointHighlightStroke: "rgba(220,220,220,1)",
	    data: tableData.uploads
	}
    ]
};

// signals data
var dataSignals = {
    labels: tableData.labels,
    datasets: [
	{
	    label: "Download dataset",
	    fillColor: " rgba(255, 190, 43, 0.3)",
	    strokeColor: " rgba(255, 190, 43, 0.8)",
	    pointColor: " rgba(255, 190, 43, 1)",
	    pointStrokeColor: "#fff",
	    pointHighlightFill: "#fff",
	    pointHighlightStroke: "rgba(220,220,220,1)",
	    data: tableData.signals
	}
    ]
};

// pings data
var dataPings = {
    labels: tableData.labels,
    datasets: [
	{
	    label: "Download dataset",
	    fillColor: "rgba(63, 131, 255, 0.3)",
	    strokeColor: "rgba(63, 131, 255, 0.8)",
	    pointColor: "rgba(63, 131, 255, 1)",
	    pointStrokeColor: "#fff",
	    pointHighlightFill: "#fff",
	    pointHighlightStroke: "rgba(220,220,220,1)",
	    data: tableData.pings
	}
    ]
};

/**
 * ******** Charts *************************************************************
 */

// downloads chart
var ctx = $("#myChartDownloads").get(0).getContext("2d");
var myDownloadsChart = new Chart(ctx).Line(dataDownload);

// uploads chart
var ctx = $("#myChartUploads").get(0).getContext("2d");
var myUploadsChart = new Chart(ctx).Line(dataUploads);

// signals chart
var ctx = $("#myChartSignals").get(0).getContext("2d");
var mySignalsChart = new Chart(ctx).Line(dataSignals);

// pings chart
var ctx = $("#myChartPings").get(0).getContext("2d");
var myPingsChart = new Chart(ctx).Line(dataPings);